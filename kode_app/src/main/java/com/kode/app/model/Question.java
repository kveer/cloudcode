package com.kode.app.model;

import java.sql.Timestamp;

import org.hibernate.validator.constraints.NotBlank;

public class Question {

	/**
	 * DB Identifiers
	 */
	private Integer qid;
	private Integer uid;
	private String status;

	/**
	 * UUID unique ID for each question.Will this be unique and can it be used
	 * as primary key in the db
	 */
	private String quid;

	/**
	 * If the question was asked for a specific mentor
	 */
	private String profilehandle;

	/**
	 * Corresponding Database Field for the date form field
	 */
	private Timestamp scheduletime;

	/**
	 * Question Form Fields
	 */
	@NotBlank
	private String title;
	private String kindofhelp;
	private String timezone;
	@NotBlank
	private String date;
	private String time;
	@NotBlank
	private String tags;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getKindofhelp() {
		return kindofhelp;
	}

	public void setKindofhelp(String kindofhelp) {
		this.kindofhelp = kindofhelp;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public Integer getUid() {
		return uid;
	}

	public void setUid(Integer uid) {
		this.uid = uid;
	}

	public Timestamp getScheduletime() {
		return scheduletime;
	}

	public void setScheduletime(Timestamp scheduletime) {
		this.scheduletime = scheduletime;
	}

	public Integer getQid() {
		return qid;
	}

	public void setQid(Integer qid) {
		this.qid = qid;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getProfilehandle() {
		return profilehandle;
	}

	public void setProfilehandle(String profilehandle) {
		this.profilehandle = profilehandle;
	}

	public String getQuid() {
		return quid;
	}

	public void setQuid(String quid) {
		this.quid = quid;
	}

}
