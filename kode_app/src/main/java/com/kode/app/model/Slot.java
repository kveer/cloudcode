package com.kode.app.model;

import java.sql.Timestamp;

import org.apache.commons.lang.builder.ToStringBuilder;

public class Slot {

	private Integer uid;
	private String date;
	private String time;
	private String timezone;
	private Timestamp slot;

	public Timestamp getSlot() {
		return slot;
	}

	public void setSlot(Timestamp slot) {
		this.slot = slot;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public Integer getUid() {
		return uid;
	}

	public void setUid(Integer uid) {
		this.uid = uid;
	}
	
	public String toString(){
		return ToStringBuilder.reflectionToString(this);
	}

}
