package com.kode.app.model;

import java.sql.Date;
import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

/**
 * This class and kode.app.model.User class are same. The only difference is
 * that User is used as a DTO whereas AuthUser is a Spring Security User
 * 
 * @author kiran
 * 
 */
public class AuthUser extends User {

	private static final long serialVersionUID = 6554863940535575771L;
	private int uid;
	private String name;
	private String timezone;
	private Date dateofjoining;
	private boolean isMentor;
	private int status;

	public AuthUser(String email, String password, int uid, String name, String timezone,boolean isMentor,int status,
			Collection<GrantedAuthority> authorities) {
		super(email, password, authorities);
		this.uid = uid;
		this.name = name;
		this.timezone = timezone;
		this.isMentor = isMentor;
		this.status = status;
	}

	public int getUid() {
		return uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public Date getDateofjoining() {
		return dateofjoining;
	}

	public void setDateofjoining(Date dateofjoining) {
		this.dateofjoining = dateofjoining;
	}

	public String toString() {
		return "uid : " + uid + " ," + super.toString();
	}

	public boolean isMentor() {
		return isMentor;
	}

	public void setMentor(boolean isMentor) {
		this.isMentor = isMentor;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
