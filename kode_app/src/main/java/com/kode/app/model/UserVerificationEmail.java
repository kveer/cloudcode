package com.kode.app.model;

public class UserVerificationEmail extends Email {

	private VerificationToken verificationToken;

	public UserVerificationEmail(String toAddress,
			VerificationToken verificationToken) {
		super(toAddress);
		this.verificationToken = verificationToken;
	}

	public String message;

	public String getMessage() {
		return "Please verify your email address by clicking on the below link"
				+ verificationToken.getEncodedToken();
	}

}
