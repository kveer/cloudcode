package com.kode.app.model;

public abstract class Email {

	public String toAddress;
	public String fromAddress;

	public Email(String toAddress) {
		this.toAddress = toAddress;
	}

	protected String getFromAddress() {
		return fromAddress;
	}

	protected void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}

	public abstract String getMessage();

}
