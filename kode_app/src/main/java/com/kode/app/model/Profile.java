package com.kode.app.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

import com.timgroup.jgravatar.Gravatar;
import com.timgroup.jgravatar.GravatarDefaultImage;
import com.timgroup.jgravatar.GravatarRating;

public class Profile {

	private int uid;
	
	int appId;//Not required

	private String headline;
	private String about;
	private String company;
	private String designation;
	private String gravatar;

	private String website;
	private String twitter;
	private String github;
	private String stackoverflow;
	private String linkedin;

	private Integer reputation;
	private Integer availability;
	private Integer rate;

	private String briefSkills;
	private List<Skill> skills;

	public Profile() {
		skills = new ArrayList<Skill>();
	}
	
	public Profile(Integer uid) {
		this();
		this.uid = uid;
	}

	public void buildSocialProfile(String url_website, String url_twitter,
			String url_github, String url_sof, String url_linkedin) {
		this.website = url_website;
		this.twitter = url_twitter;
		this.github = url_github;
		this.stackoverflow = url_sof;
		this.linkedin = url_linkedin;
	}

	public void buildPersonalProfile(String headline, String about,
			String company, String designation) {
		this.headline = headline;
		this.about = about;
		this.company = company;
		this.designation = designation;
	}

	public void buildMentorProfile(Integer availability, Integer rate) {
		this.availability = availability;
		this.rate = rate;
	}
	
	public void addSkill(Skill skill) {
		skills.add(skill);
	}

	public int getUid() {
		return uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

	public int getAppId() {
		return appId;
	}

	public void setAppId(int appId) {
		this.appId = appId;
	}

	public String getHeadline() {
		return headline;
	}

	public void setHeadline(String headline) {
		this.headline = headline;
	}

	public String getAbout() {
		return about;
	}

	public void setAbout(String about) {
		this.about = about;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public Integer getAvailability() {
		return availability;
	}

	public void setAvailability(Integer availability) {
		this.availability = availability;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getTwitter() {
		return twitter;
	}

	public void setTwitter(String twitter) {
		this.twitter = twitter;
	}

	public String getGithub() {
		return github;
	}

	public void setGithub(String github) {
		this.github = github;
	}

	public String getStackoverflow() {
		return stackoverflow;
	}

	public void setStackoverflow(String stackoverflow) {
		this.stackoverflow = stackoverflow;
	}

	public String getLinkedin() {
		return linkedin;
	}

	public void setLinkedin(String linkedin) {
		this.linkedin = linkedin;
	}

	public Integer getReputation() {
		return reputation;
	}

	public void setReputation(Integer reputation) {
		this.reputation = reputation;
	}

	public Integer getRate() {
		return rate;
	}

	public void setRate(Integer rate) {
		this.rate = rate;
	}

	public List<Skill> getSkills() {
		return skills;
	}

	public Profile setSkillList(List<Skill> skills) {
		this.skills = skills;
		return this;
	}

	public String getGravatar() {
		return gravatar;
	}

	public void setGravatar(String gravatar) {
		this.gravatar = gravatar;
	}

	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}

	public String getBriefSkills() {
		return briefSkills;
	}

	public void setBriefSkills(String briefSkills) {
		this.briefSkills = briefSkills;
	}

}
