package com.kode.app.model;

public class QuestionSkill {

	private Integer qid;
	private Integer skid;
	
	public QuestionSkill(Integer qid,Integer skid) {
		this.qid = qid;
		this.skid = skid;
	}

	public Integer getQid() {
		return qid;
	}

	public void setQid(Integer qid) {
		this.qid = qid;
	}

	public Integer getSkid() {
		return skid;
	}

	public void setSkid(Integer skid) {
		this.skid = skid;
	}

}
