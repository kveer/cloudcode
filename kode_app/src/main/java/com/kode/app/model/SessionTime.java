package com.kode.app.model;

import java.sql.Timestamp;

import org.apache.commons.lang.builder.ToStringBuilder;

public class SessionTime {

	private Integer id;
	private Integer sid;
	private Timestamp time;
	private Integer actorid;
	private String status;

	public SessionTime() {

	}

	public SessionTime(Integer sid, Timestamp time, Integer actorid,
			String status) {
		this.setSid(sid);
		this.setTime(time);
		this.setActorid(actorid);
		this.setStatus(status);
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getSid() {
		return sid;
	}

	public void setSid(Integer sid) {
		this.sid = sid;
	}

	public Timestamp getTime() {
		return time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

	public Integer getActorid() {
		return actorid;
	}

	public void setActorid(Integer actorid) {
		this.actorid = actorid;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
