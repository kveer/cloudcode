/**
 * 
 */
package com.kode.app.model;

/**
 * @author kiran
 * 
 */
public class UserBuilder {

	private User user = new User();

	public UserBuilder setName(String name) {

		user.setName(name);
		return this;
	}
	
	public UserBuilder isMentor(boolean isMentor) {
		user.setMentor(isMentor);
		return this;
	}
	
	public UserBuilder setPassword(String password) {

		user.setPassword(password);
		return this;
	}
	
	public UserBuilder setStatus(int status) {

		user.setStatus(status);
		return this;
	}
	
	public UserBuilder seTimezone(String timezone) {

		user.setTimezone(timezone);
		return this;
	}
	
	public UserBuilder setUid(int uid) {

		user.setUid(uid);
		return this;
	}
	
	public UserBuilder setEmail(String email) {

		user.setEmail(email);
		return this;
	}
	
	public UserBuilder setEnabled(boolean enabled) {

		user.setEnabled(enabled);
		return this;
	}
	
	public UserBuilder setRole(String role) {

		user.setRole(role);
		return this;
	}
	

	public User build() {

		return user;
	}

}
