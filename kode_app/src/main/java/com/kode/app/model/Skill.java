package com.kode.app.model;

public class Skill {
	
	//private String text;//This fiel dis for profile updation and angularjs tag library specified format
	private Integer uid;
	private Integer skid;
	private String skillname;
	private Integer experience;
	private String skillexpertise;
	private Integer rating;

	public Skill(){
		
	}
	
	public Skill(String skillname) {
		this.skillname = skillname;
	}

	public Skill(Integer uid, Integer skid, Integer experience,
			String skillexpertise, Integer rating) {
		this.setUid(uid);
		this.skid = skid;
		this.experience = experience;
		this.skillexpertise = skillexpertise;
		this.rating = rating;
	}

	
	public Integer getSkid() {
		return skid;
	}

	public void setSkid(Integer skid) {
		this.skid = skid;
	}

	public String getSkillname() {
		return skillname;
	}

	public void setSkillname(String skillname) {
		this.skillname = skillname;
	}

	public Integer getExperience() {
		return experience;
	}

	public void setExperience(Integer experience) {
		this.experience = experience;
	}

	public String getSkillexpertise() {
		return skillexpertise;
	}

	public void setSkillexpertise(String skillexpertise) {
		this.skillexpertise = skillexpertise;
	}

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}

	@Override
	public String toString() {
		return "skid :[" + skid + "] ,experience :[" + experience
				+ "] , skillexpertise :[" + skillexpertise + "] ,rating :["
				+ rating + "]";
	}

	public Integer getUid() {
		return uid;
	}

	public void setUid(Integer uid) {
		this.uid = uid;
	}
}
