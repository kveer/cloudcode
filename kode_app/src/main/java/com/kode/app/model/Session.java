package com.kode.app.model;

import java.sql.Timestamp;

import org.apache.commons.lang.builder.ToStringBuilder;

public class Session {

	private Integer sid;
	private Integer mentoruid;
	private Integer qid;
	/*private Timestamp sessiontime;*/
	private String timezone;
	private String status;
	private String chatRefChild;
	private Question question;
	
	public Session() {
		
	}
	
	public Session(Integer qid,Integer mentoruid,String status) {
		this.qid = qid;
		this.mentoruid=mentoruid;
		this.status = status;
	}
	
	public String toString(){
		return ToStringBuilder.reflectionToString(this);
	}

	public Integer getQid() {
		return qid;
	}

	public void setQid(Integer qid) {
		this.qid = qid;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getChatRefChild() {
		return chatRefChild;
	}

	public void setChatRefChild(String chatRefChild) {
		this.chatRefChild = chatRefChild;
	}

	/*public Timestamp getSessiontime() {
		return sessiontime;
	}

	public void setSessiontime(Timestamp sessiontime) {
		this.sessiontime = sessiontime;
	}*/

	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	public Integer getMentoruid() {
		return mentoruid;
	}

	public void setMentoruid(Integer mentoruid) {
		this.mentoruid = mentoruid;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public Integer getSid() {
		return sid;
	}

	public void setSid(Integer sid) {
		this.sid = sid;
	}

}
