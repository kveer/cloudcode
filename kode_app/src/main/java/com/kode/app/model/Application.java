package com.kode.app.model;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;


public class Application {

	private String firstname;

	private String lastname;
	
	private String email;

	private String[] skillArray;
	
	private String tags;
	
	private String headline;
	
	@NotEmpty
	private String password;
	
	@NotBlank
	private String profilehandle;
	
	boolean status;

	public String getHeadline() {
		return headline;
	}

	public void setHeadline(String headline) {
		this.headline = headline;
	}

	public String[] getSkills() {
		return skillArray;
	}

	public void setSkills(String[] skillArray) {
		this.skillArray = skillArray;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}
	
	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getProfilehandle() {
		return profilehandle;
	}

	public void setProfilehandle(String profilehandle) {
		this.profilehandle = profilehandle;
	}
	
}
