/**
 * 
 */
package com.kode.app.model;

/**
 * @author kiran
 * 
 */
public class Input {
	private Question question;
	private Integer uid;
	private boolean isMentor;

	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	public Integer getUid() {
		return uid;
	}

	public void setUid(Integer uid) {
		this.uid = uid;
	}

	public boolean isMentor() {
		return isMentor;
	}

	public void setMentor(boolean isMentor) {
		this.isMentor = isMentor;
	}
}
