package com.kode.app.model;


import java.util.Date;
import java.util.UUID;

import org.apache.commons.codec.binary.Base64;
import org.joda.time.DateTime;
import org.joda.time.DateTimeUtils;

public class VerificationToken {

	private static final int DEFAULT_EXPIRY_TIME_IN_MINS = 60 * 24; // 24 hours

	private final String token;
	
	private Date expiryDate;

	private VerificationTokenType tokenType;

	private boolean verified;

	User user;
	
	/**
	 * Public constructor for mybatis reflection while populating token info from db
	 */
	public VerificationToken(){
		this.token =null;
	}
	/**
	 * This constructor can be used for getting the token deails from the database for a given encodedtoken
	 */
	public VerificationToken(String encodedToken) {
		this.token = new String(Base64.decodeBase64(encodedToken.getBytes()));
	}

	public VerificationToken(User user, VerificationTokenType tokenType,
			int expirationTimeInMinutes) {
		this.user = user;
		this.token = UUID.randomUUID().toString();
		this.tokenType = tokenType;
		this.expiryDate = calculateExpiryDate(expirationTimeInMinutes);
		System.out.println("comp0.1-->"+expiryDate.getTime());
		System.out.println("comp0.2-->"+expiryDate);
	}
	
	public void verified(){
		this.verified = true;
	}

	public VerificationTokenType getTokenType() {
		return tokenType;
	}

	public boolean isVerified() {
		return verified;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public String getEncodedToken() {
		return new String(Base64.encodeBase64(token.getBytes()));
	}	

	private Date calculateExpiryDate(int expiryTimeInMinutes) {
		DateTime now = new DateTime();
		System.out.println("comp0-->"+now.getMillis());
		DateTime newExpiryTime = now.plusMinutes(expiryTimeInMinutes);  
		return newExpiryTime.toDate();
	}

	public enum VerificationTokenType {

		forgotPassword, emailVerification, emailRegistration
	}

	public boolean hasExpired() {
		DateTime tokenDate = new DateTime(getExpiryDate());
		System.out.println("< --comp1 -- > "+tokenDate);
		System.out.println(DateTimeUtils.currentTimeMillis()+"< --comp2 -- > "+tokenDate.getMillis());
		return tokenDate.isBeforeNow();
	}
}
