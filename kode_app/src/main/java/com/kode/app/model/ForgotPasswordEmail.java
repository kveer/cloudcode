package com.kode.app.model;

public class ForgotPasswordEmail extends Email {

	private VerificationToken verificationToken;

	public ForgotPasswordEmail(String toAddress,
			VerificationToken verificationToken) {
		super(toAddress);
		this.verificationToken = verificationToken;
	}

	public String message;

	public String getMessage() {
		return "Please reset your password by following the link - >" + verificationToken.getEncodedToken();
	}

}
