package com.kode.app.model;

public class SessionQuery {

	private Integer sid;
	private String quid;
	private Integer uid;
	private Integer mentoruid;
	private String status;

	public SessionQuery(String quid, Integer uid, String status) {
		this.quid = quid;
		this.uid = uid;
		this.status = status;
	}

	public Integer getUid() {
		return uid;
	}

	public void setUid(Integer uid) {
		this.uid = uid;
	}

	public Integer getMentoruid() {
		return mentoruid;
	}

	public void setMentoruid(Integer mentoruid) {
		this.mentoruid = mentoruid;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getQuid() {
		return quid;
	}

	public void setQuid(String quid) {
		this.quid = quid;
	}

	public Integer getSid() {
		return sid;
	}

	public void setSid(Integer sid) {
		this.sid = sid;
	}
}
