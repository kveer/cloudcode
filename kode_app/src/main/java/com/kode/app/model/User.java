package com.kode.app.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.timgroup.jgravatar.Gravatar;
import com.timgroup.jgravatar.GravatarDefaultImage;
import com.timgroup.jgravatar.GravatarRating;

public class User {

	private int uid;
	@NotEmpty
	@Email
	private String email;// email db column
	@NotEmpty
	private String password;
	private String hashedPassword;
	@NotEmpty
	private String confirmpassword;
	private String name;
	private String timezone;
	private boolean enabled;
	private boolean isMentor;
	private int status;
	private String role;
	private boolean emailVerified;
	private String profilehandle;
	private Profile profile;
	List<VerificationToken> verificationTokens = new ArrayList<VerificationToken>(
			3); // Is the size needed

	public void setRegistrationState() {
		setEnabled(true);
		setRole("ROLE_USER");
		setHashedPassword(getHashPassword(password));
	}

	public void changePassword(String newpassword) {
		password = newpassword;
		setHashedPassword(getHashPassword(newpassword));
	}

	public void addVerificationToken(VerificationToken verificationToken) {
		verificationTokens.add(verificationToken);
	}

	public List<VerificationToken> getVerificationTokens() {
		return verificationTokens;
	}

	private String getHashPassword(String password) {
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		return passwordEncoder.encode(password);
	}
	
	public String determineProfileImage() {
		Gravatar gravatar = new Gravatar();
		gravatar.setSize(50);
		gravatar.setRating(GravatarRating.GENERAL_AUDIENCES);
		gravatar.setDefaultImage(GravatarDefaultImage.IDENTICON);		
		return gravatar.getUrl(email);
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public int getUid() {
		return uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmpassword() {
		return confirmpassword;
	}

	public void setConfirmpassword(String confirmpassword) {
		this.confirmpassword = confirmpassword;
	}

	public boolean isMentor() {
		return isMentor;
	}

	public void setMentor(boolean isMentor) {
		this.isMentor = isMentor;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public boolean isEmailVerified() {
		return emailVerified;
	}

	public void setEmailVerified(boolean emailVerified) {
		this.emailVerified = emailVerified;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getHashedPassword() {
		return hashedPassword;
	}

	public void setHashedPassword(String hashedPassword) {
		this.hashedPassword = hashedPassword;
	}

	public String getProfilehandle() {
		return profilehandle;
	}

	public void setProfilehandle(String profilehandle) {
		this.profilehandle = profilehandle;
	}

	public Profile getProfile() {
		return profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
