package com.kode.app.dao;

import java.util.List;
import java.util.Map;

import com.kode.app.model.Profile;
import com.kode.app.model.Skill;

public interface MentorApplicationDAO {

	public void create(Map<String, String> applicationMap);

	public void confirm(Profile profile);

	public void update(Map application);

	public void activate(int uid);

	public Map getApplication(String appId);

	public void mapSkillstoMentor(List<Skill> skillList);

}
