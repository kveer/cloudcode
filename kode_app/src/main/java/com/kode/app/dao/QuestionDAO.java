package com.kode.app.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.kode.app.model.Question;
import com.kode.app.model.Session;
import com.kode.app.model.SessionQuery;
import com.kode.app.model.SessionTime;
import com.kode.app.model.Slot;

public interface QuestionDAO {

	public Integer saveQuestion(Question question);

	public void updateQuestion(Question question);

	public List<Question> getQuestions();
	
	public Question getQuestion(@Param("quid") String quid,@Param("userUid") Integer userUid);

	public List<Question> getQuestionsByUser(Integer uid);

	public List<Question> getQuestionsByTag(Map tagMap);

	public void setSlot(Slot slot);

	public List<Slot> getSlots(int uid);

	public Integer createSession(Session session);

	public void updateSession(Session session);

	public Session getSessionInfo(SessionQuery sessionQuery);

	public Session getSession(Session session);

	public List<Session> getSessions(Integer uid);

	public void mapSessionTime(SessionTime sessionTime);

	public void updateSessionTime(SessionTime sessionTime);
}
