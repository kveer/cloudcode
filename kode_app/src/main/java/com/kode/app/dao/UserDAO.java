package com.kode.app.dao;

import org.apache.ibatis.annotations.Param;

import com.kode.app.model.Profile;
import com.kode.app.model.User;
import com.kode.app.model.VerificationToken;

public interface UserDAO {
	int registered(String email);

	void register(User obj);

	void activate(User user);

	User getUser(String email);

	User getUserProfile(Integer userUid);

	User getProfile(Integer userUid);

	void mapUserVerificationToken(@Param("user") User user,
			@Param("verificationtoken") VerificationToken verificationToken);

	VerificationToken loadToken(VerificationToken verificationToken);

	void tokenVerified(VerificationToken verificationToken);

	void userVerified(User user);

	void changePassword(User user);

	void updatePersonalDetails(User user);

	void updatePersonalStatement(Profile profile);

	void updateSocialPresence(Profile profile);

	void updateSkills(Profile profile);
}
