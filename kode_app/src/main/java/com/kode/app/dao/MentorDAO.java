package com.kode.app.dao;

import java.util.List;

import com.kode.app.model.Mentor;
import com.kode.app.model.Skill;

public interface MentorDAO {

	public List<Mentor> getMentors();

	public Mentor getMentorByHandle(final String profilehandle);

	public List<Skill> getSkills();

}
