package com.kode.app.util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.kode.app.context.KodeApplicationContext;
import com.kode.app.model.Skill;
import com.kode.app.service.MentorService;

/**
 * 
 * @author kiran
 * 
 */
public class KodeUtil {
	private static HashMap<Integer, String> skillMap;
	private static boolean initialized = true;
	static Logger log = Logger.getLogger(KodeUtil.class.getName());

	// @Autowired(required=true)
	static MentorService profileService = (MentorService) KodeApplicationContext
			.getBean("profileService");

	static {
		log.info("Entry static block");
		try {

			List<Skill> skillList = profileService.getSkills();
			skillMap = new HashMap<Integer, String>();
			Iterator<Skill> skillIt = skillList.iterator();
			while (skillIt.hasNext()) {
				Skill skill = skillIt.next();
				skillMap.put(skill.getSkid(), skill.getSkillname());
			}
		} catch (Exception e) {
			initialized = false;
			log.error("Error while initializing static block" + e.getMessage());
			e.printStackTrace();
		}
		log.info("Static block initilaized with status : " + initialized);
		log.info("Printing skillmap : " + skillMap);
	}

	public static HashMap<Integer, String> getSkillMap() {
		return skillMap;
	}
}
