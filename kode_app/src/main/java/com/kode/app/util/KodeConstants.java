package com.kode.app.util;

public class KodeConstants {
	
	public static final String UID = "uid";
	public static final String APPID = "appId";
	public static final String SKILLS = "skills";
	public static final String SKILL = "skill";
	public static final String SKILL_LIST = "skillList";
	public static final String REDIRECT = "redirect";
	public static final String TIMEZONE = "timezone";
	public static final String MESSAGE = "message";

}
