package com.kode.app.service;

import java.io.InputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.kode.app.dao.UserDAO;
import com.kode.app.model.Profile;
import com.kode.app.model.User;
import com.kode.app.model.VerificationToken;

public class UserService {

	@Autowired(required = true)
	private UserDAO userDAO;

	public UserDAO getUserAuthDAO() {
		return userDAO;
	}

	public void setUserAuthDAO(UserDAO userAuthDAO) {
		this.userDAO = userAuthDAO;
	}

	public void register(User user) {
		userDAO.register(user);
	}

	public void activate(User obj) {
		userDAO.activate(obj);
	}

	public User getUser(String email) {
		User user = userDAO.getUser(email);
		return user;
	}
	
	public User getUserProfile(Integer userUid) {
		User user = userDAO.getUserProfile(userUid);
		return user;
	}

	public boolean registered(String email) {
		int userRowCount = userDAO.registered(email);
		return (userRowCount > 0 ? true : false);
	}

	public void mapUserVerificationToken(User user) {
		userDAO.mapUserVerificationToken(user, user.getVerificationTokens()
				.get(0));
	}

	public VerificationToken loadToken(VerificationToken verificationToken) {
		return userDAO.loadToken(verificationToken);
	}

	@Transactional
	public void userVerified(VerificationToken verificationToken) {
		verificationToken.verified();
		userDAO.tokenVerified(verificationToken);
		userDAO.userVerified(verificationToken.getUser());
	}

	public void tokenVerified(VerificationToken verificationToken) {
		verificationToken.verified();
		userDAO.tokenVerified(verificationToken);
	}

	public void changePassword(User user) {
		userDAO.changePassword(user);
	}
	
	@Transactional
	public void updateProfileDetails(User user){
		userDAO.updatePersonalDetails(user);
		user.getProfile().setUid(user.getUid());
		userDAO.updatePersonalStatement(user.getProfile());
	}
	
	public void updateSocialPresence(Profile profile){
		userDAO.updateSocialPresence(profile);
	}
	
	public void updateSkills(Profile profile){
		userDAO.updateSkills(profile);			
	}
	
	public void updateProfilePic(Integer uid,InputStream profilePicStream){
		//userDAO.updateSkills(profile);			
	}

}
