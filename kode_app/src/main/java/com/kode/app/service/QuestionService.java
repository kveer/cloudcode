package com.kode.app.service;


import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.kode.app.dao.QuestionDAO;
import com.kode.app.model.Mentor;
import com.kode.app.model.Profile;
import com.kode.app.model.Question;
import com.kode.app.model.Session;
import com.kode.app.model.SessionQuery;
import com.kode.app.model.SessionTime;
import com.kode.app.model.Slot;

public class QuestionService {

	static Logger log = Logger.getLogger(QuestionService.class.getName());

	@Autowired(required = true)
	private QuestionDAO questionDAO;

	@Autowired(required = true)
	private MentorService mentorService;

	/*
	 * Fix this by making it transactional and invoking an sp which will map
	 * skills to question by splitting the tag string from the question. For now
	 * we will use like comparision query.
	 */
	@Transactional
	public void saveQuestion(Question question) {
		Mentor mentor = null;
		if (question.getProfilehandle() != null) {
			mentor = mentorService.getMentorByHandle(question.getProfilehandle());
			if (mentor == null) {
				throw new RuntimeException("No profile handle");
			}
		}
		Integer qid = questionDAO.saveQuestion(question);
		if (question.getProfilehandle() != null) {
			createSession(qid,mentor.getUid());
			/*questionDAO.mapSessionTime(new SessionTime(sid, question
					.getScheduletime(), question.getUid(), "Initial"));*/
		}		
	}
	
	public Integer createSession(Integer qid,Integer mentorUid){
		Session newSession = new Session(qid, Integer.valueOf(mentorUid), "current");
		return questionDAO.createSession(newSession);		
	}

	@Transactional
	public void updateQuestion(Question question, Integer actorUid) {
		questionDAO.updateQuestion(question);
		/**
		 * Above :Here we anywayz update the question table. Below : We update
		 * the time in the session table only if there exists a session with any
		 * user in which case the status in the session table will be current.
		 */
		SessionQuery sessionQuery = new SessionQuery(question.getQuid(),
				question.getUid(), "current");
		Session session = questionDAO.getSessionInfo(sessionQuery);
		if (session != null) {
			/**
			 * The rows in session_time will be in invalid status or the rows
			 * will be stale in case the user updates the time consecutively
			 * without the reschedule being accepted or rejected by the mentor
			 * in between these 2 changes.For this We can check if the latest
			 * session row is in proposed or default status and the actor is
			 * user,if yes then update it rather than creating a new row. <br>
			 * <br>
			 * We only show the accept(and reject) button to the user or mentor
			 * when the status is proposed(or default) in the session_time table
			 * by either user or mentor. Also a user as per this logic can
			 * override any reschedule proposed by the mentor as we are just
			 * pushing a new row into session_time table for any question update
			 * without checking if the latest row in session_time has any
			 * reschedule proposed by the mentor for the current session. <br>
			 * *
			 */
			SessionTime sessionTime = new SessionTime(session.getSid(),
					question.getScheduletime(), actorUid, "proposed");
			questionDAO.mapSessionTime(sessionTime);
		}
	}

	/**
	 * There shud be 2 methods.One for rejecting an accepted session for mentor
	 * functionality and one for rejecting a session time proposal either by
	 * user or mentor.For the former we have to change the accept button to
	 * reject when the user clicks on accept for a session that is assigned to
	 * him by the admin or in case he manually browsed the questions and
	 * accepted the session
	 * 
	 * 
	 * @param question
	 * @param uid
	 */
	@Transactional
	public void updateSessionTime(SessionTime sessionTime, Integer uid) {
		/**
		 * Accept or reject just status changes hence method name s
		 * updatesessiontime
		 */
		/**
		 * Before this check if the sessionid belongs to the logged in user or
		 * mentor
		 **/
		// questionDAO.updateQuestion(question);
		questionDAO.updateSessionTime(sessionTime);
	}

	@Transactional
	public void rejectSession(Question question, Integer uid) {
		questionDAO.updateQuestion(question);
		/**
		 * Above :Here we anywayz update the question table. Below : We update
		 * the time in the session table only if there exists a session with any
		 * user in which case the status in the session table will be current.
		 */
		SessionQuery sessionQuery = new SessionQuery(question.getQuid(),
				question.getUid(), "current");
		Session session = questionDAO.getSessionInfo(sessionQuery);
		if (session != null) {
			/**
			 * The rows in session_time will be in invalid status or the rows
			 * will be stale in case the user updates the time consecutively
			 * without the reschedule being accepted or rejected by the mentor
			 * in between these 2 changes.For this We can check if the latest
			 * session row is in proposed or default status and the actor is
			 * user,if yes then update it rather than creating a new row. <br>
			 * <br>
			 * We only show the accept(and reject) button to the user or mentor
			 * when the status is proposed(or default) in the session_time table
			 * by either user or mentor. Also a user as per this logic can
			 * override any reschedule proposed by the mentor as we are just
			 * pushing a new row into session_time table for any question update
			 * without checking if the latest row in session_time has any
			 * reschedule proposed by the mentor for the current session. <br>
			 * *
			 */
			SessionTime sessionTime = new SessionTime(session.getSid(),
					question.getScheduletime(), uid, "proposed");
			questionDAO.mapSessionTime(sessionTime);
		}
	}

	public List<Question> getQuestions() {
		return questionDAO.getQuestions();
	}

	public List<Question> getQuestionsByUser(Integer userUid) {
		return questionDAO.getQuestionsByUser(userUid);
	}

	public List<Question> getQuestionsByTag(Map tagMap) {
		return questionDAO.getQuestionsByTag(tagMap);
	}

	public Question getQuestion(String qid,Integer userUid) {
		return questionDAO.getQuestion(qid,userUid);
	}

	public void setSlot(Slot slot) {
		questionDAO.setSlot(slot);
	}

	public List<Slot> getSlots(int uid) {
		return questionDAO.getSlots(uid);
	}

	public void createSession(Session session) {
		questionDAO.createSession(session);
	}

	public void updateSession(Session session) {
		questionDAO.updateSession(session);
	}

	public Session getSession(Session session) {
		return questionDAO.getSession(session);
	}

	public List<Session> getSessions(Integer uid) {
		return questionDAO.getSessions(uid);
	}

}