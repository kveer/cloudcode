package com.kode.app.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.kode.app.dao.MentorApplicationDAO;
import com.kode.app.dao.UserDAO;
import com.kode.app.model.Application;
import com.kode.app.model.Profile;
import com.kode.app.model.Skill;
import com.kode.app.model.User;
import com.kode.app.model.UserBuilder;
import com.kode.app.util.KodeConstants;
import com.kode.app.util.KodeUtil;

public class MentorApplicationService {

	static Logger log = Logger.getLogger(MentorApplicationService.class
			.getName());
	static Random random = new Random();

	@Autowired(required = true)
	private MentorApplicationDAO applicationDAO;

	@Autowired(required = true)
	private UserDAO userDAO;

	@Transactional
	public Integer create(Application application) {
		log.info("Entry");
		User user = createUser(application.getEmail(),
				application.getFirstname(), application.getLastname());
		int uid = user.getUid();
		Integer appId = createProfile(uid, application.getHeadline(),
				application.getSkills(), application.getFirstname(),
				application.getLastname(), application.getTags());
		log.info("Exit");
		return appId;
	}

	@Transactional
	public void confirm(String appId, Profile profile) {
		HashMap appInfoMap = (HashMap) getApplication(appId);
		log.info("AppInfo Map : [" + appInfoMap + "]");
		long uid = (Long) appInfoMap.get("uid");
		profile.setUid((int) uid);
		applicationDAO.confirm(profile);
		for (Skill skill : profile.getSkills()) {
			skill.setUid((int) uid);
		}
		mapSkillstoMentor(profile.getSkills());
	}

	// @Transactional(readOnly=false, rollbackFor=DataAcccessException.class)
	@Transactional
	public void activate(String appId, String password, String profilehandle) {
		HashMap appInfoMap = (HashMap) getApplication(appId);
		long uid = (Long) appInfoMap.get("uid");
		/**
		 * Even the timezone from mentor_application table needs to be updated
		 * in User Table
		 */
		UserBuilder userBuilder = new UserBuilder().setUid((int) uid)
				.setPassword(password).setEnabled(true).isMentor(true)
				.setStatus(3);
		// Get default timezone from browser or we anywayz take this in
		// confirmation page of mentor as a manadtaory field
		userBuilder
				.seTimezone((appInfoMap.get("timezone") != null ? (String) appInfoMap
						.get("timezone") : null));
		userDAO.activate(userBuilder.build());
		/**
		 * Set status in mentor_application to true as this profile has been
		 * activated,so that when mentors access the edit or activate
		 * applications,they are shown a message
		 */
		Map<String, Object> application = new HashMap<String, Object>();
		application.put("status", true);
		application.put("uid", uid);
		application.put("profilehandle", profilehandle);
		applicationDAO.update(application);
		applicationDAO.activate((int) uid);
	}

	private User createUser(String email, String firstname, String lastname) {
		log.info("Entry");
		UserBuilder userBuilder = new UserBuilder().setEmail(email)
				.setPassword("").setFirstname(firstname).setLastname(lastname)
				.setStatus(2).setRole("ROLE_MENTOR").setEnabled(false)
				.isMentor(true);
		/**
		 * 1 is for user . 2 is for Mentor registration. 3 for mentor
		 * confirmation. 4 for mentor rejection.
		 */
		User user = userBuilder.build();
		userDAO.register(user);
		log.info("Exit");
		return user;
	}

	private Integer createProfile(int uid, String headline,
			String[] skillArray, String firstname, String lastname, String tags) {
		log.info("Entry");
		HashMap<Integer, String> skillMap = KodeUtil.getSkillMap();
		StringBuilder skillIdBuilder = new StringBuilder();
		StringBuilder skillNameBuilder = new StringBuilder();
		for (String skill : skillArray) {
			skillIdBuilder.append(" ").append(skill);
			skillNameBuilder.append(skillMap.get(Integer.valueOf(skill)))
					.append(" ");
		}
		String applicationSkillIdStr = skillIdBuilder.toString();
		log.info("applicationSkillStr :[" + applicationSkillIdStr + "]");

		int appId = random.nextInt();
		/**
		 * The below check is for time being.Need to change.
		 */
		if (appId < 0) {
			appId = 0 - appId;
		}
		log.info("AppId : " + appId);
		HashMap<String, String> applicationMap = new HashMap<String, String>();
		applicationMap.put(KodeConstants.UID, String.valueOf((uid)));
		applicationMap.put(KodeConstants.APPID, String.valueOf((appId)));
		applicationMap.put("firstname", firstname);
		applicationMap.put("lastname", lastname);
		applicationMap.put(KodeConstants.SKILL_LIST, applicationSkillIdStr);
		applicationMap.put(KodeConstants.SKILL, skillNameBuilder.toString());
		applicationMap.put("headline", headline);
		applicationMap.put("tags", tags);
		/**
		 * skillCount is obsolete as it's not used now
		 */
		applicationMap.put("skillCount", String.valueOf(skillArray.length));
		applicationDAO.create(applicationMap);
		log.info("Exit");
		return appId;
	}

	public Map getApplication(String appId) {
		return applicationDAO.getApplication(appId);
	}

	public void mapSkillstoMentor(List<Skill> skillList) {
		applicationDAO.mapSkillstoMentor(skillList);
	}

	private void FireExcepForTransactionTesting() {

		throw new RuntimeException("Firing exception");
	}
}