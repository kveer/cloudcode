package com.kode.app.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.kode.app.dao.MentorDAO;
import com.kode.app.model.Mentor;
import com.kode.app.model.Skill;

public class MentorService {

	static Logger log = Logger.getLogger(MentorService.class.getName());

	@Autowired(required = true)
	private MentorDAO mentorDAO;

	public Mentor getMentorByHandle(final String profilehandle) {
		return mentorDAO.getMentorByHandle(profilehandle);
	}
	
	public List<Mentor> getMentors() {
		return mentorDAO.getMentors();
	}

	public List<Skill> getSkills() {
		return mentorDAO.getSkills();
	}
}