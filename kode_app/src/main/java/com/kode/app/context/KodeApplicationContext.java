package com.kode.app.context;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class KodeApplicationContext implements ApplicationContextAware {
	
	static {
		System.out.println("testing env");
        System.setProperty("env", "dev");
    }
	
	private static ApplicationContext ctx;

	public void setApplicationContext(ApplicationContext appContext)
			throws BeansException {
		ctx = appContext;

	}

	public static Object getBean(String beanName) {
		return ctx.getBean(beanName);
	}
}
