/**
 * 
 */
package com.kode.app.exception;

/**
 * @author kiran
 * 
 */
public class VerificationException extends RuntimeException {
	
	private static final long serialVersionUID = 5184188846665985002L;
	
	public enum VerificationError {

		UserVerified("UserVerified"),VerifiedToken("VerifiedToken"),InvalidToken("InvalidToken"),ExpiredToken("ExpiredToken");

		private String errormessage;

		private VerificationError(String errormessage) {
			this.errormessage = errormessage;
		}
		
		public String getErrorMessage(){
			return errormessage;
		}
	}
	
	/**
	 * Constructor for HandlerWebException.
	 * @param msg the detail message
	 */
	public VerificationException(VerificationError verificationError) {
		super(verificationError.getErrorMessage());
	}

}
