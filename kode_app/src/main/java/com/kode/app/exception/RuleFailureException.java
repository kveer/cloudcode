/**
 * 
 */
package com.kode.app.exception;

/**
 * @author kiran
 * 
 */
public class RuleFailureException extends RuntimeException {

	private String errorPage;
	private String errorPageMsg;
	private static final long serialVersionUID = 5184188846665985002L;

	public enum RuleFailureError {

		InvalidUser("InvalidUser");

		private String errormessage;

		private RuleFailureError(String errormessage) {
			this.errormessage = errormessage;
		}

		public String getErrorMessage() {
			return errormessage;
		}
	}

	/**
	 * Constructor for HandlerWebException.
	 * 
	 * @param msg
	 *            the detail message
	 */
	public RuleFailureException(RuleFailureError ruleFailureError) {
		super(ruleFailureError.getErrorMessage());
	}

	/**
	 * Constructor for HandlerWebException.
	 * 
	 * @param msg
	 *            the detail message
	 */
	public RuleFailureException(String msg) {
		super(msg);
	}

	/**
	 * Constructor for HandlerWebException.
	 * 
	 * @param msg
	 *            the detail message
	 * @param errorPage
	 *            if any specific error page to be shown
	 * @param cause
	 *            the root cause
	 */
	public RuleFailureException(String msg, String errorMessage) {
		super(msg);
		this.setErrorPageMsg(errorMessage);
	}

	/**
	 * Constructor for HandlerWebException.
	 * 
	 * @param msg
	 *            the detail message
	 * @param errorPage
	 *            if any specific error page to be shown
	 * @param cause
	 *            the root cause
	 */
	public RuleFailureException(String msg, String errorPage, Throwable cause) {
		super(msg, cause);
		this.setErrorPage(errorPage);
	}

	/**
	 * Constructor for HandlerWebException.
	 * 
	 * @param msg
	 *            the detail message
	 * @param errorPageMsg
	 *            if any specific error message to be shown on ErrorPage
	 * @param errorPage
	 *            if any specific error page to be shown
	 * @param cause
	 *            the root cause
	 */
	public RuleFailureException(String msg, String errorPageMsg,
			String errorPage) {
		super(msg);
		this.setErrorPage(errorPage);
		this.setErrorPageMsg(errorPageMsg);
	}

	public String getErrorPageMsg() {
		return errorPageMsg;
	}

	public void setErrorPageMsg(String errorPageMsg) {
		this.errorPageMsg = errorPageMsg;
	}

	public String getErrorPage() {
		return errorPage;
	}

	public void setErrorPage(String errorPage) {
		this.errorPage = errorPage;
	}

}
