package com.kode.app.gateway.email;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.kode.app.gateway.email.provider.EmailProvider;
import com.kode.app.model.Email;

public class EmailGateway {
	static Logger log = Logger.getLogger(EmailGateway.class.getName());

	@Autowired
	EmailProvider emailProvider;

	public void send(Email email) {
		emailProvider.send(email);
	}

}
