package com.kode.app.gateway.email.provider;

import org.apache.log4j.Logger;

import com.kode.app.model.Email;

public class SESEmailProvider implements EmailProvider {

	static Logger log = Logger.getLogger(SESEmailProvider.class.getName());
	
	public void send(Email email) {
		log.info("The email is sent"+email.getMessage());
	}

}
