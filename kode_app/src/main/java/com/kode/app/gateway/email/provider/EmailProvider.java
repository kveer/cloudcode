package com.kode.app.gateway.email.provider;

import com.kode.app.model.Email;

public interface EmailProvider {
	
	void send(Email email);

}
