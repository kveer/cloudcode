// create the module
var dashboardApp = angular.module('dashboardApp', [ 'ngRoute' ]);

// configure our routes
dashboardApp.config(function($routeProvider) {
	$routeProvider

	/*
	 * // route for the home page .when('/', { templateUrl :
	 * 'publicres/questions.html', controller : 'quesionsController' })
	 */
	// route for the questions page
	.when('/questions', {
		templateUrl : '/publicres/questions.html',
		controller : 'questionsController'
	})

	// route for the messages page
	.when('/sessions', {
		templateUrl : '/publicres/sessions.html',
		controller : 'sessionsController'
	})

	// route for the contact page
	.otherwise({
		redirectTo : '/questions'
	});
});

// create the controller and inject Angular's $scope
dashboardApp.controller('questionsController', function($scope, $http,
		$location) {
	/*
	 * {headers: { 'Authorization': 'Basic d2VudHdvcnRobWFuOkNoYW5nZV9tZQ==',
	 * 'Accept': 'application/json;odata=verbose' } };
	 */
	$http.get('/kode/questions/user').success(function(data) {
		console.log(data);
		$scope.questions = data;
	});
	$scope.isActive = function(viewLocation) {
		return viewLocation === $location.path();
	};
	$scope.isEmpty = function(obj) {
		return angular.equals([], obj);
	};
});

dashboardApp
		.controller(
				'sessionsController',
				function($scope) {
					$scope.message = 'Hey ! Hi from cloudkode.Hope you are fine.Please let us know your feedback';
				});

// create the module
var myaccountapp = angular.module('myaccountApp', [ 'ngRoute' ]);

// configure our routes
myaccountapp.config(function($routeProvider) {
	$routeProvider

	// route for the personal details page
	.when('/profile', {
		templateUrl : '/publicres/profile.html',
		controller : 'profileDetailsController'
	})

	// route for the billing page
	.when('/billing', {
		templateUrl : '/publicres/billing.html',
		controller : 'billingController'
	})

	.otherwise({
		redirectTo : '/profile'
	});
});

// create the controller and inject Angular's $scope
myaccountapp
		.controller(
				'profileDetailsController',
				function($scope, $http, $location, $anchorScroll) {
					// create a blank object to hold our form information
					// $scope will allow this to pass between controller and
					// view
					$http.get('/kode/users/account/profile').success(
							function(data) {
								$scope.user = data;
							}).error(
									function(data) {
										$scope.user = null;
										$scope.flashnotice = true;
										$scope.flashmessage = "Some issue while processing your request.Please try again later.";
									});
					// $scope.user = {name : "cracker"};

					$scope.isActive = function(viewLocation) {
						return viewLocation === $location.path();
					};

					// process the form
					$scope.processForm = function() {
						$http
								.post('/kode/users/account/profile',
										$scope.user)
								.success(
										function(data) {
											$scope.flashnotice = true;
											$scope.flashmessage = "Profile updated successfully";
										})
								.error(
										function(data) {
											$scope.flashnotice = true;
											$scope.flashmessage = "Some issue while processing your request.Please try again later.";
										});
						$anchorScroll();
					};

					// process the form
					$scope.close = function() {
						$scope.flashnotice = false;
					};

				});

myaccountapp.controller('billingController', function($scope, $route) {
	$scope.message = 'Hey ! All my questions have been answered freely';
});

// create the module
var profileapp = angular.module('profileapp', ['ngTagsInput']);
profileapp.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            
            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);

profileapp.controller('editProfileController', function($scope,$http,$anchorScroll) {
	$scope.message = 'Hey ! All my questions have been answered freely';
	$http.get('/kode/users/account/profile').success(
			function(data) {
				/**
				 * What if there is no user.profile object or profile.skills
				 * object.Shud we handle that in javascript ?
				 */
				$scope.user = data;
				$scope.editableUser = JSON.parse(JSON.stringify(data));
			}).error(
					function(data) {
						$scope.user = null;
						$scope.flashnotice = true;
						$scope.flashmessage = "Some issue while processing your request.Please try again later.";
					});
	// process the form
	$scope.savePrimary = function() {
		$scope.user = JSON.parse(JSON.stringify($scope.editableUser));
		$http
				.post('/kode/users/profile/primary',
						$scope.user)
				.success(
						function(data) {
							$scope.editable = "";
							$scope.flashnotice = true;
							$scope.flashmessage = "Profile updated successfully";							
						})
				.error(
						function(data) {
							$scope.flashnotice = true;
							$scope.flashmessage = "Some issue while processing your request.Please try again later.";
						});
		$anchorScroll();
	};
	
	$scope.cancel = function() {
		console.log("In cancel");
		$scope.editableUser =  JSON.parse(JSON.stringify($scope.user));
		$scope.editable = "";
	};
	
	$scope.saveSkills = function() {
		$scope.user = JSON.parse(JSON.stringify($scope.editableUser));
		$http
				.post('/kode/users/profile/skills',
						$scope.user.profile)
				.success(
						function(data) {
							$scope.editable = "";
							$scope.flashnotice = true;
							$scope.flashmessage = "Profile updated successfully";
						})
				.error(
						function(data) {
							$scope.flashnotice = true;
							$scope.flashmessage = "Some issue while processing your request.Please try again later.";
						});
		$anchorScroll();
	};
	
	// process the form
	$scope.close = function() {
		$scope.flashnotice = false;
	};	
	
	$scope.uploadFile = function(){
        var file = $scope.profilepicture;
        console.log(file);
        if(typeof file === 'undefined'){
        	alert("choose your picture");
        	return;
        }
        if(!validateProfilePicType(file))
        {	
        	console.log("Invalid Image");
        	return;
        }
        console.log('file is ' + JSON.stringify(file));       
        console.dir("Source : "+file);
        var uploadUrl = "/kode/users/profile/picture/save";
        var fd = new FormData();
        fd.append('file', file);
        console.log("fd : "+fd);
        /*$http
		.post('/kode/users/profile/picture/save',
				fd,{headers: {'Content-Type': undefined}
				})
		.success(
		)
		.error(
		);*/
        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,        	
            headers: {'Content-Type': undefined}
        })
        .success(function(data){
        })
        .error(function(data){
        	
        });
    };

});

var _validFileExtensions = ["jpg", "jpeg", "bmp", "gif", "png"];
function validateProfilePicType(file) {
    var fileType = file.type;
    if (fileType.length > 0) {
    	console.log('fileType is ' + fileType);    	
        var validFileType = false;
        for (var j = 0; j < _validFileExtensions.length; j++) {
            var sCurExtension = _validFileExtensions[j];
            if (endsWith(fileType,sCurExtension)) {
            	validFileType = true;
                break;
            }
        }
        if (!validFileType) {
            alert("Sorry, " + fileType + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
            return false;
        }
    }

    return true;
}
function endsWith(str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
}


/*
 * myprofileApp.controller('SideBarController', function($scope,$route) {
 * 
 * });
 */