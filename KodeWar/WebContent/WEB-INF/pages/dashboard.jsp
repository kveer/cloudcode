<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<div class="cont">
	<div class="content">
		<aside class="pull-left l-sidebar">
			<nav class="nav-left-col">
				<nav class="nav-nested">
					<!--
				For highlighting the current active tab ... 
				http://jsfiddle.net/pkozlowski_opensource/KzAfG/ -->
					<a href="#/questions" ng-class="{ active: isActive('/questions')}">
						Questions </a> <a href="#/sessions"
						ng-class="{ active: isActive('/sessions')}"> Sessions </a>
				</nav>
			</nav>
		</aside>
		<section class="pull-right l-main-col">
			<div ng-view></div>
		</section>
	</div>
</div>