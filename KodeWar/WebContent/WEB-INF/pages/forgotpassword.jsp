<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!-- Container -->
<script type="text/javascript">
	jQuery.validator.setDefaults({
		debug : true,
		onfocusout : false,
		onkeyup : false,
		onclick : false
	});
	$(function() {
		$("#forgotpassword-form").validate({
			// Specify the validation rules
			rules : {
				email : "required",
			},

			// Specify the validation error messages
			messages : {
				email : "Please enter a valid email address",
			},

			errorPlacement : function(error, element) {
				error.insertBefore(element.parent());
			},

			submitHandler : function(form) {
				form.submit();
			}
		});
	});
</script>
<div class="cont">
	<div class="content">
		<div class="centerArea">
			<h2>Account Help</h2>


			<c:if test="${not empty error}">
				<div class="error">${error}</div>
			</c:if>

			<form name='forgotpassword-form' id='forgotpassword-form' action="forgotpassword"
				method='POST' class="registerForm">
				<p>
					<label>Please enter your email address</label>
					<input type="text" name="email" />
				</p>
				<p>
					<input type="submit" value="Continue" />
				</p>
			</form>
		</div>
	</div>
</div>