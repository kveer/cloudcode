<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

	<sec:authorize access="isAuthenticated()">
		<div class="global">
			<div class="cont">
				<ul class="nav">
					<li class="userprofile"><a href="/kode/account">${username}</a></li>
					<li class="signout"><a href="/kode/logout">log out</a></li>
				</ul>
			</div>
		</div>
	</sec:authorize>
	<sec:authorize access="isAnonymous()">
		<div class="global">
			<div class="cont">
				<ul class="nav">
					<li class="signin"><a href="/kode/login">log in</a></li>
					<li class="signout"><a href="/kode/register">register</a></li>
				</ul>
			</div>
		</div>
	</sec:authorize>
	
	
	<div class="header">
		<div class="cont">
			<div class="brand">
				<!-- <a href="/kode/home" class="logo"><img alt="Careers 2.0" src="http://cdn-careers.sstatic.net/careers/Img/logos/careers-logo.png?v=4daa70d5b71e"></a> -->
				<a href="/kode/home" class="logo">CloudKode</a>
			</div>
			<ul class="nav options">
				<li><a class="" href="/kode/questions/ask">ask question</a></li>
				<li><a class="" href="/kode/mentors">mentors</a></li>
				<li><a class="" href="/kode/dashboard">dashboard</a></li>
			</ul>
		</div>
	</div>