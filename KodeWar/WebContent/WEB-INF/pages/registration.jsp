<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!-- Container -->
<script type="text/javascript">
	jQuery.validator.setDefaults({
		debug : true,
		onfocusout : false,
		onkeyup : false,
		onclick : false
	});
	$(function() {
		// Setup form validation on the #register-form element
		$("#register-form").validate({
			// Specify the validation rules
			rules : {
				email : {
					required : true,
					email : true
				},
				password : "required",
				confirmpassword : {
					required : true,
					equalTo : "#password"
				}
				/*
				http://stackoverflow.com/questions/21257814/form-validation-using-jquery
					http://www.sitepoint.com/basic-jquery-form-validation-tutorial/
				email: {
				    required: true,
				    email: true
				},
				password: {
				    required: true,
				    minlength: 5
				} ,
				"Confirm_Password":{
					required:true,
					equalTo: "#password"
					},
					"email":{
					required:true,  
					}
				 */,
			},

			// Specify the validation error messages
			messages : {
				email : "Please provide a valid email for your account <span class='glyphicon glyphicon-hand-down'></span>",
				password : "Please enter a password <span class='glyphicon glyphicon-hand-down'></span>",
				confirmpassword : {
					required : "This should be same as the password entered above",
					equalTo : "This should be same as the password entered above"
				}
			},

			errorPlacement : function(error, element) {
				error.insertBefore(element.parent());
			},

			submitHandler : function(form) {
				form.submit();
			}
		});
	});
</script>
<div class="cont">
	<div class="content">
		<div class="centerArea register">
			<h2>Create Account</h2>


			<c:if test="${not empty error}">
				<div class="error">${error}</div>
			</c:if>

			<form:form name='register-form' id='register-form' action="register"
				method='POST' commandName="user" class="registerForm">
				<form:errors path="email" cssClass="error" />
				<p>
					<label for="email">Email Address:</label>
					<form:input path="email" />
				</p>
				<form:errors path="password" cssClass="error" />
				<p>
					<label for="password">Password:</label>
					<form:password path="password" />
				</p>

				<form:errors path="confirmpassword" cssClass="error" />
				<p>
					<label for="confirmpassword">Confirm Password:</label>
					<form:password path="confirmpassword" />
				</p>
				<p>
					<input type="submit" value="Create Account" />
				</p>
			</form:form>
		</div>
	</div>
</div>