<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<div class="cont" ng-app="profileapp"
	ng-controller="editProfileController">
	<div class="content profileView">
		<pre>
			{{user}}
			<br>
			<!-- <br>
			{{editableUser}} -->
		</pre>
		<div id="flash-notice" ng-show="flashnotice">
			<div class="flash-notice">
				<span ng-click="close()"
					class="glyphicon glyphicon-remove flash-close"></span>
				{{flashmessage}}
			</div>
		</div>
		<div class="profile">
			<div class="profileLeftArea">
				<div ng-hide="editable == 'profilepicture'"
					style='padding-bottom: 30px'>
					<a style="float: right" href="#" ng-click="editable = 'profilepicture'">edit</a>					
					<img src="http://www.usna.edu/Users/math/meh/Ramanujan.jpeg"
						height="60px" />
				</div>
				<div ng-show="editable == 'profilepicture'" style='padding-bottom: 30px'>
					<input type="file" file-model="profilepicture"/>
					<button ng-click="uploadFile()">upload</button>
				</div>
				<div style='padding-top: 3px; padding-bottom: 10px'>
					<p>Rate : 15$ for 30 mins</p>
				</div>
				<div id="actions">
					<div class="action">
						<!--This is a security issue as users can change this and post a message to someonelse  -->
						<a href="/kode/message/2" id="messages" onclick="return false;"
							style="padding-left: 0px">Message me</a>
					</div>
					<div class="action">
						<a href="/kode/questions/save?mentor=sramanujan" id="postquestion"
							style="padding-left: 0px">Schedule a Session</a>
					</div>
				</div>
			</div>
			<%-- <div class="profileRightArea">
				<h2>${mentor.firstname}${mentor.lastname}</h2>
				<p>${mentor.profile.about}</p>
				<h2 class="subTitle">Skills</h2>
				<c:forEach var="skill" items="${mentor.profile.skills}">
					<div class="skillRow">
						<div class="skillName">${skill.skillname}</div>
						<div class="skillInfo">
							<p>${skill.skillexpertise}</p>
							<p class="skillExp">${skill.experience}years</p>
						</div>
					</div>
				</c:forEach>
			</div> --%>
			<div class="profileRightArea" style="">
				<div ng-hide="editable == 'personal'">
					<a style="float: right" href="#" ng-click="editable = 'personal'">edit</a>
					<div>
						<h1
							style="color: #fd8008; font-weight: normal; font-size: 168%; line-height: 100%; margin: 0 0 6 0;">{{editableUser.name}}</h1>
						<a href="http://urahero.wordpress.com/">{{editableUser.profile.website}}</a>
					</div>
					<div class="headline" style="margin-top: 23px;">
						<p>{{editableUser.profile.headline}}</p>
					</div>
					<p
						style="margin-top: 26px; font-family: Georgia, Times New Roman, Serif; opacity: .9; background-color: #f1f8ff; padding: 18px;">{{editableUser.profile.about}}
					</p>
				</div>
				<div ng-show="editable == 'personal'">
					<form ng-submit="savePrimary()"
						style="background-color: blanchedalmond;">

						<p>
							<label for="name">name</label> <input name="name" id="name"
								type="text" ng-model="editableUser.name"
								value="Srinivasa
							Ramanujan">
						</p>
						<p>
							<label for="headline">headline</label> <input name="headline"
								id="headline" type="text"
								ng-model="editableUser.profile.headline">
						</p>
						<p>
							<label for="about">about</label>
							<textarea name="about" id="about"
								ng-model="editableUser.profile.about" rows="8" cols="70"></textarea>
						</p>
						<p>
							<input type="submit" value="save changes" /> <input
								type="button" value="cancel" ng-click="cancel()" />
						</p>

					</form>
				</div>
				<div class="section">
					<h2>Technologies</h2>
					<div ng-hide="editable == 'skills'">
						<a style="float: right" href="#" ng-click="editable = 'skills'">edit</a>
						<!-- <div id="tags_tagsinput" class="tagsinput tagsview" style="" >
							<span class="tag"
								style="color: #3e6d8e; background-color: #e0eaf1; border: initial;padding: 6px;"><span>machine</span></span>
							<span class="tag"
								style="color: #3e6d8e; background-color: #e0eaf1; border: initial;padding: 6px;"><span>learning</span></span>
						</div> -->
						<div>
							<span class="tag"
								style="color: #3e6d8e; background-color: #e0eaf1; border: initial; padding: 6px; margin-right: 5px"
								ng-repeat="skill in editableUser.profile.skills"><span>{{skill.skillname}}</span></span>
						</div>
					</div>
					<div ng-show="editable == 'skills'">
						<form ng-submit="saveSkills()"
							style="background-color: blanchedalmond;">
							<div>
								<tags-input ng-model="editableUser.profile.skills"
									display-property="skillname"></tags-input>
							</div>
							<p>
								<input type="submit" value="update skills" /> <input
									type="button" value="cancel" ng-click="cancel()" />
							</p>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>