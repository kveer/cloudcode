<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<script type="text/javascript">
	jQuery.validator.setDefaults({
		debug : true,
		onfocusout : false,
		onkeyup : false,
		onclick : false
	});
	$(function() {
		$("#datepicker").datepicker({
			onClose : function() {
				$(this).focusout();
			}
		});

		$('#tags').tagsInput({
			'height' : '52px',
			'width' : '515px'
		});
		// Setup form validation on the #register-form element
		$("#question-form").validate({
			// Specify the validation rules
			rules : {
				title : "required",
				date : "required",
				tags : "required"
			},

			// Specify the validation error messages
			messages : {
				title : "Please provide a title for the question",
				date : "Enter a tentative date",
				tags : "Tag this question"
			},

			errorPlacement : function(error, element) {
				error.insertBefore(element.parent());
			},

			submitHandler : function(form) {
				form.submit();
			}
		});

	});
</script>
<div class="cont">
	<div class="content question-content">
		<div class="question-area">
			<h2>Question Details</h2>

			<form:form name='question-form' id='question-form' action="${action}"
				method='POST' commandName="question">
				<form:errors path="title" cssClass="error" />
				<p>
					<label for="title">Title:</label>
					<form:input path="title" />

				</p>
				<p>
					<label for="kindofhelp" class="kindofhelp">Kind of Help
						(Optional):</label>
					<form:textarea path="kindofhelp" rows="10" cols="72" value="" />
				</p>
				<%-- <p>
						<label for="timezone">TimeZone:</label> <select
							id="appointment_time_zone" name="timezone">
							<option value="America/Dawson_Creek	">(GMT-07:00)
								America/Dawson_Creek</option>
							<option value="America/Denver">(GMT-07:00)
								America/Denver</option>
							<option value="Asia/Kolkata">(GMT+05:30) Asia/Kolkata</option>
							<option value="Europe/Dublin">(GMT+00:00) Europe/Dublin</option>
							<option value="Africa/Algiers">(GMT+01:00)
								Africa/Algiers</option>
							<option value="Africa/Gaborone">(GMT+02:00)
								Africa/Gaborone</option>
						</select> <span class="hint"><form:errors path="kindofhelp"
								cssClass="error" /></span>
					</p> --%>
				<form:errors path="date" cssClass="error" />
				<p>
					<label for="date">Date:</label>
					<form:input id="datepicker" path="date" />
				</p>
				<p>
					<label for="time">Time:</label>
					<form:select path='time' style='width: 120px;'>
						<form:option value='00:00'>12:00 AM</form:option>
						<form:option value='00:30'>12:30 AM</form:option>
						<form:option value='01:00'>1:00 AM</form:option>
						<form:option value='01:30'>1:30 AM</form:option>
						<form:option value='02:00'>2:00 AM</form:option>
						<form:option value='02:30'>2:30 AM</form:option>
						<form:option value='03:00'>3:00 AM</form:option>
						<form:option value='03:30'>3:30 AM</form:option>
						<form:option value='04:00'>4:00 AM</form:option>
						<form:option value='04:30'>4:30 AM</form:option>
						<form:option value='05:00'>5:00 AM</form:option>
						<form:option value='05:30'>5:30 AM</form:option>
						<form:option value='06:00'>6:00 AM</form:option>
						<form:option value='06:30'>6:30 AM</form:option>
						<form:option value='07:00'>7:00 AM</form:option>
						<form:option value='07:30'>7:30 AM</form:option>
						<form:option value='08:00'>8:00 AM</form:option>
						<form:option value='08:30'>8:30 AM</form:option>
						<form:option value='09:00'>9:00 AM</form:option>
						<form:option value='09:30'>9:30 AM</form:option>
						<form:option value='10:00'>10:00 AM</form:option>
						<form:option value='10:30'>10:30 AM</form:option>
						<form:option value='11:00'>11:00 AM</form:option>
						<form:option value='11:30'>11:30 AM</form:option>
						<form:option value='12:00'>12:00 PM</form:option>
						<form:option value='12:30'>12:30 PM</form:option>
						<form:option value='13:00'>1:00 PM</form:option>
						<form:option value='13:30'>1:30 PM</form:option>
						<form:option value='14:00'>2:00 PM</form:option>
						<form:option value='14:30'>2:30 PM</form:option>
						<form:option value='15:00'>3:00 PM</form:option>
						<form:option value='15:30'>3:30 PM</form:option>
						<form:option value='16:00'>4:00 PM</form:option>
						<form:option value='16:30'>4:30 PM</form:option>
						<form:option value='17:00'>5:00 PM</form:option>
						<form:option value='17:30'>5:30 PM</form:option>
						<form:option value='18:00'>6:00 PM</form:option>
						<form:option value='18:30'>6:30 PM</form:option>
						<form:option value='19:00'>7:00 PM</form:option>
						<form:option value='19:30'>7:30 PM</form:option>
						<form:option value='20:00'>8:00 PM</form:option>
						<form:option value='20:30'>8:30 PM</form:option>
						<form:option value='21:00'>9:00 PM</form:option>
						<form:option value='21:30'>9:30 PM</form:option>
						<form:option value='22:00'>10:00 PM</form:option>
						<form:option value='22:30'>10:30 PM</form:option>
						<form:option value='23:00'>11:00 PM</form:option>
						<form:option value='23:30'>11:30 PM</form:option>
					</form:select>
				</p>
				<form:errors path="tags" cssClass="error" />
				<p>
					<label for="tags" class="tags">Tags:</label>
					<form:input path="tags" id="tags" value="" />
				</p>
				<p>
					<input type="submit" value="submit" />
				</p>
			</form:form>
		</div>
	</div>
</div>