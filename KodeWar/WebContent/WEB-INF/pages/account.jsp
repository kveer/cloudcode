<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<div class="cont" ng-app="myaccountApp"  ng-controller="profileDetailsController">
	<div class="content">
		<aside class="pull-left l-sidebar">
			<nav class="nav-left-col">
				<nav class="nav-nested">
					<a href="#/profile" ng-class="{ active: isActive('/profile')}">
						Profile Details </a> <a href="#/billing"
						ng-class="{ active: isActive('/billing')}"> Billing </a>
				</nav>
			</nav>
		</aside>
		<section class="pull-right l-main-col">
			<div ng-view></div>
		</section>
	</div>
</div>