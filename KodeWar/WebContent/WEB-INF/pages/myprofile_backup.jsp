<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<!-- <script type="text/javascript">
	$(function() {
		$('#questionpoolbytag').attr('onclick', 'return false;').click(
				function(e) {
					e.preventDefault();
					var tag = $(this).attr('data-tag');
					$.ajax({
						type : "GET",
						url : '/kode/questions/tags/' + tag,
						headers : {
							"Accept" : "application/json",
							"Content-Type" : "application/json"
						},
						error : error,
						complete : showQuestions
					});
				});
		$("#questions").on("change", "#action", function() {
			var qid = $(this).attr('data-qid');
			var flag;
			if (!$(this).is(':checked')) {
				flag = 'cancel';
				alert('unchecked' + flag);
			} else {
				flag = 'confirm';
				alert('checked' + flag);
			}
			$.ajax({
				type : "POST",
				url : '/kode/sessions/' + qid + '/action/' + flag,
				headers : {
					"Accept" : "application/json",
					"Content-Type" : "application/json"
				},
				error : error,
				complete : showQuestions
			});
		});

		$("#sessionsaction").attr('onclick', 'return false;').click(
				function(e) {
					e.preventDefault();
					$.ajax({
						type : "GET",
						url : '/kode/sessions/user',
						headers : {
							"Accept" : "application/json",
							"Content-Type" : "application/json"
						},
						error : error,
						complete : showSessions
					});
					defaultNavActions();
					highlightNavAction("#sessionsaction");
				});
		$("#questionpoolaction").attr('onclick', 'return false;').click(
				function(e) {
					e.preventDefault();
					$.ajax({
						type : "GET",
						url : '/kode/questions',
						headers : {
							"Accept" : "application/json",
							"Content-Type" : "application/json"
						},
						error : error,
						complete : showQuestions
					});
					defaultNavActions();
					highlightNavAction("#questionpoolaction");
				});

		$("#sessions").on("click", "#opensession", function(e) {
			e.preventDefault();
			var sid = $(this).attr('data-sid');
			window.open("/kode/opensession/" + sid, "popupWindow");
		});

		$.ajax({
			type : "GET",
			url : '/kode/questions',
			headers : {
				"Accept" : "application/json",
				"Content-Type" : "application/json"
			},
			error : error,
			complete : showQuestions
		});

	});
	function error() {
		alert('error');
	}
	function qConfirmSuccess() {
		alert('success');
	}
	function highlightNavAction(handle) {
		$(handle).css({
			color : '#545452',
			"font-weight" : 'normal'
		});
		$(handle + "div").addClass("actionHightlight");
	}
	function defaultNavActions() {
		$(".action").removeClass("actionHightlight");
		$(".actionRef").css({
			color : "orange",
			"font-weight" : 'bold'
		});
	}

	function showQuestions(result) {
		//alert(JSON.stringify(result.responseText));
		var resp = eval('(' + result.responseText + ')');
		//alert(resp.length);
		$('#questionpool').css("display", "block");
		$('#sessionpool').css("display", "none");
		$('#questions').empty();
		$
				.each(
						resp,
						function(idx, obj) {
							//alert(obj.title);
							var question = '<div id="questionRow" class="questionRow"><div id="questionTitle" class="questionTitle"><b style="color: #07c">Title</b> :'
									+ obj.title
									+ '</div>'
									+ '<div id="questionField" class="questionField"><b style="color: #07c">Date</b> :'
									+ obj.date
									+ '</div>'
									+ '<div id="questionField" class="questionField"><b style="color: #07c">Tags</b> :'
									+ obj.tags
									+ '</div><div id="questionField" class="questionField"><b style="color: #07c">Confirm for a Session</b> <input type="checkbox" value="confirm" data-qid="'+obj.qid+'" id="action" name="action"/> </div></div>';
							$(question).appendTo('#questions');
						});
	}
	function showSessions(result) {
		//alert(result);
		if (result.getResponseHeader('REQUIRES_AUTH') === '1') {
			window.location.href = '/kode/login';
		}
		//alert(JSON.stringify(result.responseText));
		var resp = eval('(' + result.responseText + ')');
		//alert(resp.length);
		$('#questionpool').css("display", "none");
		$('#sessionpool').css("display", "block");
		$('#sessions').empty();
		$
				.each(
						resp,
						function(idx, session) {
							//alert(obj.title);
							var question = '<div class="questionRow"><div class="questionField"><b style="color: #07c">User</b> :'
									+ session.mentoruid
									+ '</div>'
									+ '<div class="questionTitle"><b style="color: #07c">Title</b> :'
									+ session.question.title
									+ '</div>'
									+ '<div class="questionField"><b style="color: #07c">Date</b> :'
									+ session.sessiontime
									+ '</div>'
									+ '<div class="questionField"><b style="color: #07c">Tags</b> :'
									+ session.question.tags
									+ '</div><div class="questionField"><b style="color: #07c">Status :</b> '
									+ session.status
									+ '</div>'
									+ '<div class="questionField"><a href="/kode/questions" id="opensession" data-sid="'+session.sid+'" target="_blank" style="font-size: 16px; font-weight: bold; color: orange; padding-left: 20px;">Open Session</a></div></div>';
							$(question).appendTo('#sessions');
						});
	}
</script>
 -->
<div class="cont">
	<div class="content">
		<aside class="pull-left l-sidebar">
			<nav class="nav-left-col">
				<nav class="nav-nested">
					<!-- 						<a href="#!/inbox/applications"> Applications </a> <a
							href="#!/inbox/messages"> Messages </a> -->
					<a href="#!/inbox/personaldetails"> Personal Details </a>
					<a href="#!/inbox/billing"> Billing </a>
				</nav>
			</nav>
		</aside>
		<section class="pull-right l-main-col">



			<%-- <section class="question-list">
					<article class="question-entry">
						<p class="question-field" style="">

							<span class="question-title" style="font-weight: bold;">Webrtc
								Video Demo using Data Channels</span>
						</p>


						<p class="question-field">
							<span>status : </span> <span class="question-title">confirmed</span>
							<span style="margin-left: 270px">when : </span> <span
								class="question-title">June 12, 2014</span>
						</p>



						<div style="margin-top: 13px; margin-left: 0px;">
							<span><a href="#"
								style="color: #538dc2; margin-right: 4px">view</a></span> <span
								style="color: #ccc; font-weight: normal;"> &middot; </span><span><a
								href="#" style="color: #538dc2;">update</a></span>
						</div>
					</article>

					<article class="question-entry">
						<p class="question-field" style="">

							<span class="question-title" style="font-weight: bold;">Webrtc
								Video Demo using Data Channels</span>
						</p>


						<p class="question-field">
							<span>status : </span> <span class="question-title">confirmed</span>
							<span style="margin-left: 270px">when : </span> <span
								class="question-title">June 12, 2014</span>
						</p>



						<div style="margin-top: 13px; margin-left: 0px;">
							<span><a href="#"
								style="color: #538dc2; margin-right: 4px">view</a></span> <span
								style="color: #ccc; font-weight: normal;"> &middot; </span><span><a
								href="#" style="color: #538dc2;">update</a></span>
						</div>
					</article>
				</section> --%>
			<section class="question-list">
				<article class="question-entry">
					<p class="question-field" style="">

						<span class="question-title" style="font-weight: bold;">Webrtc
							Video Demo using Data Channels</span>
					</p>


					<p class="question-field">
						<span>status : </span> <span class="question-title">confirmed</span>
						<span style="margin-left: 270px">when : </span> <span
							class="question-title">June 12, 2014</span>
					</p>



					<div style="margin-top: 13px; margin-left: 0px;">
						<span><a href="#" style="color: #538dc2; margin-right: 4px">view</a></span>
						<span style="color: #ccc; font-weight: normal;"> &middot; </span><span><a
							href="#" style="color: #538dc2;">update</a></span>
					</div>
				</article>

				<article class="question-entry">
					<p class="question-field" style="">

						<span class="question-title" style="font-weight: bold;">Webrtc
							Video Demo using Data Channels</span>
					</p>


					<p class="question-field">
						<span>status : </span> <span class="question-title">confirmed</span>
						<span style="margin-left: 270px">when : </span> <span
							class="question-title">June 12, 2014</span>
					</p>



					<div style="margin-top: 13px; margin-left: 0px;">
						<span><a href="#" style="color: #538dc2; margin-right: 4px">view</a></span>
						<span style="color: #ccc; font-weight: normal;"> &middot; </span><span><a
							href="#" style="color: #538dc2;">update</a></span>
					</div>
				</article>
			</section>

		</section>

	</div>
</div>


<%-- <div class="containerWide">

		<div class="leftHandle">

			<div class="action">
				<a href="/kode/question" id="postquestion" class="actionRef">Post
					a Question</a>
			</div>
			<div class="action">
				<a href="/kode/question" id="postquestion" class="actionRef">Questions</a>
			</div>
			<c:if test="${mentor}">
				<div class="action actionHightlight" id="questionpoolactiondiv">
					<a href="/kode/questions" id="questionpoolaction"
						class="actionRef actionRefClicked " onclick="return false;">Questions
						in Pool</a>
				</div>
			</c:if>
			<div class="action" id="sessionsactiondiv">
				<a href="/kode/sessions/user" id="sessionsaction" class="actionRef"
					onclick="return false;">Sessions</a>
			</div>
		</div>

		<div id='questionpool'>

			<div style='padding: 15px;'>
				<b>Filter by Tag</b> <a href="/kode/questions/tags/java"
					data-tag='java' id='questionpoolbytag'
					style="font-size: 16px; font-weight: bold; color: #07c; padding-left: 20px;">java</a>
			</div>
			<p>Questions</p>
			<div id='questions'></div>

		</div>
		<!-- Question Pool End -->
		<div id='sessionpool' style='display: none; float: left; width: 900;'>

			<div style='padding: 15px;'>
				<b>Filter by Status</b> <a href="/kode/sessions/status/"
					data-tag='confirm'
					style="font-size: 16px; font-weight: bold; color: #07c; padding-left: 20px;">open</a>
			</div>
			<p>Sessions</p>
			<div id='sessions'></div>

		</div>
		<!-- Question Pool End -->


</div> --%>