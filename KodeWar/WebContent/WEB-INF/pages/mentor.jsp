<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<div class="cont">
	<div class="content profileView">
		<div class="profile">
			<div class="profileLeftArea">
				<div style='padding-bottom: 30px'>
					<img src="${mentor.profile.gravatar}" height="60px" />
				</div>
				<div style='padding-top: 3px; padding-bottom: 10px'>
					<p>Rate : ${mentor.profile.rate}$ for 15 mins</p>
				</div>
				<div id="actions">
					<div class="action">
						<!--This is a security issue as users can change this and post a message to someonelse  -->
						<a href="/kode/message/${mentor.uid}" id="messages"
							onclick="return false;" style="padding-left: 0px">Message me</a>
					</div>
					<div class="action">
						<a href="/kode/questions/save?mentor=${mentor.profilehandle}"
							id="postquestion" style="padding-left: 0px">Schedule a
							Session</a>
					</div>
				</div>
			</div>
			<%-- <div class="profileRightArea">
				<h2>${mentor.firstname}${mentor.lastname}</h2>
				<p>${mentor.profile.about}</p>
				<h2 class="subTitle">Skills</h2>
				<c:forEach var="skill" items="${mentor.profile.skills}">
					<div class="skillRow">
						<div class="skillName">${skill.skillname}</div>
						<div class="skillInfo">
							<p>${skill.skillexpertise}</p>
							<p class="skillExp">${skill.experience}years</p>
						</div>
					</div>
				</c:forEach>
			</div> --%>
			<div class="profileRightArea" style="">
				<div>
					<h1
						style="color: #fd8008; font-weight: normal; font-size: 168%; line-height: 100%; padding-bottom: 10px;">Srinivasa
						Ramanujan</h1>
					<a href="http://urahero.wordpress.com/">urahero.wordpress.com</a>
				</div>
				<div class="headline" style="margin-top: 23px;">
					<p>Mathematician whom you worship</p>
				</div>
				<p
					style="margin-top: 26px; font-family: Georgia, Times New Roman, Serif; opacity: .9; background-color: #f1f8ff; padding: 18px;">
					Srinivasa Ramanujan was an Indian mathematician and autodidact who,
					with almost no formal training in pure mathematics, made
					extraordinary contributions to mathematical analysis, number
					theory. <b>dasdada<b></b></b>
				</p>
				<div class="section">
					<h2>Technologies</h2>
					<div>
						<div id="tags_tagsinput" class="tagsinput tagsview" style="">
							<span class="tag"
								style="color: #3e6d8e; background-color: #e0eaf1; border: initial;"><span>machine</span></span>
							<span class="tag"
								style="color: #3e6d8e; background-color: #e0eaf1; border: initial;"><span>learning</span></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>