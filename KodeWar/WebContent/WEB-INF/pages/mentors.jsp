<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<div class="cont">
	<div class="content home">
		<h2>Experts</h2>
		<c:forEach var="mentor" items="${mentors}" varStatus="loopStatus">
			<div class="profileArea ${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
				<div class="profileImage">
					<img src="${mentor.profile.gravatar}" height="60px" />
				</div>
				<div class="profileInfo">
					<p>
						<!-- <h3 class="profileName">${profile.firstname}</h3> -->
						<a href="/kode/${mentor.profilehandle}"
							style="font-size: 16px; font-weight: bold; color: #07c">${mentor.name}</a>
					</p>
					<p>${mentor.profile.headline}</p>
					<p class="profileSkill">${mentor.profile.briefSkills}</p>
					<p>${mentor.profile.about}</p>
				</div>
			</div>
		</c:forEach>
	</div>
</div>