<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!-- Container -->
<script type="text/javascript">
	jQuery.validator.setDefaults({
		debug : true,
		onfocusout : false,
		onkeyup : false,
		onclick : false
	});
	$(function() {
		// Setup form validation on the #register-form element
		$("#changepassword-form").validate({
			// Specify the validation rules
			rules : {
				password : "required",
				confirmpassword : {
					required : true,
					equalTo : "#password"
				},
			},

			// Specify the validation error messages
			messages : {
				password : "Please enter a password",
				confirmpassword : {
					required : "Please enter the same password as above",
					equalTo : "Please enter the same password as above"
				}
			},

			errorPlacement : function(error, element) {
				error.insertBefore(element.parent());
			},

			submitHandler : function(form) {
				form.submit();
			}
		});

	});
</script>
<div class="cont">
	<div class="content">
		<div class="centerArea">
			<h2>Set your Password</h2>


			<c:if test="${not empty error}">
				<div class="error">${error}</div>
			</c:if>

			<form name='changepassword-form' id='changepassword-form' action="/kode/changepassword"
				method='POST' class="registerForm">
				<input type="hidden" name="vt" value='${vt}'/>
				<p>
					<label for="password">Password:</label>
					<input type="password" name="password" id="password"/>
				</p>
				<p>
					<label for="confirmpassword">Confirm Password:</label>
					<input type="password" name="confirmpassword" id="confirmpassword" />
				</p>
				<p>
					<input type="submit" value="Confirm" />
				</p>
			</form>
		</div>
	</div>
</div>