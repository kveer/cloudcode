<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<script type="text/javascript">
	$(function() {
		$('#tags').tagsInput({
			'height' : '52px',
			'width' : '515px'
		});
	});
</script>
<div class="cont">
	<div class="content question-content">
		<div class="question-area">
			<h2>Question Details</h2>

			<div class="questiondetails-area">

				<p>
					<label for="title" class="Entry">Title:</label> <label
						class="EntryContent">${question.title}</label>

				</p>
				<p>
					<label for="kindofhelp" class="kindofHelp">Kind of Help</label>
				<div>${question.kindofhelp}</div>
				</p>




				<p>
					<label for="date" class="Entry">When:</label> <label
						class="EntryContent">${question.date} ${question.time}</label>
				</p>
				<p>
					<label for="time" class="Entry">Status:</label> <label
						class="EntryContent">${question.status}</label>
				</p>

				<div>

					<label for="tags" class="Entry"
						style="display: inline-block; text-align: start; vertical-align: top;">Tags:</label>

					<div id="tags_tagsinput" class="tagsinput tagsview"
						style="display: inline-block;">
						<c:forEach var="tag" items="${tags}">
							<span class="tag"><span>${tag}</span></span>
						</c:forEach>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>