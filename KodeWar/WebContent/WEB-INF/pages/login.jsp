<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- <body onload='document.loginForm.email.focus();'> -->

<!-- Container -->
<div class="cont">
	<div class="content">
		<div class="centerArea login">
			<h2>
				Log in
			</h2>
			<c:if test="${not empty error}">
				<div class="error">
					There seems to be some issue with either the Email or Password.
					<%-- ${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message} --%>
				</div>
			</c:if>

			<form name='loginForm' action="/auth" method='POST' class="loginForm">

					<p>
						<!-- <label>Email:</label> -->
						<!-- <input type='text' name='email' value='' size='25' placeholder="Email Address" value="koder3@koder.com"> -->
						<input type='text' name='email' maxlength='256'  >
					</p>
					<p>
						<!-- <label>Password:</label> -->
						<!-- <input type='password' name='password' size='25' placeholder="Password" value="123456"/> -->
						<input type='password' name='password'/>
					</p>
					<p>
						<input name="submit" type="submit"
							value="log in" />
					</p>

			</form>
			<p>
				<a class="" href="/kode/register">Create a new account �</a>
			</p>
			<p>
				<a class="" href="/kode/forgotpassword">Forgot Password �</a>
			</p>
		</div>
	</div>
</div>