<%@ page errorPage="errorpage.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
<title>Mentor Application</title>
<link rel="stylesheet" type="text/css"
	href="http://xoxco.com/projects/code/tagsinput/jquery.tagsinput.css" />
<script type="text/javascript"
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
<script type="text/javascript"
	src="http://xoxco.com/projects/code/tagsinput/jquery.tagsinput.js"></script>
<script type='text/javascript'
	src='https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/jquery-ui.min.js'></script>
<link rel="stylesheet" type="text/css"
	href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/themes/start/jquery-ui.css" />
<script src="/js/firepad.js"></script>
  	<link rel="stylesheet" href="/css/kode.css" />
<script type="text/javascript">
$(function() {

	$('#tags').tagsInput({
		width : 'auto',
	});
});
</script>
</head>
<body>
	<!-- Header -->	
	<div class="header">
		<b style="color: Orange">CloudKode</b>
	</div>
	
	
	<!-- Container -->
	<div class="container">
	
	<h3>Mentor Application Step 1</h3>

	<form:form name='f' action="/kode/mentor/application/create" commandName="application" method='POST'>

		<table>
			<tr>
				<td>Step 1</td>
			</tr>
			<%-- <tr>
				<td>First Name: *</td>
				<td><form:input path="firstname" /></td>
				<td><form:errors path="firstname" cssClass="error" /></td>
			</tr>
			
			<tr>
				<td>Last Name: *</td>
				<td><form:input path="lastname" /></td>
				<td><form:errors path="lastname" cssClass="error" /></td>
			</tr> --%>
			<tr>
				<td>email: * </td>
				<td><form:input path="email" /></td>
				<c:if test="${not empty requestScope.emailAlreadyRegisteredError}">
					<td>${requestScope.emailAlreadyRegisteredError}</td>
				</c:if>
				<td><form:errors path="email" cssClass="error" /></td>
			</tr>
			<%-- <tr>
				<td>headline for your profile: *</td>
				<td><form:input path="headline" /></td>
				<td>Ex : Full Stack Javascript Developer </td>
				<td><form:errors path="headline" cssClass="error" /></td>				
			</tr> --%>
			<%-- <tr>
				<c:forEach var="skill" items="${requestScope.skills}">
					<td>${skill.value}: <form:checkbox path="skills" value='${skill.key}' />
					</td>
				</c:forEach>
				<td><form:errors path="skills" cssClass="error" /></td>				
				
			</tr> --%>
			<tr>
			<td>Skills :</td>
			<td><form:input path="tags" id="tags" class="tags" value="" /></td>
			</tr>
			<tr>
				<td colspan='2'><input name="submit" type="submit"
					value="submit" /></td>
			</tr>
		</table>

	</form:form>
	</div>
</body>
</html>