<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<html>
<head>
<title>Mentor Activation Page</title>
<link rel="stylesheet" href="/css/kode.css" />
</head>
<body>
	<!-- Header -->
	<div class="header">
		<b style="color: Orange">CloudKode</b>
	</div>


	<!-- Container -->
	<div class="container">
		<form:form name='f' action="${requestScope.path}" commandName="application"
			method='POST'>

			<table>
				<tr>
					<td>Please choose a Password:</td>
					<td><form:input path="password" /></td>
					<td><form:errors path="password" cssClass="error" /></td>
				</tr>
				<tr>
					<td>Please choose a public profile name: cloudKode.com/<b>myname</b></td>
					<td><form:input path="profilehandle" /></td>
					<td><form:errors path="profilehandle" cssClass="error" /></td>
				</tr>
				<tr>
					<td colspan='2'><input name="submit" type="submit"
						value="submit" /></td>
				</tr>
			</table>

		</form:form>
	</div>
</body>
</html>