<%@page
	import="org.springframework.security.core.context.SecurityContextHolder"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!-- Container -->
<div class="cont">
	<div class="content">
		<div class="paddedArea">
			<c:choose>
				<c:when test="${not empty errormessage}">
        		<p>${errormessage}</p>
    		</c:when>
				<c:otherwise>
					<p><fmt:message key="error.application" /></p>
				</c:otherwise>
			</c:choose>
			<p>
				<a href="<c:url value="/kode/home" />"> Back to Home</a>
			</p>
		</div>
	</div>
</div>