
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!-- Container -->
<div class="cont">
	<div class="content home">

		<!-- <span style="margin-bottom: 50px; font-weight: 20px"><h2>How
						it works</h2></span> -->
		<h2>How it works</h2>
		<div class="howBar">

			<div class="howStep">
				<div class="howStepIndex">
					<p>1</p>
				</div>
				<div class="howStepContent">
					<p class="howStepContentText">Post a Question on any technical
						issue(Python,Javascript etc.)</p>
				</div>
			</div>

			<div class="howStep">
				<div class="howStepIndex">
					<p>2</p>
				</div>
				<div class="howStepContent">
					<p class="howStepContentText">Our platform will find the expert
						with the needed skill set</p>
				</div>
			</div>

			<div class="howStep">
				<div class="howStepIndex">
					<p>3</p>
				</div>
				<div class="howStepContent">
					<p class="howStepContentText">Live session(screen sharing,video
						chat)between you and the expert</p>
				</div>
			</div>
			<div style="clear: both"></div>
		</div><!-- howBar End -->

		<h2>Experts</h2>
		<div id="profiles">
			<c:forEach var="mentor" items="${mentors}" varStatus="loopStatus">
				<div
					class="profileArea ${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
					<div class="profileImage">
						<img src="${mentor.profile.gravatar}" height="60px" />
					</div>
					<div class="profileInfo">
						<p>
							<!-- <h3 class="profileName">${profile.firstname}</h3> -->
							<a href="${mentor.profilehandle}"
								style="font-size: 16px; font-weight: bold; color: #07c">${mentor.name}</a>
						</p>
						<p>${mentor.profile.headline}</p>
						<p class="profileSkill">${mentor.profile.briefSkills}</p>
						<p>${mentor.profile.about}</p>
					</div>
				</div>
			</c:forEach>
			<a href="/kode/mentors">More Experts&#8230;</a>
		</div><!--profiles End  -->
	</div><!-- content home End -->
</div>