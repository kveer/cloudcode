<%@ page errorPage="errorpage.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<html>
<head>
<title>Set Availability</title>
<script type="text/javascript"
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/jquery-ui.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css"
	href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/themes/start/jquery-ui.css" />

<style>
.global {
	text-align: right;
	color: orange;
	padding: 3px;
}

.header {
	width: 1200px;
	height: 52px;
	margin: 0 auto;
}

.container {
	width: 975px;
	background: #fff;
	display: block;
	margin-left: 188px;
	color: grey;
}

.profileRow {
	font-family: "Times New Roman", Georgia, Serif;
	border-bottom: solid 1px #e9e9e9;
	border-right: 1px solid transparent;
	position: relative;
	padding: 10px;
	box-sizing: border-box;
	height: auto
}

.profileImage {
	float: left;
	padding-right: 10px;
	padding-top: 3px;
}

.profileInfo {
	height: auto;
	overflow: hidden;
}

h3 {
	margin: 0px;
}

p {
	margin: 0px;
	margin-bottom: 10px;
}

p.profileSkill {
	font-style: italic
}

h3.profileName {
	color: #07c
}

.actionTab {
	padding-left: 600px;
	padding-bottom: 10px;
}

.askQuestionAction {
	float: left;
	padding-right: 10px;
}

.availabilityAction {
	float: left;
	padding-right: 10px;
}
</style>
<script type="text/javascript">
	$(function() {
		$("#date").datepicker();
	});
	function setAvail() {
	   alert('hello');
	   var date = $("#date").val();
	   var time = $("#time").val();
	   $('<div style="color:#07c;margin-bottom:7px;border:inset;">'+ date + '  '+ time + '</div>').appendTo('#timeRows');
	   $.ajax({
		   type: "post",
		   url: '/KodeWebWar-1.0-SNAPSHOT/kode/setSlot',
	       contentType: "application/json",
		   data:  JSON.stringify({ date: date, time: time,timezone :  $("#timezone").val() })
		 });
	};
</script>
</head>
<body>
	<!-- Global -->
	<sec:authorize access="isAuthenticated()">
		<div class="global">
			<p>${username}</p>
		</div>
	</sec:authorize>
	<sec:authorize access="isAnonymous()">
		<div class="global">
			<a href="/KodeWebWar-1.0-SNAPSHOT/kode/login" style="color: orange;">
				Signin</a> <a href="/KodeWebWar-1.0-SNAPSHOT/kode/reg"
				style="color: orange;"> Register</a>
		</div>
	</sec:authorize>

	<!-- Header -->
	<div class="header">
		<b style="color: Orange">CloudKode</b>
	</div>


	<!-- Container -->
	<div class="container">
		<div style="float:left;padding-right:400px;">
			<table>
				<tr>
					<td>TimeZone : <select id="timezone"
						name="timezone">
							<option value="America/Dawson_Creek">(GMT-07:00)
								America/Dawson_Creek</option>
							<option value="America/Denver">(GMT-07:00)
								America/Denver</option>
							<option value="Asia/Kolkata">(GMT+05:30) Asia/Kolkata</option>
							<option value="Europe/Dublin">(GMT+00:00) Europe/Dublin</option>
							<option value="Africa/Algiers">(GMT+01:00)
								Africa/Algiers</option>
							<option value="Africa/Gaborone">(GMT+02:00)
								Africa/Gaborone</option>
					</select></td>
				</tr>
				<tr>
					<td>Date: <input id="date" name="date" />
					</td>
				</tr>
				<tr>
					<td>Time : <select name='time' id ='time' style='width: 120px;'>
							<option value='00:00'>12:00 AM</option>
							<option value='00:30'>12:30 AM</option>
							<option value='01:00'>1:00 AM</option>
							<option value='01:30'>1:30 AM</option>
							<option value='02:00'>2:00 AM</option>
							<option value='02:30'>2:30 AM</option>
							<option value='03:00'>3:00 AM</option>
							<option value='03:30'>3:30 AM</option>
							<option value='04:00'>4:00 AM</option>
							<option value='04:30'>4:30 AM</option>
							<option value='05:00'>5:00 AM</option>
							<option value='05:30'>5:30 AM</option>
							<option value='06:00'>6:00 AM</option>
							<option value='06:30'>6:30 AM</option>
							<option value='07:00'>7:00 AM</option>
							<option value='07:30'>7:30 AM</option>
							<option value='08:00'>8:00 AM</option>
							<option value='08:30'>8:30 AM</option>
							<option value='09:00'>9:00 AM</option>
							<option value='09:30'>9:30 AM</option>
							<option value='10:00'>10:00 AM</option>
							<option value='10:30'>10:30 AM</option>
							<option value='11:00'>11:00 AM</option>
							<option value='11:30'>11:30 AM</option>
							<option value='12:00'>12:00 PM</option>
							<option value='12:30'>12:30 PM</option>
							<option value='13:00'>1:00 PM</option>
							<option value='13:30'>1:30 PM</option>
							<option value='14:00'>2:00 PM</option>
							<option value='14:30'>2:30 PM</option>
							<option value='15:00'>3:00 PM</option>
							<option value='15:30'>3:30 PM</option>
							<option value='16:00'>4:00 PM</option>
							<option value='16:30'>4:30 PM</option>
							<option value='17:00'>5:00 PM</option>
							<option value='17:30'>5:30 PM</option>
							<option value='18:00'>6:00 PM</option>
							<option value='18:30'>6:30 PM</option>
							<option value='19:00'>7:00 PM</option>
							<option value='19:30'>7:30 PM</option>
							<option value='20:00'>8:00 PM</option>
							<option value='20:30'>8:30 PM</option>
							<option value='21:00'>9:00 PM</option>
							<option value='21:30'>9:30 PM</option>
							<option value='22:00'>10:00 PM</option>
							<option value='22:30'>10:30 PM</option>
							<option value='23:00'>11:00 PM</option>
							<option value='23:30'>11:30 PM</option>
					</select></td>
				</tr>
				<tr>
					<td colspan='2'><input type="button" id='Add' value="Add" onClick='setAvail()'/></td>
				</tr>
			</table>
		</div>
		<div style="float:left;" id='timeRows'></div>
	</div>
</body>
</html>