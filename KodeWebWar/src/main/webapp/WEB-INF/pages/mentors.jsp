<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
<title>Login Page</title>
<style>
.header {
	width: 975px;
	height: 67px;
	margin: 0 auto;
}

.container {
	width: 975px;
	background: #fff;
	display: block;
	margin-left: 188px;
	color: grey;
}
.profile {
	font-family: "Times New Roman", Georgia, Serif;
	border-bottom: solid 1px #e9e9e9;
	border-right: 1px solid transparent;
	position: relative;
	padding: 10px;
	box-sizing: border-box;
  height:auto
}

.profileImage {
	float: left;
	padding-right: 10px;
  padding-top:13px;
}
.profileInfo {
	height: auto;
  overflow:hidden;
}
h3 {
    margin:0px;
}
p{
    margin:0px;
    margin-bottom:10px;
}

p.profileSkill {
	font-style: italic
}
h3.profileName {
	color: #07c
}
</style>
</head>
<body>
	<div class="header">
		<b style="color: Orange">CloudKode</b>
	</div>
	<div class="container">
		<div>
			<b>Experts : </b>
		</div>

		<c:forEach var="profile" items="${profiles}">
			<div class="profile">
				<div class="profileImage">
					<img src="${profile.gravatar}" height="60px" />
				</div>
				<div class="profileInfo">
					<p >
					<h3 class="profileName">${profile.firstname}</h3>
					</p>
					<p>${profile.headline}</p>
					<p class="profileSkill">${profile.skill}</p>
					<p>${profile.about}</p>
				</div>
			</div>
		</c:forEach>
	</div>
</body>
</html>