<%@ page errorPage="errorpage.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
<title>Login Page</title>
<style>
.global {
	text-align: right;
	color: orange;
	padding: 3px;
}

.header {
	width: 1200px;
	height: 52px;
	margin: 0 auto;
}

.container {
	width: 975px;
	background: #fff;
	display: block;
	margin-left: 188px;
	color: grey;
}

.profileRow {
	font-family: "Times New Roman", Georgia, Serif;
	border-bottom: solid 1px #e9e9e9;
	border-right: 1px solid transparent;
	position: relative;
	padding: 10px;
	box-sizing: border-box;
	height: auto
}

.profileImage {
	float: left;
	padding-right: 10px;
	padding-top: 3px;
}

.profileInfo {
	height: auto;
	overflow: hidden;
}

h3 {
	margin: 0px;
}

p {
	margin: 0px;
	margin-bottom: 10px;
}

p.profileSkill {
	font-style: italic
}

h3.profileName {
	color: #07c
}
.actionTab{
	padding-left:600px;padding-bottom:10px;
}
.askQuestionAction{
	float:left;padding-right:10px;
}
.availabilityAction{
	float:left;padding-right:10px;
}
.error {
	color: #ff0000;
	background-color: #ffEEEE;
}
</style>
</head>
<body onload='document.f.j_username.focus();'>
	<!-- Header -->	
	<div class="header">
		<b style="color: Orange">CloudKode</b>
	</div>
	
	
	<!-- Container -->
	<div class="container">
	
	<h3>Mentor Application Step 2</h3>

	<form:form name='f' action="/KodeWebWar-1.0-SNAPSHOT/kode/mentor/application/confirm/${requestScope.appId}" commandName="profile" method='POST'>

		<table>
			<tr>
				<td>Step 2</td>
			</tr>
			<tr>
				<td><b>Professional Info</b></td>
			</tr>
			<tr>
				<td>Company:</td>
				<td><form:input path="company" /></td>
				<td><form:errors path="company" cssClass="error" /></td>				
			</tr>
			<tr>
				<td>designation:</td>
				<td><form:input path="designation" /></td>
				<td><form:errors path="designation" cssClass="error" /></td>				
			</tr>
			<tr>
				<td>Describe about your journey and any major highlights of your career:<br>(This would be visible on your profile) *</td>
				<td><form:input path="about" /></td>
				<td><form:errors path="about" cssClass="error" /></td>
			</tr>
			<br>
			<tr>
				<td><b>Personal Info</b></td>
			</tr>
			<tr>
				<td>Availability (Hours per week) : *</td>
				<td><form:input path="availability" /></td>
				<td><form:errors path="availability" cssClass="error" /></td>
			</tr>
			<tr>
				<td>timezone: (AutoDetected) *</td>
				<td><select id="appointment_time_zone" name="timezone">
						<option value="America/Dawson_Creek	">(GMT-07:00) America/Dawson_Creek</option>
						<option value="America/Denver">(GMT-07:00) America/Denver</option>
						<option value="Asia/Kolkata">(GMT+05:30) Asia/Kolkata</option>
						<option value="Europe/Dublin">(GMT+00:00) Europe/Dublin</option>
						<option value="Africa/Algiers">(GMT+01:00) Africa/Algiers</option>
						<option value="Africa/Gaborone">(GMT+02:00) Africa/Gaborone</option></select>
				</td>
			</tr>
			<tr>
				<td>Rate (Charge per 15 mins) : * ,Use 0 for free</td>
				<td><form:input path="rate" /></td>
				<td><form:errors path="rate" cssClass="error" /></td>
			</tr>
			<tr>
				<td><b>Social Profile</b></td>
			</tr>
			<tr>
				<td>website:</td>
				<td><form:input path="website" /></td>
			</tr>
			<tr>
				<td>twitter:</td>
				<td><form:input path="twitter" /></td>
			</tr>
			<tr>
				<td>github:</td>
				<td><form:input path="github" /></td>
			</tr>
			<tr>
				<td>stackoverflow:</td>
				<td><form:input path="stackoverflow" /></td>
			</tr>
			<tr>
				<td>linkedin:</td>
				<td><form:input path="linkedin" /></td>
			</tr>
			<tr>
				<td>gravatar:</td>
				<td><form:input path="gravatar" /></td>
			</tr>
			<tr>
				<td><b>Skill Breakup</b></td>
			</tr>
			<c:forEach var="skill" items="${profile.skills}" varStatus="skillCounter">
				<tr><td><b>${skill.skillname}</b></td></tr>
				<form:hidden path="skills[${skillCounter.index}].skillname"  value="${skill.skillname}"/>
				<form:hidden path="skills[${skillCounter.index}].skid"  value="${skill.skid}"/>
				<tr></tr>
				<tr>
					<td>About your experience in this skill:</td>
					<td colspan='2'><form:input path="skills[${skillCounter.index}].skillexpertise" /></td>
					<td><form:errors path="skills[${skillCounter.index}].skillexpertise" cssClass="error" /></td>
				</tr>
				<tr>
					<td>Number of years that you worked:</td>
					<td colspan='2'><form:input path="skills[${skillCounter.index}].experience" /></td>
					<td><form:errors path="skills[${skillCounter.index}].experience" cssClass="error" /></td>
				</tr>
				<tr>
					<td>how would you rate yourself: 1-5</td>
					<td colspan='2'><form:input path="skills[${skillCounter.index}].rating" /></td>
					<td><form:errors path="skills[${skillCounter.index}].rating" cssClass="error" /></td>
				</tr>
			</c:forEach>
			<tr>
				<td colspan='2'><input name="submit" type="submit"
					value="submit" /></td>
			</tr>	
		</table>

	</form:form>
	</div>
</body>
</html>