<%@ page errorPage="errorpage.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
<title>Mentor Application</title>
<style>
.global {
	text-align: right;
	color: orange;
	padding: 3px;
}

.header {
	width: 1200px;
	height: 52px;
	margin: 0 auto;
}

.container {
	width: 975px;
	background: #fff;
	display: block;
	margin-left: 188px;
	color: grey;
}

.profileRow {
	font-family: "Times New Roman", Georgia, Serif;
	border-bottom: solid 1px #e9e9e9;
	border-right: 1px solid transparent;
	position: relative;
	padding: 10px;
	box-sizing: border-box;
	height: auto
}

.profileImage {
	float: left;
	padding-right: 10px;
	padding-top: 3px;
}

.profileInfo {
	height: auto;
	overflow: hidden;
}

h3 {
	margin: 0px;
}

p {
	margin: 0px;
	margin-bottom: 10px;
}

p.profileSkill {
	font-style: italic
}

h3.profileName {
	color: #07c
}
.actionTab{
	padding-left:600px;padding-bottom:10px;
}
.askQuestionAction{
	float:left;padding-right:10px;
}
.availabilityAction{
	float:left;padding-right:10px;
}
.error {
	color: #ff0000;
	background-color: #ffEEEE;
}
</style>
</head>
<body>
	<!-- Header -->	
	<div class="header">
		<b style="color: Orange">CloudKode</b>
	</div>
	
	
	<!-- Container -->
	<div class="container">
	
	<h3>Mentor Application Step 1</h3>

	<form:form name='f' action="/KodeWebWar-1.0-SNAPSHOT/kode/mentor/application/create" commandName="application" method='POST'>

		<table>
			<tr>
				<td>Step 1</td>
			</tr>
			<tr>
				<td>email:</td>
				<td><form:input path="email" /></td>
				<c:if test="${not empty requestScope.emailAlreadyRegisteredError}">
					<td>${requestScope.emailAlreadyRegisteredError}</td>
				</c:if>
				<td><form:errors path="email" cssClass="error" /></td>
			</tr>
			
			<tr>
				<td>First Name:</td>
				<td><form:input path="firstname" /></td>
				<td><form:errors path="firstname" cssClass="error" /></td>
			</tr>
			
			<tr>
				<td>Last Name:</td>
				<td><form:input path="lastname" /></td>
				<td><form:errors path="lastname" cssClass="error" /></td>
			</tr>

			<tr>
				<c:forEach var="skill" items="${requestScope.skills}">
					<td>${skill.value}:</td>
					<td><form:checkbox path="skills" value='${skill.key}' />
					</td>
				</c:forEach>
				<td><form:errors path="skills" cssClass="error" /></td>				
				
			</tr>
			<tr>
				<td>headline:</td>
				<td><form:input path="headline" /></td>
				<td><form:errors path="headline" cssClass="error" /></td>				
			</tr>
			<tr>
				<td colspan='2'><input name="submit" type="submit"
					value="submit" /></td>
			</tr>
		</table>

	</form:form>
	</div>
</body>
</html>