<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<html>
<head>
<title>CloudKode</title>
<link rel="stylesheet" href="/css/kode.css" />
</head>
<body>
	<sec:authorize access="isAuthenticated()">
		<div class="global">
			<p>${username}</p>
		</div>
	</sec:authorize>
	<sec:authorize access="isAnonymous()">
		<div class="global">
			<a href="/KodeWebWar-1.0-SNAPSHOT/kode/login" style="color: orange;">
				Signin</a> <a href="/KodeWebWar-1.0-SNAPSHOT/kode/reg"
				style="color: orange;"> Register</a>
		</div>
	</sec:authorize>
	<div class="header">
		<b style="color: Orange">CloudKode</b>
	</div>
	<div class="containerWide">
		<div class="profile">
			<div class="profileLeftArea">
				<div style='padding-top: 20px; padding-bottom: 63px'>
					<p>${profile.firstname}</p>
					<img src="${profile.gravatar}" height="60px" />
				</div>
				<div style="">
					<div class="action">
						<a href="/kode/sessions/users" id="messages" class="actionRef"
							onclick="return false;">Message me</a>
					</div>
					<div class="action">
						<a href="/kode/question" id="postquestion" class="actionRef">Schedule
							a Session</a>
					</div>
				</div>
			</div>
			<div class="profileRightArea">
				<h2>About Me :</h2>
				<p>${profile.about}</p>
				<p class="skillSection">Skills</p>
				<c:forEach var="skill" items="${profile.skills}">
					<div class="skillRow">
						<div class="skillName">${skill.skillname}</div>
						<div class="skillInfo">
							<p>${skill.skillexpertise}</p>
							<p class="skillExp">${skill.experience}years</p>
						</div>
					</div>
				</c:forEach>
			</div>
		</div>
	</div>
</body>
</html>