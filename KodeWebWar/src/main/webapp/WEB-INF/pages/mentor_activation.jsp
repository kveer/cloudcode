<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>Login Page</title>
<style>
.global {
	text-align: right;
	color: orange;
	padding: 3px;
}

.header {
	width: 1200px;
	height: 52px;
	margin: 0 auto;
}

.container {
	width: 975px;
	background: #fff;
	display: block;
	margin-left: 188px;
	color: grey;
}

.profileRow {
	font-family: "Times New Roman", Georgia, Serif;
	border-bottom: solid 1px #e9e9e9;
	border-right: 1px solid transparent;
	position: relative;
	padding: 10px;
	box-sizing: border-box;
	height: auto
}

.profileImage {
	float: left;
	padding-right: 10px;
	padding-top: 3px;
}

.profileInfo {
	height: auto;
	overflow: hidden;
}

h3 {
	margin: 0px;
}

p {
	margin: 0px;
	margin-bottom: 10px;
}

p.profileSkill {
	font-style: italic
}

h3.profileName {
	color: #07c
}
.actionTab{
	padding-left:600px;padding-bottom:10px;
}
.askQuestionAction{
	float:left;padding-right:10px;
}
.availabilityAction{
	float:left;padding-right:10px;
}
.errorblock {
	color: #ff0000;
	background-color: #ffEEEE;
	border: 3px solid #ff0000;
	padding: 8px;
	margin: 16px;
}
.error {
	color: #ff0000;
	background-color: #ffEEEE;
}
</style>
</head>
<body>
 <!-- Header -->	
	<div class="header">
		<b style="color: Orange">CloudKode</b>
	</div>
	
	
	<!-- Container -->
	<div class="container">
	<form name='f' action="/kode/${requestScope.path}" method='POST'>
 
		<table>
			<tr>
				<td>Please choose a Password:</td>
				<td><input type='password' name='password' />
				</td>
			</tr>
			<tr>
				<td colspan='2'><input name="submit" type="submit"
					value="submit" />
				</td>
			</tr>
		</table>
 
	</form>
	</div>
</body>
</html>