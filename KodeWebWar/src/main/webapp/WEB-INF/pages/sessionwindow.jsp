<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<html>
<head>
<title>CloudKode</title>
<script type="text/javascript"
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
<script
	src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/jquery-ui.min.js"
	type="text/javascript"></script>
<link rel="stylesheet" type="text/css"
	href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/themes/start/jquery-ui.css" />
	 <script src='https://cdn.firebase.com/js/client/1.0.11/firebase.js'></script>
	   <script src="http://localhost:8080/KodeWebWar-1.0-SNAPSHOT/js/codemirror.js"></script>
  <script src="http://localhost:8080/KodeWebWar-1.0-SNAPSHOT/js/javascript.js"></script>
  <link rel="stylesheet" href="http://localhost:8080/KodeWebWar-1.0-SNAPSHOT/css/codemirror.css" />
	<script src="http://localhost:8080/KodeWebWar-1.0-SNAPSHOT/js/firepad.js"></script>
  	<link rel="stylesheet" href="http://localhost:8080/KodeWebWar-1.0-SNAPSHOT/css/firepad.css" />
	 
<style>
.global {
	text-align: right;
	color: orange;
	padding: 10px;
}

.header {
	width: 975px;
	height: 67px;
	margin: 0 auto;
}

.container {
	width: 975px;
	background: #fff;
	display: block;
	margin-left: 188px;
	color: grey;
}

.profile {
	font-family: "Times New Roman", Georgia, Serif;
	border-bottom: solid 1px #e9e9e9;
	border-right: 1px solid transparent;
	position: relative;
	padding: 10px;
	box-sizing: border-box;
	height: auto
}

.profileImage {
	float: left;
	padding-right: 10px;
	padding-top: 3px;
}

.profileInfo {
	height: auto;
	overflow: hidden;
}

h3 {
	margin: 0px;
}

p {
	margin: 0px;
	margin-bottom: 10px;
}

p.profileSkill {
	font-style: italic
}

h3.profileName {
	color: #07c
}

.questionRow {
	padding: 18px;
	float: left;
}

.questionField {
	float: left;
	padding-right: 19px;
}

.questionTitle {
	float: left;
	padding-right: 7px;
	width: 370px;
}
.actionTab {
	padding-left: 600px;
	padding-bottom: 10px;
}
.action {
	float: left;
	padding-right: 10px;
}

.mySessionsAction {
	float: left;
	padding-right: 10px;
}

.viewQuestionsAction {
	float: left;
	padding-right: 10px;
}
.sessionContainer {
	background: #fff;
	display: block;
	margin-left: 20px;
	color: grey;
}
.leftBlock {
	height:500px;width:800px;float:left;
}
.rightBlock {
	float:left;padding-left:60px;
}
.camArea{
	height:300px;
}
.firepad {
	height:500px;width:800px;float:left;
      border-color: black;border-style:solid;
        border-width:5px;
}
</style>
</head>
<body>

	<div class="global">
			<p>${username}</p>
	</div>
	
	<div class="header">
		<b style="color: Orange">CloudKode</b>
	</div>


	<div class="sessionContainer">
		
		<div id='firepad-container'>
          	<!-- <textarea name="area" style="width: 800px; height: 500px;">Please Type here</textarea> -->
  		</div>
  		<div class="rightBlock">
          	<div class="camArea">
            </div>
			<div id="chatWidget">
	        	<div style="height:130px;border-style: solid;" id="messagesDiv"></div>
				<input type="hidden" name="name" id="name" value="${username}">
				<input type="hidden" name="sid" id="sid" value="${sid}">
          		<b>Name : ${username}</b>
	        	<input type="text" id="messageInput" placeholder="Message">
    	 	</div>
  		</div>

	</div>
<!-- Container End -->
	<script type="text/javascript">
	$(function() {
        var furl = 'https://glaring-fire-8489.firebaseio.com';
		alert('Initialize Chat Widget');
        var sid = $('#sid').val();
        var chatUrl= furl+"/sessions/"+'/chats/'+sid;
        console.log(chatUrl);
        var chatRef = new Firebase(chatUrl);
		$('#messageInput').keypress(function (e) {
            if (e.keyCode == 13) {
              var name = $('#name').val();
              var text = $('#messageInput').val();
              chatRef.push({name: name, text: text});
              $('#messageInput').val('');
            }
        });
		chatRef.on('child_added', function(snapshot) {
            var message = snapshot.val();
            displayChatMessage(message.name, message.text);
        });
		initializeFirepad();
	});	
	function initializeFirepad(){
	//// Initialize Firebase.
	 	var furl = 'https://glaring-fire-8489.firebaseio.com';
		alert('Initialize Firepad Widget');
        var sid = $('#sid').val();
        var fpUrl= furl+"/sessions/"+'/editor/'+sid;
        console.log(fpUrl);
        var firepadRef = new Firebase(fpUrl);
	    //// Create CodeMirror (with line numbers and the JavaScript mode).
	    var codeMirror = CodeMirror(document.getElementById('firepad-container'), {
	      lineNumbers: true,
	      mode: 'javascript'
	    });

	    //// Create Firepad.
	    var firepad = Firepad.fromCodeMirror(firepadRef, codeMirror);

	    //// Initialize contents.
	    firepad.on('ready', function() {
	      if (firepad.isHistoryEmpty()) {
	        firepad.setText('// JavaScript Editing with Firepad!\nfunction go() {\n  var message = "Hello, world.";\n  console.log(message);\n}');
	      }
	    });
	}
	function displayChatMessage(name, text) {
        $('<div/>').text(text).prepend($('<em/>').text(name+': ')).appendTo($('#messagesDiv'));
        $('#messagesDiv')[0].scrollTop = $('#messagesDiv')[0].scrollHeight;
    }
	</script>
</body>
</html>