<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<html>
<head>
<title>CloudKode</title>
<script type="text/javascript"
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
<script
	src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/jquery-ui.min.js"
	type="text/javascript"></script>
<link rel="stylesheet" type="text/css"
	href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/themes/start/jquery-ui.css" />
<style>
.global {
	text-align: right;
	color: orange;
	padding: 10px;
}

.header {
	width: 975px;
	height: 67px;
	margin: 0 auto;
}

.container {
	width: 975px;
	background: #fff;
	display: block;
	margin-left: 188px;
	color: grey;
}

.profile {
	font-family: "Times New Roman", Georgia, Serif;
	border-bottom: solid 1px #e9e9e9;
	border-right: 1px solid transparent;
	position: relative;
	padding: 10px;
	box-sizing: border-box;
	height: auto
}

.profileImage {
	float: left;
	padding-right: 10px;
	padding-top: 3px;
}

.profileInfo {
	height: auto;
	overflow: hidden;
}

h3 {
	margin: 0px;
}

p {
	margin: 0px;
	margin-bottom: 10px;
}

p.profileSkill {
	font-style: italic
}

h3.profileName {
	color: #07c
}

.questionRow {
	padding: 18px;
	float: left;
}

.questionField {
	float: left;
	padding-right: 19px;
}

.questionTitle {
	float: left;
	padding-right: 7px;
	width: 370px;
}
.actionTab {
	padding-left: 600px;
	padding-bottom: 10px;
}
.action {
	float: left;
	padding-right: 10px;
}

.mySessionsAction {
	float: left;
	padding-right: 10px;
}

.viewQuestionsAction {
	float: left;
	padding-right: 10px;
}
</style>
<script type="text/javascript">
	$(function() {
		$('#questionpoolbytag')
				.attr('onclick', 'return false;')
				.click(
						function(e) {
							e.preventDefault();
							var tag = $(this).attr('data-tag');
							alert('tag -- >' + tag);
							$
									.ajax({
										type : "GET",
										url : 'http://localhost:8080/KodeWebWar-1.0-SNAPSHOT/kode/questions/tags/'
												+ tag,
										headers : {
											"Accept" : "application/json",
											"Content-Type" : "application/json"
										},
										error : error,
										complete : showQuestions
									});
						});
		$("#questions")
				.on("change",
					"#action",
					function() {
						alert('hello');
						var qid = $(this).attr('data-qid');
						var flag;
						if (!$(this).is(':checked')) {
							flag = 'cancel';
							alert('unchecked' + flag);
						} else {
							flag = 'confirm';
							alert('checked' + flag);
						}
						$.ajax({
								type : "POST",
								url : 'http://localhost:8080/KodeWebWar-1.0-SNAPSHOT/kode/sessions/'
										+ qid + '/action/' + flag,
								headers : {
									"Accept" : "application/json",
									"Content-Type" : "application/json"
								},
								error : error,
								complete : showQuestions
						});
				});
		
		$("#sessionsaction").attr('onclick', 'return false;').click(
				function(e) {
					e.preventDefault();					
					$.ajax({
						type : "GET",
						url : 'http://localhost:8080/KodeWebWar-1.0-SNAPSHOT/kode/sessions/users',
						headers : {
							"Accept" : "application/json",
							"Content-Type" : "application/json"
						},
						error : error,
						complete : showSessions
					});
				}
				);
		$("#questionpoolaction").attr('onclick', 'return false;').click(
				function(e) {
					e.preventDefault();
					$.ajax({
						type : "GET",
						url : 'http://localhost:8080/KodeWebWar-1.0-SNAPSHOT/kode/questions',
						headers : {
							"Accept" : "application/json",
							"Content-Type" : "application/json"
						},
						error : error,
						complete : showQuestions
					});
				}
				);
		
		$("#sessions")
		.on("click",
			"#opensession",
			function(e) {
				alert('binding click event for opensession');
				e.preventDefault();
				var sid = $(this).attr('data-sid');
			    window.open("http://localhost:8080/KodeWebWar-1.0-SNAPSHOT/kode/opensession/"+sid, "popupWindow");
			});
		
		$.ajax({
			type : "GET",
			url : 'http://localhost:8080/KodeWebWar-1.0-SNAPSHOT/kode/questions',
			headers : {
				"Accept" : "application/json",
				"Content-Type" : "application/json"
			},
			error : error,
			complete : showQuestions
		});
		

	});
	function error() {
		alert('error');
	}
	function qConfirmSuccess() {
		alert('success');
	}

	function showQuestions(result) {
		//alert(JSON.stringify(result.responseText));
		var resp = eval('(' + result.responseText + ')');
		//alert(resp.length);
		$('#questionpool').css("display","block");
		$('#sessionpool').css("display","none");
		$('#questions').empty();
		$
				.each(
						resp,
						function(idx, obj) {
							//alert(obj.title);
							var question = '<div id="questionRow" class="questionRow"><div id="questionField" class="questionField"><b style="color: #07c">User</b> :'
									+ obj.uid
									+ '</div>'
									+ '<div id="questionTitle" class="questionTitle"><b style="color: #07c">Title</b> :'
									+ obj.title
									+ '</div>'
									+ '<div id="questionField" class="questionField"><b style="color: #07c">Date</b> :'
									+ obj.date
									+ '</div>'
									+ '<div id="questionField" class="questionField"><b style="color: #07c">Tags</b> :'
									+ obj.tags
									+ '</div><div id="questionField" class="questionField"><b style="color: #07c">Schedule a Live Session</b> <input type="checkbox" value="confirm" data-qid="'+obj.qid+'" id="action" name="action"/> </div></div>';
							$(question).appendTo('#questions');
						});
	}
	function showSessions(result) {
		alert(JSON.stringify(result.responseText));
		var resp = eval('(' + result.responseText + ')');
		//alert(resp.length);
		$('#questionpool').css("display","none");
		$('#sessionpool').css("display","block");
		$('#sessions').empty();
		$
				.each(
						resp,
						function(idx, session) {
							//alert(obj.title);
							var question = '<div class="questionRow"><div class="questionField"><b style="color: #07c">User</b> :'
									+ session.mentoruid
									+ '</div>'
									+ '<div class="questionTitle"><b style="color: #07c">Title</b> :'
									+ session.question.title
									+ '</div>'
									+ '<div class="questionField"><b style="color: #07c">Date</b> :'
									+ session.sessiontime
									+ '</div>'
									+ '<div class="questionField"><b style="color: #07c">Tags</b> :'
									+ session.question.tags
									+ '</div><div class="questionField"><b style="color: #07c">Status :</b> '+session.status+'</div>'+
									'<div class="questionField"><a href="/KodeWebWar-1.0-SNAPSHOT/kode/questions" id="opensession" data-sid="'+session.sid+'" target="_blank" style="font-size: 16px; font-weight: bold; color: orange; padding-left: 20px;">Open Session</a></div></div>';
							$(question).appendTo('#sessions');
						});
	}
</script>
</head>
<body>

	<sec:authorize access="isAuthenticated()">
		<div class="global">
			<p>${username}</p>
		</div>
	</sec:authorize>
	<sec:authorize access="isAnonymous()">
		<div class="global">
			<a href="/KodeWebWar-1.0-SNAPSHOT/kode/login" style="color: orange;">
				Signin</a> <a href="/KodeWebWar-1.0-SNAPSHOT/kode/reg"
				style="color: orange;"> Register</a>
		</div>
	</sec:authorize>


	<div class="header">
		<b style="color: Orange">CloudKode</b>
	</div>


	<div class="container">
		
		<div class='actionTab'>
			<sec:authorize access="isAuthenticated()">
				<div class='action'>
					<a href="/KodeWebWar-1.0-SNAPSHOT/kode/sessions/users" id="sessionsaction" 
					style="font-size: 16px; font-weight: bold; color: orange; padding-left: 20px;">Sessions</a>
				</div>
				<div class='action'>
					<a href="/KodeWebWar-1.0-SNAPSHOT/kode/questions" id="questionpoolaction" 
					style="font-size: 16px; font-weight: bold; color: orange; padding-left: 20px;">Questions in Pool</a>
				</div>
			</sec:authorize>
		</div>

		<div id='questionpool'>

			<div style='padding: 15px;'>
				<b>Filter by Tag</b> <a
					href="/KodeWebWar-1.0-SNAPSHOT/kode/questions/tags/java" data-tag='java' id='questionpoolbytag'
					style="font-size: 16px; font-weight: bold; color: #07c; padding-left: 20px;">java</a>
			</div>
			<p>Questions</p>
			<div id='questions'>				
			</div>

		</div><!-- Question Pool End -->
		
		
		<div id='sessionpool' style='display:none'>

			<div style='padding: 15px;'>
				<b>Filter by Status</b> <a
					href="/KodeWebWar-1.0-SNAPSHOT/kode/sessions/status/" data-tag='confirm'
					style="font-size: 16px; font-weight: bold; color: #07c; padding-left: 20px;">open</a>
			</div>
			<p>Sessions</p>
			<div id='sessions'>				
			</div>

		</div><!-- Question Pool End -->
		
		
	</div><!-- Container End -->
</body>
</html>