<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<html>
<head>
<title>Ask a Question</title>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script type="text/javascript"
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
<script
	src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/jquery-ui.min.js"
	type="text/javascript"></script>
<link rel="stylesheet" type="text/css"
	href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/themes/start/jquery-ui.css" />

<link rel="stylesheet" type="text/css"
	href="http://xoxco.com/projects/code/tagsinput/jquery.tagsinput.css" />
<script type="text/javascript"
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
<script type="text/javascript"
	src="http://xoxco.com/projects/code/tagsinput/jquery.tagsinput.js"></script>

<link rel="stylesheet" href="/css/kode.css" />
<script type="text/javascript">
	$(function() {
		$('#questionpoolbytag').attr('onclick', 'return false;').click(
				function(e) {
					e.preventDefault();
					var tag = $(this).attr('data-tag');
					$.ajax({
						type : "GET",
						url : '/kode/questions/tags/' + tag,
						headers : {
							"Accept" : "application/json",
							"Content-Type" : "application/json"
						},
						error : error,
						complete : showQuestions
					});
				});
		$("#questions").on("change", "#action", function() {
			var qid = $(this).attr('data-qid');
			var flag;
			if (!$(this).is(':checked')) {
				flag = 'cancel';
				alert('unchecked' + flag);
			} else {
				flag = 'confirm';
				alert('checked' + flag);
			}
			$.ajax({
				type : "POST",
				url : '/kode/sessions/' + qid + '/action/' + flag,
				headers : {
					"Accept" : "application/json",
					"Content-Type" : "application/json"
				},
				error : error,
				complete : showQuestions
			});
		});

		$("#sessionsaction").attr('onclick', 'return false;').click(
				function(e) {
					e.preventDefault();
					$.ajax({
						type : "GET",
						url : '/kode/sessions/user',
						headers : {
							"Accept" : "application/json",
							"Content-Type" : "application/json"
						},
						error : error,
						complete : showSessions
					});
					defaultNavActions();
					highlightNavAction("#sessionsaction");
				});
		$("#questionpoolaction").attr('onclick', 'return false;').click(
				function(e) {
					e.preventDefault();
					$.ajax({
						type : "GET",
						url : '/kode/questions',
						headers : {
							"Accept" : "application/json",
							"Content-Type" : "application/json"
						},
						error : error,
						complete : showQuestions
					});
					defaultNavActions();
					highlightNavAction("#questionpoolaction");
				});

		$("#sessions").on("click", "#opensession", function(e) {
			e.preventDefault();
			var sid = $(this).attr('data-sid');
			window.open("/kode/opensession/" + sid, "popupWindow");
		});

		$.ajax({
			type : "GET",
			url : '/kode/questions',
			headers : {
				"Accept" : "application/json",
				"Content-Type" : "application/json"
			},
			error : error,
			complete : showQuestions
		});

		$("#datepicker").datepicker();

		$('#tags').tagsInput({
			width : 'auto'
		});

	});
	function error() {
		alert('error');
	}
	function qConfirmSuccess() {
		alert('success');
	}
	function highlightNavAction(handle) {
		$(handle).css({
			color : '#545452',
			"font-weight" : 'normal'
		});
		$(handle + "div").addClass("actionHightlight");
	}
	function defaultNavActions() {
		$(".action").removeClass("actionHightlight");
		$(".actionRef").css({
			color : "orange",
			"font-weight" : 'bold'
		});
	}

	function showQuestions(result) {
		//alert(JSON.stringify(result.responseText));
		var resp = eval('(' + result.responseText + ')');
		//alert(resp.length);
		$('#questionpool').css("display", "block");
		$('#sessionpool').css("display", "none");
		$('#questions').empty();
		$
				.each(
						resp,
						function(idx, obj) {
							//alert(obj.title);
							var question = '<div id="questionRow" class="questionRow"><div id="questionTitle" class="questionTitle"><b style="color: #07c">Title</b> :'
									+ obj.title
									+ '</div>'
									+ '<div id="questionField" class="questionField"><b style="color: #07c">Date</b> :'
									+ obj.date
									+ '</div>'
									+ '<div id="questionField" class="questionField"><b style="color: #07c">Tags</b> :'
									+ obj.tags
									+ '</div><div id="questionField" class="questionField"><b style="color: #07c">Confirm for a Session</b> <input type="checkbox" value="confirm" data-qid="'+obj.qid+'" id="action" name="action"/> </div></div>';
							$(question).appendTo('#questions');
						});
	}
	function showSessions(result) {
		//alert(result);
		if (result.getResponseHeader('REQUIRES_AUTH') === '1') {
			window.location.href = '/kode/login';
		}
		//alert(JSON.stringify(result.responseText));
		var resp = eval('(' + result.responseText + ')');
		//alert(resp.length);
		$('#questionpool').css("display", "none");
		$('#sessionpool').css("display", "block");
		$('#sessions').empty();
		$
				.each(
						resp,
						function(idx, session) {
							//alert(obj.title);
							var question = '<div class="questionRow"><div class="questionField"><b style="color: #07c">User</b> :'
									+ session.mentoruid
									+ '</div>'
									+ '<div class="questionTitle"><b style="color: #07c">Title</b> :'
									+ session.question.title
									+ '</div>'
									+ '<div class="questionField"><b style="color: #07c">Date</b> :'
									+ session.sessiontime
									+ '</div>'
									+ '<div class="questionField"><b style="color: #07c">Tags</b> :'
									+ session.question.tags
									+ '</div><div class="questionField"><b style="color: #07c">Status :</b> '
									+ session.status
									+ '</div>'
									+ '<div class="questionField"><a href="/kode/questions" id="opensession" data-sid="'+session.sid+'" target="_blank" style="font-size: 16px; font-weight: bold; color: orange; padding-left: 20px;">Open Session</a></div></div>';
							$(question).appendTo('#sessions');
						});
	}
</script>
</head>
<body class="body">

	<sec:authorize access="isAuthenticated()">
		<div class="global">
			<div class="cont">
				<ul class="nav">
					<li class="userprofile"><a href="/kode/profile">${username}</a></li>
					<li class="signout"><a href="/kode/logout">sign out</a></li>
				</ul>
			</div>
		</div>
	</sec:authorize>
	<sec:authorize access="isAnonymous()">
		<div class="global">
			<div class="cont">
				<ul class="nav">
					<li class="signin"><a href="/kode/login">sign in</a></li>
					<li class="signout"><a href="/kode/reg">register</a></li>
				</ul>
			</div>
		</div>
	</sec:authorize>


	<div class="header">
		<div class="cont">
			<div class="brand">
				<a href="/kode/home" class="logo">CloudKode</a>
			</div>
			<ul class="nav options">
				<li><a class="" href="/">ask question</a></li>
				<li><a class="" href="/">mentors</a></li>
			</ul>
		</div>
	</div>

	<div class="cont">
		<div class="content" style='width: 810px; padding: 20px'>
			<div class="question-area" style='margin: 20px 0 0 32px;'>
				<form:form name='f' action="${action}" method='POST'
					commandName="question">
					<p>
						<label for="title">Title:</label>
						<form:input path="title" />
						<span class="hint"><form:errors path="title"
								cssClass="error" /></span>
					</p>
					<p>
						<label for="kindofhelp">Kind of Help (Optional):</label>
						<form:input path="kindofhelp" />
						<span class="hint"><form:errors path="kindofhelp"
								cssClass="error" /></span>
					</p>
					<p>
						<label for="timezone">TimeZone:</label> <select
							id="appointment_time_zone" name="timezone">
							<option value="America/Dawson_Creek	">(GMT-07:00)
								America/Dawson_Creek</option>
							<option value="America/Denver">(GMT-07:00)
								America/Denver</option>
							<option value="Asia/Kolkata">(GMT+05:30) Asia/Kolkata</option>
							<option value="Europe/Dublin">(GMT+00:00) Europe/Dublin</option>
							<option value="Africa/Algiers">(GMT+01:00)
								Africa/Algiers</option>
							<option value="Africa/Gaborone">(GMT+02:00)
								Africa/Gaborone</option>
						</select> <span class="hint"><form:errors path="kindofhelp"
								cssClass="error" /></span>
					</p>
					<p>
						<label for="date">Date:</label>
						<form:input id="datepicker" path="date" />
						<span class="hint"><form:errors path="date"
								cssClass="error" /></span>
					</p>
					<p>
						<label for="time">Time:</label> <select name='time'
							style='width: 120px;'>
							<option value='00:00'>12:00 AM</option>
							<option value='00:30'>12:30 AM</option>
							<option value='01:00'>1:00 AM</option>
							<option value='01:30'>1:30 AM</option>
							<option value='02:00'>2:00 AM</option>
							<option value='02:30'>2:30 AM</option>
							<option value='03:00'>3:00 AM</option>
							<option value='03:30'>3:30 AM</option>
							<option value='04:00'>4:00 AM</option>
							<option value='04:30'>4:30 AM</option>
							<option value='05:00'>5:00 AM</option>
							<option value='05:30'>5:30 AM</option>
							<option value='06:00'>6:00 AM</option>
							<option value='06:30'>6:30 AM</option>
							<option value='07:00'>7:00 AM</option>
							<option value='07:30'>7:30 AM</option>
							<option value='08:00'>8:00 AM</option>
							<option value='08:30'>8:30 AM</option>
							<option value='09:00'>9:00 AM</option>
							<option value='09:30'>9:30 AM</option>
							<option value='10:00'>10:00 AM</option>
							<option value='10:30'>10:30 AM</option>
							<option value='11:00'>11:00 AM</option>
							<option value='11:30'>11:30 AM</option>
							<option value='12:00'>12:00 PM</option>
							<option value='12:30'>12:30 PM</option>
							<option value='13:00'>1:00 PM</option>
							<option value='13:30'>1:30 PM</option>
							<option value='14:00'>2:00 PM</option>
							<option value='14:30'>2:30 PM</option>
							<option value='15:00'>3:00 PM</option>
							<option value='15:30'>3:30 PM</option>
							<option value='16:00'>4:00 PM</option>
							<option value='16:30'>4:30 PM</option>
							<option value='17:00'>5:00 PM</option>
							<option value='17:30'>5:30 PM</option>
							<option value='18:00'>6:00 PM</option>
							<option value='18:30'>6:30 PM</option>
							<option value='19:00'>7:00 PM</option>
							<option value='19:30'>7:30 PM</option>
							<option value='20:00'>8:00 PM</option>
							<option value='20:30'>8:30 PM</option>
							<option value='21:00'>9:00 PM</option>
							<option value='21:30'>9:30 PM</option>
							<option value='22:00'>10:00 PM</option>
							<option value='22:30'>10:30 PM</option>
							<option value='23:00'>11:00 PM</option>
							<option value='23:30'>11:30 PM</option>
						</select>
					</p>
					<p>
						<label for="date">tags:</label>
						<form:input path="tags" id="tags" class="tags" value="" />
						<span class="hint"><form:errors path="tags"
								cssClass="error" /></span>
					</p>
					<p>
						<input type="submit" value="submit" />
					</p>
				</form:form>
			</div>
		</div>
	</div>


	<%-- <div class="containerWide">

		<div class="leftHandle">

			<div class="action">
				<a href="/kode/question" id="postquestion" class="actionRef">Post
					a Question</a>
			</div>
			<div class="action">
				<a href="/kode/question" id="postquestion" class="actionRef">Questions</a>
			</div>
			<c:if test="${mentor}">
				<div class="action actionHightlight" id="questionpoolactiondiv">
					<a href="/kode/questions" id="questionpoolaction"
						class="actionRef actionRefClicked " onclick="return false;">Questions
						in Pool</a>
				</div>
			</c:if>
			<div class="action" id="sessionsactiondiv">
				<a href="/kode/sessions/user" id="sessionsaction" class="actionRef"
					onclick="return false;">Sessions</a>
			</div>
		</div>

		<div id='questionpool'>

			<div style='padding: 15px;'>
				<b>Filter by Tag</b> <a href="/kode/questions/tags/java"
					data-tag='java' id='questionpoolbytag'
					style="font-size: 16px; font-weight: bold; color: #07c; padding-left: 20px;">java</a>
			</div>
			<p>Questions</p>
			<div id='questions'></div>

		</div>
		<!-- Question Pool End -->
		<div id='sessionpool' style='display: none; float: left; width: 900;'>

			<div style='padding: 15px;'>
				<b>Filter by Status</b> <a href="/kode/sessions/status/"
					data-tag='confirm'
					style="font-size: 16px; font-weight: bold; color: #07c; padding-left: 20px;">open</a>
			</div>
			<p>Sessions</p>
			<div id='sessions'></div>

		</div>
		<!-- Question Pool End -->


	</div> --%>
	<!-- Container End -->
	<footer>
		<div class="cont">

			<div class="footer-menu">
				<a href="/about">about</a> <a href="/faq">faq</a> <a href="/support">support</a>
				<a href="http://blog.careers.stackoverflow.com">blog</a> <a
					href="http://stackoverflow.com/legal">legal</a> <a
					href="http://stackexchange.com/legal/privacy-policy">privacy
					policy</a> <a
					href="http://engine.adzerk.net/r?e=eyJhdiI6NDE0LCJhdCI6MjAsImNtIjo5NTQsImNoIjoxMTc4LCJjciI6MzM5MSwiZG0iOjQsImZjIjozODA5LCJmbCI6Mjc1MSwibnciOjIyLCJydiI6MCwicHIiOjExNSwic3QiOjAsInVyIjoiaHR0cDovL3N0YWNrb3ZlcmZsb3cuY29tL2Fib3V0L2NvbnRhY3QiLCJyZSI6MX0&amp;s=UVPvvSTOyYq-sFzTxnQb7_dg91E">advertise
					here</a> <strong><a href="http://twitter.com/StackCareers">follow
						us</a></strong> <strong><a href="http://meta.stackexchange.com">feedback
						always welcome</a></strong>
			</div>
		</div>
	</footer>
</body>
</html>