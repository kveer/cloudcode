<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>Login Page</title>
<style>
.global {
	text-align: right;
	color: orange;
	padding: 3px;
}

.header {
	width: 1200px;
	height: 52px;
	margin: 0 auto;
}

.container {
	width: 975px;
	background: #fff;
	display: block;
	margin-left: 188px;
	color: grey;
}

.profileRow {
	font-family: "Times New Roman", Georgia, Serif;
	border-bottom: solid 1px #e9e9e9;
	border-right: 1px solid transparent;
	position: relative;
	padding: 10px;
	box-sizing: border-box;
	height: auto
}

.profileImage {
	float: left;
	padding-right: 10px;
	padding-top: 3px;
}

.profileInfo {
	height: auto;
	overflow: hidden;
}

h3 {
	margin: 0px;
}

p {
	margin: 0px;
	margin-bottom: 10px;
}

p.profileSkill {
	font-style: italic
}

h3.profileName {
	color: #07c
}
.actionTab{
	padding-left:600px;padding-bottom:10px;
}
.askQuestionAction{
	float:left;padding-right:10px;
}
.availabilityAction{
	float:left;padding-right:10px;
}
.errorblock {
	color: #ff0000;
	background-color: #ffEEEE;
	border: 3px solid #ff0000;
	padding: 8px;
	margin: 16px;
}
</style>
</head>
<body onload='document.f.j_username.focus();'>
	<!-- Header -->	
	<div class="header">
		<b style="color: Orange">CloudKode</b>
	</div>
	
	
	<!-- Container -->
	<div class="container">
	 	<div style='padding-top: 150px;padding-left: 225px;'>
			<c:if test="${not empty error}">
				<div class="errorblock">
					Your login attempt was not successful, try again.<br /> Caused :
					${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}
				</div>
			</c:if>
		 
			<form name='f' action="/KodeWebWar-1.0-SNAPSHOT/auth" method='POST'>
		 
				<table>
					<tr>
						<td>Email:</td>
						<td><input type='text' name='email' value=''>
						</td>
					</tr>
					<tr>
						<td>Password:</td>
						<td><input type='password' name='password' />
						</td>
					</tr>
					<tr>
						<td colspan='1'><input name="submit" type="submit"
							value="submit" />
						</td>
						<td colspan='2'><input name="reset" type="reset" />
						</td>
					</tr>
				</table>
		 
			</form>
		</div>
	</div>
</body>
</html>