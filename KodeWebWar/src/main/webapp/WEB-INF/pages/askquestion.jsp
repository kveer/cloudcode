<%@ page errorPage="errorpage.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<html>
<head>
<title>Ask a Question</title>
<link rel="stylesheet" type="text/css"
	href="http://xoxco.com/projects/code/tagsinput/jquery.tagsinput.css" />
<script type="text/javascript"
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
<script type="text/javascript"
	src="http://xoxco.com/projects/code/tagsinput/jquery.tagsinput.js"></script>
<!-- To test using the original jQuery.autocomplete, uncomment the following -->
<!--
	<script type='text/javascript' src='http://xoxco.com/x/tagsinput/jquery-autocomplete/jquery.autocomplete.min.js'></script>
	<link rel="stylesheet" type="text/css" href="http://xoxco.com/x/tagsinput/jquery-autocomplete/jquery.autocomplete.css" />
	-->
<script type='text/javascript'
	src='https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/jquery-ui.min.js'></script>
<link rel="stylesheet" type="text/css"
	href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/themes/start/jquery-ui.css" />

<style>
.global {
	text-align: right;
	color: orange;
	padding: 3px;
}

.header {
	width: 1200px;
	height: 52px;
	margin: 0 auto;
}

.container {
	width: 975px;
	background: #fff;
	display: block;
	margin-left: 188px;
	color: grey;
}

.profileRow {
	font-family: "Times New Roman", Georgia, Serif;
	border-bottom: solid 1px #e9e9e9;
	border-right: 1px solid transparent;
	position: relative;
	padding: 10px;
	box-sizing: border-box;
	height: auto
}

.profileImage {
	float: left;
	padding-right: 10px;
	padding-top: 3px;
}

.profileInfo {
	height: auto;
	overflow: hidden;
}

h3 {
	margin: 0px;
}

p {
	margin: 0px;
	margin-bottom: 10px;
}

p.profileSkill {
	font-style: italic
}

h3.profileName {
	color: #07c
}
.actionTab{
	padding-left:600px;padding-bottom:10px;
}
.askQuestionAction{
	float:left;padding-right:10px;
}
.availabilityAction{
	float:left;padding-right:10px;
}

.error {
	color: #ff0000;
	background-color: #ffEEEE;
}
</style>
<script type="text/javascript">
	function onAddTag(tag) {
		alert("Added a tag: " + tag);
	}
	function onRemoveTag(tag) {
		alert("Removed a tag: " + tag);
	}

	function onChangeTag(input, tag) {
		alert("Changed a tag: " + tag);
	}

	$(function() {
	    $("#datepicker").datepicker();

		$('#tags').tagsInput({
			width : 'auto'
		});

		//Uncomment this line to see the callback functions in action
		//	$('input.tags').tagsInput({onAddTag:onAddTag,onRemoveTag:onRemoveTag,onChange: onChangeTag});		

		//Uncomment this line to see an input with no interface for adding new tags.
		//	$('input.tags').tagsInput({interactive:false});
	});
</script>
</head>
<body>
	<body>
	<!-- Global -->
	<sec:authorize access="isAuthenticated()">
		<div class="global">
			<p>${username}</p>
		</div>
	</sec:authorize>
	<sec:authorize access="isAnonymous()">
		<div class="global">
			<a href="/KodeWebWar-1.0-SNAPSHOT/kode/login" style="color: orange;">
				Signin</a> <a href="/KodeWebWar-1.0-SNAPSHOT/kode/reg"
				style="color: orange;"> Register</a>
		</div>
	</sec:authorize>
	
	<!-- Header -->	
	<div class="header">
		<b style="color: Orange">CloudKode</b>
	</div>
	
	
	<!-- Container -->
	<div class="container">
	
	<form:form name='f' action="/KodeWebWar-1.0-SNAPSHOT/kode/question/save/"
		method='POST' commandName="question">
		<div>
		<table>
			<tr>
				<td>Title:</td>
				<td><form:input path="title" /></td>
				<td><form:errors path="title" cssClass="error" /></td>
			</tr>
			<tr>
				<td>Kind of Help (Optional):</td>
				<td><form:input path="kindofhelp" /></td>
				<td><form:errors path="kindofhelp" cssClass="error" /></td>
			</tr>
			<tr>
				<td>TimeZone : <select id="appointment_time_zone" name="timezone">
						<option value="America/Dawson_Creek	">(GMT-07:00) America/Dawson_Creek</option>
						<option value="America/Denver">(GMT-07:00) America/Denver</option>
						<option value="Asia/Kolkata">(GMT+05:30) Asia/Kolkata</option>
						<option value="Europe/Dublin">(GMT+00:00) Europe/Dublin</option>
						<option value="Africa/Algiers">(GMT+01:00) Africa/Algiers</option>
						<option value="Africa/Gaborone">(GMT+02:00) Africa/Gaborone</option></select></td>
			</tr>
			<tr>
			<td>Date: <form:input id="datepicker" path="date"/>
			</td>
			<td><form:errors path="date" cssClass="error" /></td>
			</tr>
			<tr>
				<td>Time : <select name='time' style='width: 120px;'>
						<option value='00:00'>12:00 AM</option>
						<option value='00:30'>12:30 AM</option>
						<option value='01:00'>1:00 AM</option>
						<option value='01:30'>1:30 AM</option>
						<option value='02:00'>2:00 AM</option>
						<option value='02:30'>2:30 AM</option>
						<option value='03:00'>3:00 AM</option>
						<option value='03:30'>3:30 AM</option>
						<option value='04:00'>4:00 AM</option>
						<option value='04:30'>4:30 AM</option>
						<option value='05:00'>5:00 AM</option>
						<option value='05:30'>5:30 AM</option>
						<option value='06:00'>6:00 AM</option>
						<option value='06:30'>6:30 AM</option>
						<option value='07:00'>7:00 AM</option>
						<option value='07:30'>7:30 AM</option>
						<option value='08:00'>8:00 AM</option>
						<option value='08:30'>8:30 AM</option>
						<option value='09:00'>9:00 AM</option>
						<option value='09:30'>9:30 AM</option>
						<option value='10:00'>10:00 AM</option>
						<option value='10:30'>10:30 AM</option>
						<option value='11:00'>11:00 AM</option>
						<option value='11:30'>11:30 AM</option>
						<option value='12:00'>12:00 PM</option>
						<option value='12:30'>12:30 PM</option>
						<option value='13:00'>1:00 PM</option>
						<option value='13:30'>1:30 PM</option>
						<option value='14:00'>2:00 PM</option>
						<option value='14:30'>2:30 PM</option>
						<option value='15:00'>3:00 PM</option>
						<option value='15:30'>3:30 PM</option>
						<option value='16:00'>4:00 PM</option>
						<option value='16:30'>4:30 PM</option>
						<option value='17:00'>5:00 PM</option>
						<option value='17:30'>5:30 PM</option>
						<option value='18:00'>6:00 PM</option>
						<option value='18:30'>6:30 PM</option>
						<option value='19:00'>7:00 PM</option>
						<option value='19:30'>7:30 PM</option>
						<option value='20:00'>8:00 PM</option>
						<option value='20:30'>8:30 PM</option>
						<option value='21:00'>9:00 PM</option>
						<option value='21:30'>9:30 PM</option>
						<option value='22:00'>10:00 PM</option>
						<option value='22:30'>10:30 PM</option>
						<option value='23:00'>11:00 PM</option>
						<option value='23:30'>11:30 PM</option>
				</select></td>
			</tr>
			<tr>
				<td>tags:</td>
				<td><form:input path="tags" id="tags" class="tags" value="" /></td>
				<td><form:errors path="tags" cssClass="error" /></td>
			</tr>

			<tr>
				<td colspan='2'><input type="submit" value="submit" /></td>
			</tr>
		</table>
		</div>
	</form:form>
	</div>
</body>
</html>