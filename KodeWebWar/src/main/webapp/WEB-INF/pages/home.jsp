<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<html>
<head>
<title>CloudKode</title>
<style>
.global {
	text-align: right;
	color: orange;
	padding: 3px;
}

.header {
	width: 1200px;
	height: 52px;
	margin: 0 auto;
}

.container {
	width: 975px;
	background: #fff;
	display: block;
	margin-left: 188px;
	color: grey;
}

.profileRow {
	font-family: "Times New Roman", Georgia, Serif;
	border-bottom: solid 1px #e9e9e9;
	border-right: 1px solid transparent;
	position: relative;
	padding: 10px;
	box-sizing: border-box;
	height: auto
}

.profileImage {
	float: left;
	padding-right: 10px;
	padding-top: 3px;
}

.profileInfo {
	height: auto;
	overflow: hidden;
}

h3 {
	margin: 0px;
}

p {
	margin: 0px;
	margin-bottom: 10px;
}

p.profileSkill {
	font-style: italic
}

h3.profileName {
	color: #07c
}
.actionTab{
	padding-left:600px;padding-bottom:10px;
}
.askQuestionAction{
	float:left;padding-right:10px;
}
.availabilityAction{
	float:left;padding-right:10px;
}
</style>
</head>
<body>
	<!-- Global -->
	<sec:authorize access="isAuthenticated()">
		<div class="global">
			<p>${username}</p>
		</div>
	</sec:authorize>
	<sec:authorize access="isAnonymous()">
		<div class="global">
			<a href="/KodeWebWar-1.0-SNAPSHOT/kode/login" style="color: orange;">
				Signin</a> <a href="/KodeWebWar-1.0-SNAPSHOT/kode/reg"
				style="color: orange;"> Register</a>
		</div>
	</sec:authorize>
	
	<!-- Header -->	
	<div class="header">
		<b style="color: Orange">CloudKode</b>
	</div>
	
	
	<!-- Container -->
	<div class="container">
		<div>
			<div class='actionTab'>
				<sec:authorize access="isAuthenticated()">
					<div class='askQuestionAction'>
						<a href="/KodeWebWar-1.0-SNAPSHOT/kode/question"
							style="color: orange;">Ask a Question</a>
					</div>
					<div class='availabilityAction'>
						<a href="/KodeWebWar-1.0-SNAPSHOT/kode/schedule"
							style="color: orange;">My Availability</a>
					</div>
					<div>
						<a href="/KodeWebWar-1.0-SNAPSHOT/kode/viewquestions"
							style="color: orange;">My Questions</a>
					</div>
				</sec:authorize>
			</div>
			<p style='font-size:18px;font-weight:bold;'>Experts</p>			
		</div>

		<c:forEach var="profile" items="${profiles}">
			<div class="profileRow">
				<div class="profileImage">
					<img src="${profile.gravatar}" height="60px" />
				</div>
				<div class="profileInfo">
					<p>
					<!-- <h3 class="profileName">${profile.firstname}</h3> -->
					<a href="${profile.uid}" style="font-size: 16px; font-weight: bold;color: #07c">${profile.firstname}</a>
					</p>
					<p>${profile.headline}</p>
					<p class="profileSkill">${profile.skill}</p>
					<p>${profile.about}</p>
				</div>
			</div>
		</c:forEach>
	</div>
</body>
</html>