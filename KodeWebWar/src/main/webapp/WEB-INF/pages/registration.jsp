<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
<title>Registration Page</title>
<style>
.global {
	text-align: right;
	color: orange;
	padding: 3px;
}

.header {
	width: 1200px;
	height: 52px;
	margin: 0 auto;
}

.container {
	width: 975px;
	background: #fff;
	display: block;
	margin-left: 188px;
	color: grey;
}

.profileRow {
	font-family: "Times New Roman", Georgia, Serif;
	border-bottom: solid 1px #e9e9e9;
	border-right: 1px solid transparent;
	position: relative;
	padding: 10px;
	box-sizing: border-box;
	height: auto
}

.profileImage {
	float: left;
	padding-right: 10px;
	padding-top: 3px;
}

.profileInfo {
	height: auto;
	overflow: hidden;
}

h3 {
	margin: 0px;
}

p {
	margin: 0px;
	margin-bottom: 10px;
}

p.profileSkill {
	font-style: italic
}

h3.profileName {
	color: #07c
}
.actionTab{
	padding-left:600px;padding-bottom:10px;
}
.askQuestionAction{
	float:left;padding-right:10px;
}
.availabilityAction{
	float:left;padding-right:10px;
}
.errorblock {
	color: #ff0000;
	background-color: #ffEEEE;
	border: 3px solid #ff0000;
	padding: 8px;
	margin: 16px;
}
.error {
	color: #ff0000;
	background-color: #ffEEEE;
}
</style>
</head>
<body onload='document.f.j_username.focus();'>
	<!-- Header -->	
	<div class="header">
		<b style="color: Orange">CloudKode</b>
	</div>
	
	
	<!-- Container -->
	<div class="container">
	<h3>Please provide your details for registration</h3>
 
	<c:if test="${not empty error}">
		<div class="errorblock">
			${error}
		</div>
	</c:if>
 
	<form:form name='f' action="regConf" method='POST' commandName="user">
 
		<table>
			<tr>
				<td>email:</td>
				<td><form:input path="username" />
				</td>
				<td><form:errors path="username" cssClass="error" /></td>
			</tr>
			<tr>
				<td>Password:</td>
				<td><form:input path="password" />
				</td>
				<td><form:errors path="password" cssClass="error" /></td>
			</tr>
			<tr>
				<td>firstname:</td>
				<td><form:input path="firstname" />
				</td>
			</tr>
			<tr>
				<td>lastname:</td>
				<td><form:input path="lastname" />
				</td>
			</tr>
			<tr>
				<td>timezone:</td>
				<td><select id="appointment_time_zone" name="timezone">
						<option value="America/Dawson_Creek	">(GMT-07:00) America/Dawson_Creek</option>
						<option value="America/Denver">(GMT-07:00) America/Denver</option>
						<option value="Asia/Kolkata">(GMT+05:30) Asia/Kolkata</option>
						<option value="Europe/Dublin">(GMT+00:00) Europe/Dublin</option>
						<option value="Africa/Algiers">(GMT+01:00) Africa/Algiers</option>
						<option value="Africa/Gaborone">(GMT+02:00) Africa/Gaborone</option></select>
				</td>
			</tr>
			<tr>
				<td colspan='2'><input name="submit" type="submit"
					value="submit" />
				</td>
			</tr>
			<tr>
				<td colspan='2'><input name="reset" type="reset" />
				</td>
			</tr>
		</table>
 
	</form:form>
	</div>
</body>
</html>