package com.kode.web.responsebuilder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface ResponseBuilder {
	
	void buildResponse(HttpServletRequest request,HttpServletResponse response);

}
