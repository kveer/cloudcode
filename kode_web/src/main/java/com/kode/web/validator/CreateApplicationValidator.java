package com.kode.web.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.kode.app.model.Application;

public class CreateApplicationValidator implements Validator {

	public boolean supports(Class<?> clazz) {
		return Application.class.equals(clazz);
	}

	public void validate(Object target, Errors errors) {
		Application application = (Application) target;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors,
				"email", "email.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors,
				"firstname", "firstname.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors,
				"lastname", "lastname.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors,
				"headline", "headline.required");
		if (application.getSkills().length == 0) {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "skills",
					"skills.required");
		}

	}

}
