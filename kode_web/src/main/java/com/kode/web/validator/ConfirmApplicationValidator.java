package com.kode.web.validator;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.kode.app.model.Profile;
import com.kode.app.model.Skill;

public class ConfirmApplicationValidator implements Validator {

	public boolean supports(Class<?> clazz) {
		return Profile.class.equals(clazz);
	}

	public void validate(Object target, Errors errors) {
		Profile profile = (Profile) target;
		// target
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "company",
				"company.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "designation",
				"designation.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "about",
				"about.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "availability",
				"availability.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "rate",
				"rate.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "timezone",
				"timezone.required");
		int i = 0;
		System.out.println(profile.getSkills());
		for (Skill skill : profile.getSkills()) {
			if (StringUtils.isBlank(skill.getSkillexpertise())) {
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "skills[" + i
						+ "].skillexpertise", "skillexpertise.required");
			} else {
				System.out.println("skill expertise : "
						+ skill.getSkillexpertise());
			}
			if (skill.getExperience() == null) {
				System.out.println("skill expertise : "
						+ skill.getExperience());
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "skills[" + i
						+ "].experience", "experience.required");
			}
			++i;
		}

	}
}
