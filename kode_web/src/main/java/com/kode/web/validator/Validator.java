package com.kode.web.validator;

import javax.servlet.http.HttpServletRequest;

import org.springframework.ui.ModelMap;

public interface Validator {
	void validate(HttpServletRequest request,ModelMap modelMap);
}
