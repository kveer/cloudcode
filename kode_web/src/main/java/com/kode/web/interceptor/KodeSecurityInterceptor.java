package com.kode.web.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.kode.app.model.AuthUser;
import com.kode.app.util.KodeConstants;

/**
 * This class sets the gateway paramater's such as user's name,mentor ,timezone
 * in the request scope so as to be used in the view(jsp)
 * 
 * @author kiran
 * 
 */
public class KodeSecurityInterceptor extends HandlerInterceptorAdapter {

	static Logger log = Logger.getLogger(KodeSecurityInterceptor.class
			.getName());

	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		log.info("In Prehandle");
		Authentication authentication = SecurityContextHolder.getContext()
				.getAuthentication();
		boolean authorized;
		if (authentication != null && authentication.isAuthenticated()
				&& (authentication.getPrincipal() instanceof AuthUser)) {
			authorized = true;
			AuthUser user = (AuthUser) authentication.getPrincipal();
			log.info(ReflectionToStringBuilder.toString(user));
			request.setAttribute("auth", true);
			request.setAttribute("uid", user.getUid());
			request.setAttribute("username", user.getName());
			if(user.getName() == null){
				String username = user.getUsername();
				request.setAttribute("username", username.substring(0,username.lastIndexOf('@')));
			}
			request.setAttribute("mentor", user.isMentor());
			request.setAttribute(KodeConstants.TIMEZONE, user.getTimezone());
			/**
			 * Perform any other checks related to mentor activation etc
			 */
			log.info("After setting the uid and username in the request");
		} else {
			authorized = false;
			log.info("User Authentication Criteria not met");
		}
		boolean proceed = applyRules(request, response, authorized);
		return proceed;
	}

	private boolean applyRules(HttpServletRequest request,
			HttpServletResponse response, boolean authorized) {
		boolean proceed = true;
		log.info(request.getRequestURI() + " --contextpath --- "
				+ request.getServletPath());
		/*if ((request.getRequestURI().startsWith("/kode/question") || request
				.getRequestURI().startsWith("/kode/arena")) && !authorized) {
			log.info("Applying rules");
			try {
				proceed = false;
				response.sendRedirect("/kode/login");
			} catch (Exception e) {
				proceed = false;
				e.printStackTrace();
			}
		}
		if (request.getRequestURI().startsWith("/kode/sessions/user")
				&& !authorized) {
			log.info("Applying rules");
			try {
				proceed = false;
				// response.sendRedirect("/kode/reg");
				response.setHeader("REQUIRES_AUTH", "1");
			} catch (Exception e) {
				proceed = false;
				e.printStackTrace();
			}
		}*/
		return proceed;
	}

}
