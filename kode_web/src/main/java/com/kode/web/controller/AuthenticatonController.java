/**
 * 
 */
package com.kode.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author kiran
 * 
 */
@Controller
public class AuthenticatonController {

	static Logger log = Logger.getLogger(AuthenticatonController.class
			.getName());

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(ModelMap model) {
		log.info("Returning Login Page");
		return "login";

	}

	@RequestMapping(value = "/loginfailed", method = RequestMethod.GET)
	public String loginerror(ModelMap model,HttpServletRequest request) {
		log.info("Login Failed");
		model.addAttribute("error", "true");
		model.addAttribute("username", request.getParameter("username"));
		return "login";
	}

}
