package com.kode.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.kode.app.context.KodeApplicationContext;
import com.kode.app.model.User;
import com.kode.app.service.UserService;
import com.kode.web.handler.RequestHandler;
import com.kode.web.security.SecurityAuthenticationGateway;

/**
 * This class has api's for User Registration,Forgot Password
 * 
 * @author kiran
 * 
 */
@Controller
public class RegisterController {

	static Logger log = Logger.getLogger(RegisterController.class.getName());

	@Autowired
	UserService userService;

	@Autowired(required = true)
	private SecurityAuthenticationGateway securityAuthenticationGateway;

	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String showRegistration(ModelMap model) {
		model.addAttribute("user", new User());
		return "registration";
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String register(@Valid User user, BindingResult result,
			ModelMap model, HttpServletRequest request) throws Exception {
		if (result.hasErrors()) {
			return "registration";
		}
		log.info("User : " + ReflectionToStringBuilder.toString(user));
		String view = ((RequestHandler) KodeApplicationContext
				.getBean(registerHandler)).handle(request, user);
		/**
		 * Incase of any exception in the below steps.Swallow and show login
		 * page.
		 */
		securityAuthenticationGateway.autoLogin(user, request);
		return view;
	}

	@RequestMapping(value = "/forgotpassword", method = RequestMethod.GET)
	public String displayForgotPassword() throws Exception {
		return "forgotpassword";
	}

	@RequestMapping(value = "/forgotpassword", method = RequestMethod.POST)
	public String confirmForgotPassword(HttpServletRequest request)
			throws Exception {
		String view = ((RequestHandler) KodeApplicationContext
				.getBean(forgotPasswordHandler)).handle(request,
				request.getParameter("email"));
		return view;
	}

	@RequestMapping(value = "/changepassword", method = RequestMethod.POST)
	public String changepassword(HttpServletRequest request) throws Exception {
		String password = (String) request.getParameter("password");
		String confirmpassword = (String) request
				.getParameter("confirmpassword");
		if (StringUtils.isBlank(password)
				|| StringUtils.isBlank(confirmpassword)
				|| !confirmpassword.equals(password)) {
			log.error("Error while validating change password fields");
			request.setAttribute("errormessage",
					"Please ensure that password and confirm are not empty and are same.");
			return "changepassword";
		}
		String view = ((RequestHandler) KodeApplicationContext
				.getBean(changePasswordHandler)).handle(request, password);
		securityAuthenticationGateway.autoLogin(
				(User) request.getAttribute("user"), request);// Incase user is
																// null.handle
		return view;
	}

	/*
	 * private String sendActivationMail(ModelMap model, HttpServletRequest req)
	 * throws Exception { String status = "Send Activation Mail"; return "home";
	 * 
	 * }
	 * 
	 * @RequestMapping(value = "/activate/{aid}", method = RequestMethod.POST)
	 * public String activate(@PathVariable String aid, ModelMap model,
	 * HttpServletRequest req) throws Exception { String status =
	 * "Successfully Registered"; // Activate User model.addAttribute("message",
	 * status); return "home"; }
	 */

	@ExceptionHandler(Exception.class)
	public ModelAndView handleException(Exception exp) {
		log.error("An exception occured while processing request : ["
				+ exp.getMessage() + "]");
		ModelAndView mav = new ModelAndView();
		/*
		 * StackTraceElement[] sTE = exp.getStackTrace(); mav.addObject("ste",
		 * sTE.toString()); mav.addObject("exception", exp);
		 */
		mav.setViewName("error");
		exp.printStackTrace();
		return mav;
	}

	public static final String registerHandler = "registerHandler";
	public static final String changePasswordHandler = "changePasswordHandler";
	public static final String forgotPasswordHandler = "forgotPasswordHandler";

}
