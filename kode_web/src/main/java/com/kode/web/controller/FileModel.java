package com.kode.web.controller;

import org.springframework.web.multipart.MultipartFile;

public class FileModel {
	
	private MultipartFile file;
	private String blob;

	public String getBlob() {
		return blob;
	}

	public void setBlob(String blob) {
		this.blob = blob;
	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}	
}
