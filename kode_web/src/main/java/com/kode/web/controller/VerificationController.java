package com.kode.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.kode.app.context.KodeApplicationContext;
import com.kode.app.exception.RuleFailureException;
import com.kode.app.model.User;
import com.kode.app.service.UserService;
import com.kode.web.handler.RegisterHandler;
import com.kode.web.handler.RequestHandler;

/**
 * This handles the user forgot password and user email verification flows
 * 
 * @author kiran
 * 
 */
@Controller
public class VerificationController {

	static Logger log = Logger.getLogger(VerificationController.class.getName());

	@Autowired
	UserService userService;

	@RequestMapping(value = "/user/verify/email/{encodedToken}", method = RequestMethod.GET)
	public String verifyEmail(HttpServletRequest request,@PathVariable String encodedToken) throws Exception {
		return ((RequestHandler) KodeApplicationContext.getBean(userVerificationHandler)).handle(request , encodedToken);
	}
	
	@RequestMapping(value = "/user/verify/forgotpassword/{encodedToken}", method = RequestMethod.GET)
	public String verify(HttpServletRequest request,@PathVariable String encodedToken) throws Exception {
		return ((RequestHandler) KodeApplicationContext.getBean(forgotPasswordVerificationHandler)).handle(request, encodedToken);
	}

	/*
	 * private String sendActivationMail(ModelMap model, HttpServletRequest req)
	 * throws Exception { String status = "Send Activation Mail"; return "home";
	 * 
	 * }
	 * 
	 * @RequestMapping(value = "/activate/{aid}", method = RequestMethod.POST)
	 * public String activate(@PathVariable String aid, ModelMap model,
	 * HttpServletRequest req) throws Exception { String status =
	 * "Successfully Registered"; // Activate User model.addAttribute("message",
	 * status); return "home"; }
	 */

	@ExceptionHandler(Exception.class)
	public ModelAndView handleException(Exception exp) {
		log.error("An exception occured while processing request : ["
				+ exp.getMessage() + "]");
		ModelAndView mav = new ModelAndView();
		StackTraceElement[] sTE = exp.getStackTrace();
		mav.addObject("ste", sTE.toString());
		mav.addObject("exception", exp);
		mav.setViewName("error");
		exp.printStackTrace();
		return mav;
	}
	
	public static final String userVerificationHandler = "userVerificationHandler";
	public static final String forgotPasswordVerificationHandler = "forgotPasswordVerificationHandler";

}
