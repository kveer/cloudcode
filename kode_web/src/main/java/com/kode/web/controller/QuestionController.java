package com.kode.web.controller;

import java.sql.Timestamp;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.kode.app.context.KodeApplicationContext;
import com.kode.app.exception.RuleFailureException;
import com.kode.app.model.Question;
import com.kode.app.model.Session;
import com.kode.app.service.QuestionService;
import com.kode.app.util.KodeConstants;
import com.kode.web.handler.RequestHandler;

/**
 * This will not only be used or posing questions but also for hosting sessiosn
 * by the admin in whch case this shud be a generic name
 * 
 * @author kiran
 * 
 */
@Controller
public class QuestionController {

	static Logger log = Logger.getLogger(QuestionController.class.getName());

	@Autowired(required = true)
	private QuestionService questionService;

	/**
	 * Return the view for posting a question
	 * 
	 * @param model
	 * @return
	 * @throws RuleFailureException
	 */
	@RequestMapping(value = "/questions/ask", method = RequestMethod.GET)
	public String presentQuestionPage(ModelMap model,
			HttpServletRequest request, HttpServletResponse response)
			throws RuleFailureException {
		log.info("Entry");
		Question question = new Question();
		request.setAttribute("action", "/kode/questions/save");
		if (request.getParameter("mentor") != null) {
			request.setAttribute("action", "/kode/questions/save?mentor="
					+ request.getParameter("mentor"));
			request.setAttribute("questionHasMentor", true);
			question.setProfilehandle(request.getParameter("mentor"));
		}
		RequestHandler requestHandler = getHandler(presentQuestionHandler);
		String view = requestHandler.handle(request, question);
		model.addAttribute("question", question);
		log.info("Exit");
		return view;
	}

	/**
	 * Need to validate the fields here using question
	 * 
	 * @param question
	 * @param bindingResult
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws RuleFailureException
	 */
	@RequestMapping(value = "/questions/save", method = RequestMethod.POST)
	public String saveQuestion(@Valid Question question,
			BindingResult bindingResult, ModelMap model,
			HttpServletRequest request, HttpServletResponse response)
			throws RuleFailureException {
		log.info("Entry");
		if (bindingResult.hasErrors()) {
			log.info("Errors during form validation,Hence presenting question page");
			return "question_ask";
		}
		RequestHandler requestHandler = getHandler(saveQuestionHandler);
		return requestHandler.handle(request, question);
	}

	/**
	 * Need to validate the fields here using question
	 * 
	 * @param question
	 * @param bindingResult
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws RuleFailureException
	 */
	@RequestMapping(value = "/questions/view/{questionUUID}", method = RequestMethod.GET)
	public String presentViewQuestionPage(HttpServletRequest request,
			@PathVariable String questionUUID) throws RuleFailureException {
		log.info("Entry");
		RequestHandler requestHandler = getHandler(viewQuestionHandler);
		String view = requestHandler.handle(request, questionUUID);
		log.info("Exit");
		return view;
	}

	@RequestMapping(value = "/questions/update/{questionUUID}", method = RequestMethod.GET)
	public String presentUpdateQuestionPage(HttpServletRequest request,
			@PathVariable String questionUUID) throws RuleFailureException {
		log.info("Entry");
		/**
		 * Incase the question has a tagged mentor ,form the ulr with request
		 * param as mentor profile handle
		 */
		request.setAttribute("action", "/questions/update/" + questionUUID);
		RequestHandler requestHandler = getHandler(presentQuestionForUpdateHandler);
		requestHandler.handle(request, questionUUID);
		log.info("Exit");
		return "question_update";
	}

	@RequestMapping(value = "/questions/update/{questionUUID}", method = RequestMethod.POST)
	public String updateQuestion(@Valid Question question,
			BindingResult bindingResult, @PathVariable String questionUUID,
			ModelMap model, HttpServletRequest request,
			HttpServletResponse response) throws RuleFailureException {
		log.info("Entry");
		if (bindingResult.hasErrors()) {
			log.info("Errors during form validation,Hence presenting update question page");
			return "question_update";
		}
		question.setQuid(questionUUID);
		RequestHandler requestHandler = getHandler(updateQuestionHandler);
		String view = requestHandler.handle(request, question);
		log.info("Exit");
		return view;
	}

	/**
	 * http://webmasters.stackexchange.com/questions/31212/difference-between-
	 * accept-and-content-type-http-headers The headers below in requestmaping
	 * are request headers. Content type is when a sends content to b in which
	 * case you specify what is the type of content that a sent. Here a or b can
	 * be browser or server
	 * 
	 */

	@RequestMapping(value = "/questions", method = RequestMethod.GET, headers = { "Accept=application/json" })
	@ResponseBody
	public ResponseEntity<List<Question>> getQuestions(
			HttpServletRequest request) {
		log.info("Entry");
		String timeZone = (String) request.getAttribute(KodeConstants.TIMEZONE);
		List<Question> questionList = questionService.getQuestions();
		for (Question question : questionList) {
			DateTime dateTime = new DateTime(question.getScheduletime()
					.getTime(), DateTimeZone.forID(timeZone));
			// DateTimeFormatter formatter =
			// DateTimeFormat.forPattern("MM/dd/yyyy HH:mm");
			// question.setDate(dateTime.toString("MM/dd/yyyy hh:mm a"));
			question.setDate(dateTime.toString("dd MMM hh:mm a , yyyy"));
		}
		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", "application/json");
		log.info("Exit");
		return new ResponseEntity<List<Question>>(questionList, headers,
				HttpStatus.OK);
	}

	@RequestMapping(value = "/questions/user", method = RequestMethod.GET, headers = { "Accept=application/json" })
	public ResponseEntity<List<Question>> getQuestionsByUser(Model model,
			HttpServletRequest request) {
		log.info("Entry");
		List<Question> questionList = questionService
				.getQuestionsByUser((Integer) request.getAttribute("uid"));
		String timeZone = (String) request.getAttribute(KodeConstants.TIMEZONE);
		for (Question question : questionList) {
			DateTime dateTime = new DateTime(question.getScheduletime()
					.getTime(), DateTimeZone.forID(timeZone));
			// DateTimeFormatter formatter =
			// DateTimeFormat.forPattern("MM/dd/yyyy HH:mm");
			// question.setDate(dateTime.toString("MM/dd/yyyy hh:mm a"));
			question.setDate(dateTime.toString("dd MMM hh:mm a , yyyy"));
		}
		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", "application/json");
		log.info("Exit");
		return new ResponseEntity<List<Question>>(questionList, headers,
				HttpStatus.OK);
	}

	/*
	 * @RequestMapping(value = "/questions/tags/{tag}", method =
	 * RequestMethod.GET, headers = { "Accept=application/json"})
	 * 
	 * @ResponseBody public ResponseEntity<List<Question>> getQuestionsByTag(
	 * 
	 * @PathVariable String tag, HttpServletRequest request) {
	 * log.info("Entry"); Map tagMap = new HashMap(); tagMap.put("tag", tag);
	 * List<Question> questionList = questionService.getQuestionsByTag(tagMap);
	 * for (Question question : questionList) { Timestamp timestamp =
	 * question.getScheduletime(); String tz = question.getTimezone();
	 * log.info(timestamp + " " + tz); log.info(timestamp.getTime());
	 * log.info(timestamp.getTimezoneOffset()); log.info(TimeZone.getDefault());
	 * DateTime dateTime = new DateTime(timestamp.getTime(),
	 * DateTimeZone.forID(tz));// Use LocalDateTime if no zone // conversion
	 * required .. question.setDate(dateTime.toString()); log.info(dateTime);
	 * log.info(dateTime.toString()); } HttpHeaders headers = new HttpHeaders();
	 * headers.set("Content-Type", "application/json"); log.info("Exit"); return
	 * new ResponseEntity<List<Question>>(questionList, headers, HttpStatus.OK);
	 * }
	 */

	@RequestMapping(value = "/sessions/user", method = RequestMethod.GET, headers = {
			"Accept=application/json", "Content-type=application/json" })
	@ResponseBody
	public ResponseEntity<List<Session>> getSessions(HttpServletRequest request) {
		log.info("Entry");
		Integer uid = (Integer) request.getAttribute("uid");
		List<Session> sessionList = questionService.getSessions(uid);
		for (Session session : sessionList) {
			Question question = session.getQuestion();
			Timestamp timestamp = question.getScheduletime();
			String tz = question.getTimezone();
			log.info(timestamp.getTime());
			DateTime dateTime = new DateTime(timestamp.getTime(),
					DateTimeZone.forID(tz));// Use LocalDateTime if no zone
											// conversion required ..
			question.setDate(dateTime.toString());
			log.info(dateTime.getMillis());
			log.info(dateTime.toString());
		}
		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", "application/json");
		log.info("Exit");
		return new ResponseEntity<List<Session>>(sessionList, headers,
				HttpStatus.OK);
	}

	/*
	 * @RequestMapping(value = "/opensession/{sid}", method = RequestMethod.GET)
	 * public String openSession(@PathVariable String sid, Model model,
	 * HttpServletRequest request) { log.info("Entry");
	 * model.addAttribute("sid", sid); log.info("Exit"); return "sessionwindow";
	 * }
	 */

	/*
	 * @RequestMapping(value = "/setSlot", method = RequestMethod.POST, headers
	 * = { "Accept=application/json", "Content-type=application/json" }) public
	 * ResponseEntity setSlot(@RequestBody Slot slot, HttpServletRequest
	 * request) throws RuleFailureException { log.info("Entry"); log.info(slot);
	 * RequestHandler requestHandler = getHandler(scheduleHandler);
	 * requestHandler.handle(request, null, slot); log.info("Exit"); // return
	 * "availability"; HttpHeaders headers = new HttpHeaders();
	 * headers.set("Content-Type", "application/json"); return new
	 * ResponseEntity(headers, HttpStatus.OK);
	 * 
	 * }
	 *//**
	 * Get the slots for the specific user
	 * 
	 * @param uid
	 * @param request
	 * @return
	 */
	/*
	 * @RequestMapping(value = "/getSlots", method = RequestMethod.GET, headers
	 * = { "Accept=application/json", "Content-type=application/json" }) public
	 * ResponseEntity getSlots(@PathVariable int uid, HttpServletRequest
	 * request) throws RuleFailureException { log.info("Entry"); RequestHandler
	 * requestHandler = getHandler(scheduleHandler);
	 * requestHandler.handle(request, null, null); log.info("Exit"); // return
	 * "availability"; HttpHeaders headers = new HttpHeaders();
	 * headers.set("Content-Type", "application/json"); return new
	 * ResponseEntity(headers, HttpStatus.OK);
	 * 
	 * }
	 */

	/*
	 * @RequestMapping(value = "/sessions/{qid}/action/{actionstatus}", method =
	 * RequestMethod.POST, headers = { "Accept=application/json",
	 * "Content-type=application/json" })
	 * 
	 * @ResponseBody public ResponseEntity<List<Question>>
	 * confirmSessionForQuestion(
	 * 
	 * @PathVariable Integer qid, @PathVariable String actionstatus,
	 * HttpServletRequest request) throws HandlerException { log.info("Entry");
	 * RequestHandler requestHandler = getHandler(sessionHandler);
	 * requestHandler.handle(request,null, new Session(qid, actionstatus));
	 * HttpHeaders headers = new HttpHeaders(); headers.set("Content-Type",
	 * "application/json"); log.info("Exit"); return new
	 * ResponseEntity<List<Question>>(headers, HttpStatus.OK); }
	 */

	/*
	 * @RequestMapping(value = "/schedule", method = RequestMethod.GET) public
	 * String schedule(Model model, HttpServletRequest request) {
	 * log.info("Entry"); log.info("Exit"); return "availability"; }
	 */

	private RequestHandler getHandler(String handlerName) {
		return (RequestHandler) KodeApplicationContext.getBean(handlerName);
	}

	@ExceptionHandler(RuleFailureException.class)
	public ModelAndView handleWebException(RuleFailureException exp) {
		log.error("An exception occured while processing request : ["
				+ exp.getMessage() + "]");
		ModelAndView mav = new ModelAndView();
		StackTraceElement[] sTE = exp.getStackTrace();
		if (exp.getErrorPage() != null) {
			mav.setViewName(exp.getErrorPage());
		} else {
			mav.setViewName("error");
		}
		mav.addObject("ste", sTE.toString());
		mav.addObject("exception", exp);
		mav.addObject("message", exp.getErrorPageMsg());
		exp.printStackTrace();
		return mav;
	}

	@ExceptionHandler(Exception.class)
	public ModelAndView handleException(Exception exp) {
		log.error("An exception occured while processing request : ["
				+ exp.getMessage() + "]");
		ModelAndView mav = new ModelAndView();
		StackTraceElement[] sTE = exp.getStackTrace();
		mav.addObject("ste", sTE.toString());
		mav.addObject("exception", exp);
		mav.setViewName("error");
		exp.printStackTrace();
		return mav;
	}

	public static final String presentQuestionHandler = "presentQuestionHandler";
	public static final String updateQuestionHandler = "updateQuestionHandler";
	public static final String presentQuestionForUpdateHandler = "presentQuestionForUpdateHandler";
	public static final String saveQuestionHandler = "saveQuestionHandler";
	public static final String viewQuestionHandler = "viewQuestionHandler";
	public static final String scheduleHandler = "scheduleHandler";
	public static final String sessionHandler = "sessionHandler";

}
