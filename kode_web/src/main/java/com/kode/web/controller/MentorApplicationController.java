package com.kode.web.controller;


import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.kode.app.context.KodeApplicationContext;
import com.kode.app.exception.RuleFailureException;
import com.kode.app.model.Application;
import com.kode.app.model.Profile;
import com.kode.app.model.Skill;
import com.kode.app.util.KodeConstants;
import com.kode.app.util.KodeUtil;
import com.kode.web.handler.RequestHandler;

/**
 * This class has api's for Mentor Creation and Activation
 * 
 * @author kiran
 * 
 */
/**
 * Let the handlers return a Response Object which has the view name and if its
 * a redirect kind of status.
 * 
 * 
 */
@Controller
public class MentorApplicationController {

	static Logger log = Logger.getLogger(MentorApplicationController.class
			.getName());

	@Autowired
	Validator confirmApplicationValidator;

	@Autowired
	Validator createApplicationValidator;

	@RequestMapping(value = "/mentor/apply", method = RequestMethod.GET)
	public String showApplication(Model model, HttpServletRequest request) {
		log.info("Entry");
		String view = "mentorapplication_step1";
		log.info("Getting skillmap for setting in the request");
		if (KodeUtil.getSkillMap() != null) {
			request.setAttribute(KodeConstants.SKILLS, KodeUtil.getSkillMap());
		} else {
			view = "application_error";
		}
		model.addAttribute("application", new Application());
		log.info("Exit");
		return view;
	}

	@RequestMapping(value = "/mentor/application/create", method = RequestMethod.POST)
	public String createApplication(Application application,
			BindingResult bindingResult, Model model,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String view = "application_error";
		log.info("entry");
		createApplicationValidator.validate(application, bindingResult);
		if (bindingResult.hasErrors()) {
			log.info("Errors during form validation,Hence returning user to mentorapplication_step1 page");
			request.setAttribute(KodeConstants.SKILLS, KodeUtil.getSkillMap());
			return "mentorapplication_step1";
		}
		RequestHandler requestHandler = getHandler(createApplicationHandler);
		requestHandler.handle(request,application);
		if (request.getAttribute(KodeConstants.REDIRECT) != null) {
			log.info("Redirecting the user to edit application page");
			response.sendRedirect((String) (request
					.getAttribute(KodeConstants.REDIRECT)));
			view = null;
		} else if (request.getAttribute("emailAlreadyRegisteredError") != null) {
			log.info("Email is already registered,Hence returning user to mentorapplication_step1 page");
			request.setAttribute(KodeConstants.SKILLS, KodeUtil.getSkillMap());
			view = "mentorapplication_step1";
		} else {
			log.info("None of the conditions are satisfied,Hence redirecting the user to Error Page");
			view = "application_error";
		}
		log.info("Exit");
		return view;
	}

	@RequestMapping(value = "/mentor/application/edit/{appId}", method = RequestMethod.GET)
	public String editApplication(ModelMap model, HttpServletRequest request,
			HttpServletResponse response, @PathVariable String appId)
			throws RuleFailureException {
		log.info("entry");
		log.info("AppId from path : " + appId);
		request.setAttribute("appId", appId);
		RequestHandler requestHandler = getHandler(editApplicationHandler);
		requestHandler.handle(request, null);
		log.info("Exit");
		model.addAttribute("profile", new Profile()
				.setSkillList((List<Skill>) request
						.getAttribute(KodeConstants.SKILL_LIST)));
		return "mentorapplication_step2";
	}

	@RequestMapping(value = "/mentor/application/confirm/{appId}", method = RequestMethod.POST)
	public String confirmApplication(Profile profile,
			BindingResult bindingResult, ModelMap model,
			HttpServletRequest request, HttpServletResponse response,
			@PathVariable String appId) throws RuleFailureException {
		request.setAttribute("appId", appId);
		confirmApplicationValidator.validate(profile, bindingResult);
		if (bindingResult.hasErrors()) {
			log.info("BindingResult has Errors,Returning mentorapplication_step2 page");
			return "mentorapplication_step2";
		}
		RequestHandler requestHandler = getHandler(confirmApplicationHandler);
		requestHandler.handle(request, profile);
		model.addAttribute("message", "Successful Appplication Submission");
		log.info("Exit");
		return "application_success";

	}

	@RequestMapping(value = "/mentor/application/activate/{appId}", method = RequestMethod.GET)
	public String showMentorActivationPage(Model model,
			HttpServletRequest request, HttpServletResponse response,
			@PathVariable String appId) {
		log.info("Entry");
		request.setAttribute("path", "/kode/mentor/application/activate/"
				+ appId);
		model.addAttribute("application", new Application());
		log.info("Exit");
		return "mentor_activation";
	}

	@RequestMapping(value = "/mentor/application/activate/{appId}", method = RequestMethod.POST)
	public String activateMentor(@Valid Application application,
			BindingResult bindingResult, ModelMap model,
			HttpServletRequest request, HttpServletResponse response,
			@PathVariable String appId) throws RuleFailureException {
		request.setAttribute("appId", appId);
		if (bindingResult.hasErrors()) {
			log.info("Errors during form validation,Hence returning user to mentor_activation page");
			return "mentor_activation";
		}
		RequestHandler requestHandler = getHandler(activationHandler);
		requestHandler.handle(request, application);
		model.addAttribute("message", "Successful Mentor Activation");
		log.info("Exit");
		return "application_success";
	}

	private RequestHandler getHandler(String handlerName) {
		return (RequestHandler) KodeApplicationContext.getBean(handlerName);
	}

	@ExceptionHandler(RuleFailureException.class)
	public ModelAndView handleWebException(RuleFailureException exp) {
		log.error("An exception occured while processing request : ["
				+ exp.getMessage() + "]");
		ModelAndView mav = new ModelAndView();
		StackTraceElement[] sTE = exp.getStackTrace();
		if (exp.getErrorPage() != null) {
			mav.setViewName(exp.getErrorPage());
		} else {
			mav.setViewName("application_error");
		}
		mav.addObject("ste", sTE.toString());
		mav.addObject("exception", exp);
		mav.addObject("message", exp.getErrorPageMsg());
		exp.printStackTrace();
		return mav;
	}

	@ExceptionHandler(Exception.class)
	public ModelAndView handleException(Exception exp) {
		log.error("An exception occured while processing request : ["
				+ exp.getMessage() + "]");
		ModelAndView mav = new ModelAndView();
		StackTraceElement[] sTE = exp.getStackTrace();
		mav.addObject("ste", sTE.toString());
		mav.addObject("exception", exp);
		mav.setViewName("application_error");
		exp.printStackTrace();
		return mav;
	}

	public static final String createApplicationHandler = "createApplicationHandler";
	public static final String editApplicationHandler = "editApplicationHandler";
	public static final String confirmApplicationHandler = "confirmApplicationHandler";
	public static final String activationHandler = "activationHandler";

}
