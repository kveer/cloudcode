package com.kode.web.controller;

import java.security.Principal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.kode.app.model.Mentor;
import com.kode.app.service.MentorService;

/**
 * Profile might conflict as even users will have profiles.So change
 * profile_mapper to mentorprofile_mapper and change names to getMentorProfile
 * so that we can use profile for generic profile which any user has.
 * 
 * We might change the mentor term to expert
 * 
 * @author kiran
 * 
 */

@Controller
public class MentorController {

	static Logger log = Logger.getLogger(MentorController.class
			.getName());

	@Autowired(required = true)
	MentorService mentorService;

	@RequestMapping(value = "/{profilehandle}", method = RequestMethod.GET)
	public String getMentor(@PathVariable String profilehandle, ModelMap model) {
		log.info("Entry");
		Mentor mentor = mentorService.getMentorByHandle(profilehandle);
		if (mentor == null) {
			log.info("Null Profile Retrieved,hence returning to Error Page");
			return "application_error";
		}		
		log.info("Exit");
		model.addAttribute("mentor", mentor);
		return "mentor";
	}

	@RequestMapping(value = "/mentors", method = RequestMethod.GET)
	public String getMentors(ModelMap model) {
		log.info("Entry");
		List<Mentor> mentors = mentorService.getMentors();
		for (Mentor mentor : mentors) {
			if(mentor.getProfile().getBriefSkills()!=null){
				String briefSkills = mentor.getProfile().getBriefSkills();
				briefSkills.replace("|", "  ");
				mentor.getProfile().setBriefSkills(briefSkills);
			}			
		}
		model.addAttribute("mentors", mentors);
		log.info("Exit");
		return "mentors";
	}

	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String home(ModelMap model, Principal principal,
			HttpServletRequest req) {
		List<Mentor> mentors = mentorService.getMentors();
		model.addAttribute("mentors", mentors);
		return "home";
	}

	@ExceptionHandler(Exception.class)
	public ModelAndView handleException(Exception exp) {
		log.error("An exception occured while processing request : ["
				+ exp.getMessage() + "]");
		ModelAndView mav = new ModelAndView();
		StackTraceElement[] sTE = exp.getStackTrace();
		mav.addObject("ste", sTE.toString());
		mav.addObject("exception", exp);
		mav.setViewName("application_error");
		exp.printStackTrace();
		return mav;
	}

	/*
	 * String date = request.getParameter("date"); String time =
	 * request.getParameter("time"); String tz =
	 * request.getParameter("timezone"); String datetime = date+" "+time;
	 * log.info(datetime+" "+tz); DateTimeFormatter formatter =
	 * DateTimeFormat.forPattern("MM/dd/yyyy HH:mm"); DateTime dt =
	 * formatter.parseDateTime(datetime); log.info(dt); DateTime dtz =
	 * dt.withZone(DateTimeZone.forID(tz)); log.info(dtz);
	 * log.info(DateTimeZone.getDefault()); log.info(DateTimeZone.forID(tz));
	 * log.info(dt.getMillis()); log.info(dtz.getMillis());
	 * 
	 * DateTimeFormatter formatter1Tz =
	 * DateTimeFormat.forPattern("MM/dd/yyyy HH:mm"
	 * ).withZone(DateTimeZone.forID(tz)); DateTime dt1 =
	 * formatter1Tz.parseDateTime(datetime); log.info(dt1);
	 * log.info(dt1.getMillis());
	 */

}
