package com.kode.web.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.firebase.client.utilities.Base64.InputStream;
import com.kode.app.context.KodeApplicationContext;
import com.kode.app.exception.RuleFailureException;
import com.kode.app.model.Profile;
import com.kode.app.model.Skill;
import com.kode.app.model.User;
import com.kode.app.service.UserService;
import com.kode.web.handler.RequestHandler;

/**
 * This will not only be used or posing questions but also for hosting sessiosn
 * by the admin in whch case this shud be a generic name
 * 
 * @author kiran
 * 
 */
@Controller
public class AccountController {

	static Logger log = Logger.getLogger(AccountController.class.getName());

	@Autowired(required = true)
	private UserService userService;

	@RequestMapping(value = { "/dashboard", "/dashboard/" }, method = RequestMethod.GET)
	public String dashboard(Model model, HttpServletRequest request) {
		log.info("Entry");
		log.info("Exit");
		return "dashboard";
	}

	@RequestMapping(value = "/account", method = RequestMethod.GET)
	public String account(Model model, HttpServletRequest request) {
		log.info("Entry");
		log.info("Exit");
		return "account";
	}

	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	public String profile(Model model, HttpServletRequest request) {
		log.info("Entry");
		log.info("Exit");
		return "profile";
	}

	@RequestMapping(value = "/users/account/profile", method = RequestMethod.GET, headers = { "Accept=application/json" })
	public ResponseEntity<User> getUserProfile(Model model,
			HttpServletRequest request) {
		log.info("Entry");
		User user = userService.getUserProfile((Integer) request
				.getAttribute("uid"));
		if (user.getProfile().getBriefSkills() != null) {
			List<Skill> skillList = new ArrayList<Skill>();
			StringTokenizer skillTokens = new StringTokenizer(user.getProfile()
					.getBriefSkills(), "|");
			while (skillTokens.hasMoreTokens()) {
				skillList.add(new Skill(skillTokens.nextToken()));
			}
			user.getProfile().setSkillList(skillList);
		}
		log.info(ReflectionToStringBuilder.toString(user));
		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", "application/json");
		log.info("Exit");
		return new ResponseEntity<User>(user, headers, HttpStatus.OK);
	}

	@RequestMapping(value = "/users/profile/primary", method = RequestMethod.POST, headers = { "Accept=application/json" /*
																														 * ,
																														 * "Content-Type: application/json"
																														 */})
	public ResponseEntity updateProfileDetails(@RequestBody User user,
			HttpServletRequest request) {
		log.info("Entry" + ReflectionToStringBuilder.toString(user));
		HttpStatus httpStatus = HttpStatus.OK;
		try {
			user.setUid((Integer) request.getAttribute("uid"));
			userService.updateProfileDetails(user);
		} catch (Exception e) {
			e.printStackTrace();
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}
		log.info("Exit");
		return new ResponseEntity(httpStatus);
	}

	@RequestMapping(value = "/users/profile/social", method = RequestMethod.POST, headers = { "Accept=application/json" /*
																														 * ,
																														 * "Content-Type: application/json"
																														 */})
	public ResponseEntity updateSocialPresence(@RequestBody Profile profile,
			HttpServletRequest request) {
		log.info("Entry" + ReflectionToStringBuilder.toString(profile));
		HttpStatus httpStatus = HttpStatus.OK;
		try {
			profile.setUid((Integer) request.getAttribute("uid"));
			userService.updateSocialPresence(profile);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}
		log.info("Exit");
		return new ResponseEntity(httpStatus);
	}

	/**
	 * Whenever a new user is created create an empty profile column for him
	 * 
	 * @param skillList
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/users/profile/skills", method = RequestMethod.POST, headers = { "Accept=application/json" /*
																														 * ,
																														 * "Content-Type: application/json"
																														 */})
	public ResponseEntity updateSkills(@RequestBody Profile profile,
			HttpServletRequest request) {
		log.info("Entry" + ReflectionToStringBuilder.toString(profile));
		HttpStatus httpStatus = HttpStatus.OK;
		try {
			StringBuffer skillsBuffer = new StringBuffer();
			if (profile.getSkills().size() != 0) {
				for (Skill skill : profile.getSkills()) {
					skillsBuffer.append(skill.getSkillname() + "|");
				}
			}
			profile.setUid((Integer) request.getAttribute("uid"));
			profile.setBriefSkills(skillsBuffer.toString());
			userService.updateSkills(profile);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}
		log.info("Exit");
		return new ResponseEntity(httpStatus);
	}

	/* headers = "Accept = multipasrt/*" *//*
											 * , headers = {
											 * "consumes = multipart/form-data "
											 * }
											 *//*
												 * ,headers = {
												 * "Content-Type = multipart/form-data;boundary=***"
												 * }
												 *//*
													 * {
													 * "Accept=application/json"
													 * ,
													 * "Content-Type: application/json"
													 * }
													 */
	@RequestMapping(value = "/users/profile/picture/save", method = RequestMethod.POST)
	public ResponseEntity updateProfilePicture(
			MultipartHttpServletRequest request) {
		try {
			/* Iterator<String> itr=request.getFileNames(); */
			MultipartFile file = request.getFile("file");
			String fileName = file.getOriginalFilename();
			file.getInputStream();
			System.out.println("filename : " + fileName);
			Part filePart = (Part) request.getPart("profilepicture");
			log.info("filePart :[" + filePart + "]");
			// obtains input stream of the upload file
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ServletException e) {
			e.printStackTrace();
		}
		log.info("Exit");
		return new ResponseEntity(HttpStatus.OK);
	}

	private RequestHandler getHandler(String handlerName) {
		return (RequestHandler) KodeApplicationContext.getBean(handlerName);
	}

	@ExceptionHandler(RuleFailureException.class)
	public ModelAndView handleWebException(RuleFailureException exp) {
		log.error("An exception occured while processing request : ["
				+ exp.getMessage() + "]");
		ModelAndView mav = new ModelAndView();
		StackTraceElement[] sTE = exp.getStackTrace();
		if (exp.getErrorPage() != null) {
			mav.setViewName(exp.getErrorPage());
		} else {
			mav.setViewName("error");
		}
		mav.addObject("ste", sTE.toString());
		mav.addObject("exception", exp);
		mav.addObject("message", exp.getErrorPageMsg());
		exp.printStackTrace();
		return mav;
	}

	@ExceptionHandler(Exception.class)
	public ModelAndView handleException(Exception exp) {
		log.error("An exception occured while processing request : ["
				+ exp.getMessage() + "]");
		ModelAndView mav = new ModelAndView();
		StackTraceElement[] sTE = exp.getStackTrace();
		mav.addObject("ste", sTE.toString());
		mav.addObject("exception", exp);
		mav.setViewName("error");
		exp.printStackTrace();
		return mav;
	}

	public static final String presentQuestionHandler = "presentQuestionHandler";
	public static final String updateQuestionHandler = "updateQuestionHandler";
	public static final String presentQuestionForUpdateHandler = "presentQuestionForUpdateHandler";
	public static final String saveQuestionHandler = "saveQuestionHandler";
	public static final String viewQuestionHandler = "viewQuestionHandler";
	public static final String scheduleHandler = "scheduleHandler";
	public static final String sessionHandler = "sessionHandler";

}
