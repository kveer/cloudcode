package com.kode.web.security;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.kode.app.model.User;

public class SecurityAuthenticationGateway {

	@Autowired
	@Qualifier("authenticationManager")
	private AuthenticationManager authenticationManager;

	/**
	 * Authenticates user by invoking the spring security framework. This does a
	 * silent login.
	 * 
	 * @param user
	 * @param request
	 */
	public void autoLogin(User user, HttpServletRequest request) {
		/**
		 * The below will be done by the authenticate method of the
		 * Authenticationmanager when invoked.So no need to do that.
		 */
		/*
		 * Collection<GrantedAuthority> grantedAuthorities = new
		 * ArrayList<GrantedAuthority>(); grantedAuthorities.add(new
		 * GrantedAuthorityImpl( user.getRole() != null ? user.getRole() :
		 * "ROLE_USER"));
		 */
		UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
				user.getEmail(), user.getPassword());

		/**
		 * Is the request getsesson required -kiran ?
		 * 
		 * 
		 * http://gerrydevstory.com/2014/03/11/
		 * spring-security-auto-login-after-successful-registration/
		 * http://stackoverflow
		 * .com/questions/3813028/auto-login-after-successful-registeration
		 */
		// generate session if one doesn't exist
		// request.getSession();

		/**
		 * The below is required incase any remoteIp details etc are required
		 */
		// token.setDetails(new WebAuthenticationDetails(request));

		/**
		 * Authenticate
		 */
		Authentication authenticatedUser = authenticationManager
				.authenticate(token);

		/**
		 * Update the Security Context with the Authentication for the current
		 * Request
		 */
		SecurityContextHolder.getContext().setAuthentication(authenticatedUser);
	}

}
