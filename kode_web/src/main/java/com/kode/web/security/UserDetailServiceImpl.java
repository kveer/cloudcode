package com.kode.web.security;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.kode.app.model.AuthUser;
import com.kode.app.model.User;
import com.kode.app.service.UserService;

/**
 * This class overrides methods of the UserDetailsService to provide hook to
 * Spring Security Authentication
 * 
 * @author kiran
 * 
 */
public class UserDetailServiceImpl implements UserDetailsService {

	static Logger log = Logger.getLogger(UserDetailServiceImpl.class.getName());

	@Autowired(required = true)
	private UserService userService;

	@SuppressWarnings("deprecation")
	public UserDetails loadUserByUsername(String email)
			throws UsernameNotFoundException {
		log.info("In loadUserByUsername :" + email);
		AuthUser authUser;
		User user;
		try {
			user = userService.getUser(email);
			log.info(ReflectionToStringBuilder.toString(user));
			Collection<GrantedAuthority> gA = new ArrayList<GrantedAuthority>();
			gA.add(new GrantedAuthorityImpl(user.getRole() !=null ?user.getRole() : "ROLE_USER"));
			if (user == null)
				throw new UsernameNotFoundException("user name not found");
			if (!user.isEnabled()) {
				throw new UsernameNotFoundException("user not enabled");
				// Enabled flag can be passed while creating AuthUser and spring
				// will internally handle the validation
			}
			/**
			 * Ideally User extends AuthUser so that it helps while invoking the below constructor and thats the right way bcoz,
			 * Authuser and user are same
			 */
			authUser = new AuthUser(user.getEmail(), user.getPassword(),
					user.getUid(), user.getName(),
					user.getTimezone(),user.isMentor(),user.getStatus(),gA);
		} catch (Exception exp) {
			log.error("loadUserByUsername Error : " + exp.getMessage());
			throw new UsernameNotFoundException("database error ");
		}
		return authUser;
	}

}
