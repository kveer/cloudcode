package com.kode.web.handler;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;

import com.kode.app.exception.RuleFailureException;
import com.kode.app.gateway.email.EmailGateway;
import com.kode.app.model.User;
import com.kode.app.model.UserVerificationEmail;
import com.kode.app.model.VerificationToken;
import com.kode.app.service.UserService;

public class RegisterHandler implements RequestHandler {

	static Logger log = Logger.getLogger(RegisterHandler.class.getName());

	@Autowired
	UserService userService;

	@Autowired
	EmailGateway emailGateway;

	public String handle(HttpServletRequest request, Object obj)
			throws RuleFailureException {
		String view = "redirect:home";
		User user = (User) obj;
		try {
			register(user);
			sendVerificationEmail(user);
		} catch (DuplicateKeyException exp) {
			log.error("DuplicateKeyException occured while processing request , Message : ["
					+ exp.getMessage() + "] ,Cause : [" + exp.getCause() + "]");
			exp.printStackTrace();
			String errorMessage = "This Email Id is already registered.Use forgot your password to retrieve your password";
			view = "registration";
			request.setAttribute("error", errorMessage);
		}
		return view;
	}

	private void register(User user) {
		user.setRegistrationState();
		userService.register(user);
	}

	private void sendVerificationEmail(User user) {
		VerificationToken verificationToken = new VerificationToken(user,
				VerificationToken.VerificationTokenType.emailVerification,
				31 * 24 * 60);
		emailGateway.send(new UserVerificationEmail(user.getEmail(),
				verificationToken));
		user.addVerificationToken(verificationToken);
		userService.mapUserVerificationToken(user);
	}

}
