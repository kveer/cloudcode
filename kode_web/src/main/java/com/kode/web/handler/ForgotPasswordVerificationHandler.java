package com.kode.web.handler;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.kode.app.exception.VerificationException;
import com.kode.app.model.VerificationToken;
import com.kode.app.service.UserService;

public class ForgotPasswordVerificationHandler extends TokenVerificationHandler {

	static Logger log = Logger
			.getLogger(ForgotPasswordVerificationHandler.class.getName());

	@Autowired
	UserService userService;

	public String handle(HttpServletRequest request, Object obj) {
		String view = "changepassword";
		try {
			String encodedToken = (String) obj;
			VerificationToken verificationToken = verify(encodedToken);
			userService.tokenVerified((verificationToken));
			setRequiredRequestParams(verificationToken, request);
		} catch (VerificationException exp) {
			log.error("VerificationException occured while processing request , Message : ["
					+ exp.getMessage() + "] ,Cause : [" + exp.getCause() + "]");
			exp.printStackTrace();
			view = "error";
			request.setAttribute("errormessage",
					"The link might have expired or might be invalid");
		}
		return view;
	}

	private void setRequiredRequestParams(VerificationToken verificationToken,
			HttpServletRequest request) {
		/**
		 * For jsp hidden data field to identify the user when he clicks on the
		 * link
		 */
		request.setAttribute("vt", verificationToken.getEncodedToken());
	}

}
