package com.kode.web.handler;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;

import com.kode.app.exception.RuleFailureException;
import com.kode.app.model.Skill;
import com.kode.app.service.MentorApplicationService;
import com.kode.app.util.KodeConstants;
import com.kode.app.util.KodeUtil;

/**
 * 
 * @author kiran
 * 
 */
public class EditApplicationHandler implements RequestHandler {

	static Logger log = Logger
			.getLogger(EditApplicationHandler.class.getName());
	@Autowired(required = true)
	MentorApplicationService applicationService;

	/*
	 * Need to check an invalid appid for which we shud not be having any entry
	 * in the db. (non-Javadoc)
	 * 
	 * Handle if appId corresponds to a mentor profile which is either already
	 * submitted or activated,in which case,Show Message that Mentor is already
	 * Done Invoke UserAuthService and check status column.If needed we can
	 * maintain similar one in mentor_application so that an extra call is not
	 * needed Anyone who has appId can edit application,but they cannot edit the
	 * email registered under the appId
	 * 
	 * @see com.kode.web.handler.RequestHandler#handle(javax.servlet.http.
	 * HttpServletRequest, java.lang.Object)
	 */
	public String handle(HttpServletRequest request, Object obj)
			throws RuleFailureException {
		try {
			Map application = applicationService
					.getApplication((String) request
							.getAttribute(KodeConstants.APPID));
			/**
			 * The below block looks too messy.Damn! Change it.
			 */
			if (application != null) {
				log.info("application : ["
						+ ReflectionToStringBuilder.toString(application) + "]");
				if (application.get("status") != null
						&& ((Boolean) application.get("status")).booleanValue()) {
					log.info("Status is Confirmed and hence this Application is already confirmed");
					throw new RuleFailureException(
							"This application is already confirmed",
							"This application is already confirmed",
							"application_error");
				} else {
					List<Skill> skillList = getApplicationSkillList((String) application
							.get(KodeConstants.SKILL_LIST));
					request.setAttribute(KodeConstants.SKILL_LIST, skillList);
				}
			}else{
				throw new RuleFailureException(
						"There is no application with this ID",
						"There is no application with this ID",
						"application_error");
			}

		} catch (DataAccessException exp) {
			log.error("DataAcessException Occurred,Cause : [ " + exp.getCause()
					+ "]");
			Throwable th = exp.getCause();
			log.error("DataAcessException Occurred,Message : ["
					+ exp.getMessage() + "]");
			if (th instanceof SQLException) {
				SQLException sqlExp = (SQLException) th;
				log.error("SQLState: " + sqlExp.getSQLState());
				log.error("Error Code: " + sqlExp.getErrorCode());
			}
			throw exp;
		}
		return null;
	}

	private List<Skill> getApplicationSkillList(String skills) {
		log.info("entry");
		HashMap<Integer, String> skillMap = KodeUtil.getSkillMap();
		List<Skill> skillList = null;
		StringTokenizer skillST = new StringTokenizer(skills, " ");
		skillList = new ArrayList<Skill>();
		while (skillST.hasMoreTokens()) {
			Integer skillId = Integer.valueOf(skillST.nextToken());
			Skill sk = new Skill();
			sk.setSkid(Integer.valueOf(skillId));
			sk.setSkillname(skillMap.get(skillId));
			skillList.add(sk);
		}
		log.info("Mentor Application  skillList : [" + skillList + "]");
		log.info("exit");
		return skillList;
	}

}
