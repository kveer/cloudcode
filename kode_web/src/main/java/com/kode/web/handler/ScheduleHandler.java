package com.kode.web.handler;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;

import com.kode.app.model.Question;
import com.kode.app.model.Slot;
import com.kode.app.service.MentorService;
import com.kode.app.service.QuestionService;

public class ScheduleHandler implements RequestHandler {

	static Logger log = Logger.getLogger(ScheduleHandler.class.getName());
	@Autowired(required = true)
	QuestionService questionService;

	/*
	 * Need to check an invalid appid for which we shud not be having any entry
	 * in the db. (non-Javadoc)
	 * 
	 * @see com.kode.web.handler.RequestHandler#handle(javax.servlet.http.
	 * HttpServletRequest, java.lang.Object)
	 */
	public String handle(HttpServletRequest request, Object obj) {
		try {
			Integer uid = null;
			if(request.getAttribute("uid") != null){
				uid = (Integer)request.getAttribute("uid");
			} 
			Slot slot= (Slot)obj;
			log.info(slot);
			log.info(uid);
			DateTimeFormatter dtFormatter = DateTimeFormat.forPattern("MM/dd/yyyy HH:mm").withZone(DateTimeZone.forID(slot.getTimezone()));
			String datetimeStr = slot.getDate() + " " + slot.getTime();
			DateTime datetime = dtFormatter.parseDateTime(datetimeStr);
			log.info(datetime);
			log.info(datetime.getMillis());
			slot.setUid(uid);
			slot.setSlot(new Timestamp(datetime.getMillis()));
			questionService.setSlot(slot);
			/*List<Slot> list = profileService.getSlots(uid);
			for (Slot slot2 : list) {
				log.info(slot2.getSlot().getTime());
			}*/
			log.info("Exit");
		} catch (DataAccessException exp) {
			log.error("DataAcessException Occurred,Cause : [ " + exp.getCause()
					+ "]");
			Throwable th = exp.getCause();
			log.error("DataAcessException Occurred,Message : ["
					+ exp.getMessage() + "]");
			if (th instanceof SQLException) {
				SQLException sqlExp = (SQLException) th;
				log.error("SQLState: " + sqlExp.getSQLState());
				log.error("Error Code: " + sqlExp.getErrorCode());
			}
			throw exp;
		}
		return null;
	}

}
