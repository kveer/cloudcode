package com.kode.web.handler;

import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.kode.app.exception.RuleFailureException;
import com.kode.app.gateway.email.EmailGateway;
import com.kode.app.model.ForgotPasswordEmail;
import com.kode.app.model.User;
import com.kode.app.model.VerificationToken;
import com.kode.app.service.UserService;
import com.kode.app.util.KodeConstants;

public class ForgotPasswordHandler implements RequestHandler {

	static Logger log = Logger.getLogger(ForgotPasswordHandler.class.getName());

	@Autowired
	UserService userService;

	@Autowired
	EmailGateway emailGateway;

	public String handle(HttpServletRequest request, Object obj)
			throws RuleFailureException {
		String view = "success";
		String displayMessage = "An email has been sent to the provided email address.Please follow the instructions in the email for accessing your Account";
		String email = (String) obj;
		log.info("email " + email);
		try {
			User user = isValidUser(email);
			sendVerificationEmail(user);
		} catch (RuleFailureException exp) {
			log.error("RuleFailureException occured while processing request , Message : ["
					+ exp.getMessage() + "] ,Cause : [" + exp.getCause() + "]");
			exp.printStackTrace();
			view = "forgotpassword";
			ResourceBundle errorMsg = ResourceBundle
					.getBundle(KodeConstants.MESSAGE);
			String errormessage = errorMsg.getString(exp.getMessage());
			request.setAttribute("error", errormessage);
		}
		request.setAttribute("successmessage", displayMessage);
		return view;
	}

	private User isValidUser(String email) {
		User user = userService.getUser(email);
		if (user == null) {
			throw new RuleFailureException(
					RuleFailureException.RuleFailureError.InvalidUser);
		}
		return user;
	}

	private void sendVerificationEmail(User user) {
		VerificationToken verificationToken = new VerificationToken(user,
				VerificationToken.VerificationTokenType.forgotPassword, 24 * 60);
		emailGateway.send(new ForgotPasswordEmail(user.getEmail(),
				verificationToken));
		user.addVerificationToken(verificationToken);
		userService.mapUserVerificationToken(user);
	}

}
