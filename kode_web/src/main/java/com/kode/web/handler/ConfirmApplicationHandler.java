package com.kode.web.handler;

import java.sql.SQLException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;

import com.kode.app.exception.RuleFailureException;
import com.kode.app.model.Profile;
import com.kode.app.service.MentorApplicationService;
import com.kode.app.util.KodeConstants;

/**
 * 
 * @author kiran
 * 
 */
public class ConfirmApplicationHandler implements RequestHandler {

	static Logger log = Logger.getLogger(ConfirmApplicationHandler.class
			.getName());
	@Autowired(required = true)
	MentorApplicationService applicationService;

	public String handle(HttpServletRequest request, Object obj)
			throws RuleFailureException {
		try {
			/**
			 * A null check for the appId is not required as path variable is
			 * not optional in the request mapping as per spring reference docs
			 */
			/**
			 * Validate if the application for this appId is already
			 * confirmed.Maintain a status variable if needed mentor_appl table
			 */
			/**
			 * Incase invalid appId,then handle
			 */
			/**
			 * The below block looks too messy.Damn! Change it.
			 */
			String appId = (String) request.getAttribute(KodeConstants.APPID);
			Map application = applicationService.getApplication(appId);
			if (application!=null) {
				if (application.get("status") != null
						&& ((Boolean) application.get("status")).booleanValue()) {
					log.info("Status is Confirmed and hence this Application is already confirmed");
					throw new RuleFailureException(
							"This application is already confirmed",
							"This application is already confirmed",
							"application_error");
				} else {
					Profile profile = (Profile) obj;
					applicationService.confirm(appId, profile);
				}				
			}else{
				throw new RuleFailureException(
						"There is no application with this ID",
						"There is no application with this ID",
						"application_error");
			}			
		} catch (DataAccessException exp) {
			log.error("DataAcessException Occurred,Cause : [ " + exp.getCause()
					+ "]");
			Throwable th = exp.getCause();
			log.error("DataAcessException Occurred,Message : ["
					+ exp.getMessage() + "]");
			if (th instanceof SQLException) {
				SQLException sqlExp = (SQLException) th;
				log.error("SQLState: " + sqlExp.getSQLState());
				log.error("Error Code: " + sqlExp.getErrorCode());
			}
			throw exp;
		} catch (NumberFormatException exp) {
			log.error("NumberFormatException Occurred,Cause : [ "
					+ exp.getCause() + "]");
			exp.printStackTrace();
			throw exp;
		}
		return null;
	}

}
