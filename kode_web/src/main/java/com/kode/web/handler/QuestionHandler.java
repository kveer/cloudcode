package com.kode.web.handler;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.kode.web.domain.QuestionIntent;

public class QuestionHandler implements RequestHandler {

	static Logger log = Logger.getLogger(QuestionHandler.class.getName());

	QuestionIntent questionIntent;

	public String handle(HttpServletRequest request, Object input) {
		questionIntent.setInput(new Object[] { request, input });
		return questionIntent.act();
	}

	public QuestionIntent getQuestionIntent() {
		return questionIntent;
	}

	public void setQuestionIntent(QuestionIntent questionIntent) {
		this.questionIntent = questionIntent;
	}

}
