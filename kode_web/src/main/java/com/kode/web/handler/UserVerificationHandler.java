package com.kode.web.handler;

import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.kode.app.exception.VerificationException;
import com.kode.app.model.User;
import com.kode.app.model.VerificationToken;
import com.kode.app.service.UserService;
import com.kode.app.util.KodeConstants;

public class UserVerificationHandler extends TokenVerificationHandler {

	static Logger log = Logger.getLogger(UserVerificationHandler.class
			.getName());

	@Autowired
	UserService userService;

	public String handle(HttpServletRequest request, Object obj) {
		String view = "success";
		String errormessage = null;
		try {
			String encodedToken = (String) obj;	
			log.info("encodedToken-->"+encodedToken);
			VerificationToken verificationToken = verify(encodedToken);
			verifyUser(verificationToken.getUser());
			updateUserVerification(verificationToken);
		} catch (VerificationException exp) {
			log.error("VerificationException occured while processing request , Message : ["
					+ exp.getMessage() + "] ,Cause : [" + exp.getCause() + "]");
			exp.printStackTrace();
			ResourceBundle errorMsg = ResourceBundle
					.getBundle(KodeConstants.MESSAGE);
			view = "error";
			errormessage = errorMsg.getString(exp.getMessage());
			request.setAttribute("errormessage", errormessage);
		}
		request.setAttribute("successmessage", "Your email has been succesfully verified.Thanks");
		log.info("view :"+view);
		return view;
	}

	private boolean verifyUser(User user) {
		log.info("user :["+ReflectionToStringBuilder.toString(user)+"]");
		if (user.isEmailVerified()) {
			throw new VerificationException(
					VerificationException.VerificationError.UserVerified);
		}
		return true;
	}

	private void updateUserVerification(VerificationToken verificationToken) {
		userService.userVerified(verificationToken);
	}
}
