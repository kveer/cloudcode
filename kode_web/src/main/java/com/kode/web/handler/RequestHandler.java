package com.kode.web.handler;

import javax.servlet.http.HttpServletRequest;

import com.kode.app.exception.RuleFailureException;

public interface RequestHandler {

	/**
	 * Idealy return some response object from the handler which can say if teh
	 * response ha sto be an ajx call or not and if so the output and also if an
	 * error page or success page has to be displayed and the key for that page
	 * 
	 * @param request
	 * @param obj
	 * @throws RuleFailureException
	 */
	String handle(HttpServletRequest request, Object obj) throws RuleFailureException;

}
