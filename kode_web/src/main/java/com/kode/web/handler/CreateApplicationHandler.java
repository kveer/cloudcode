package com.kode.web.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;

import com.kode.app.model.Application;
import com.kode.app.service.MentorApplicationService;
import com.kode.app.util.KodeConstants;

/**
 * 
 * @author kiran
 * 
 */
public class CreateApplicationHandler implements RequestHandler {

	static Logger log = Logger.getLogger(CreateApplicationHandler.class
			.getName());
	private static final String prefixPath = "edit/";

	@Autowired(required = true)
	MentorApplicationService applicationService;

	public String handle(HttpServletRequest request,Object obj) {
		log.info("Entry");
		Application application = (Application) obj;
		log.info("Application : [" + ReflectionToStringBuilder.toString(application)+"]");
		try {
			Integer appId = applicationService.create(application);
			if(appId != null){
				request.setAttribute(KodeConstants.REDIRECT, prefixPath + appId);
				log.info("Retrieved a Valid App ID");
			}			
		} catch (DuplicateKeyException exp) {
			log.error("DuplicateKeyException Occurred,Cause : [ "
					+ exp.getCause() + "]");
			request.setAttribute("emailAlreadyRegisteredError",
					"Email Already Registered");
		} 
		log.info("Exit");
		return null;
	}
}
