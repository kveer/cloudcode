package com.kode.web.handler;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.kode.app.exception.VerificationException;
import com.kode.app.model.VerificationToken;
import com.kode.app.service.UserService;

abstract class TokenVerificationHandler implements RequestHandler {

	static Logger log = Logger.getLogger(TokenVerificationHandler.class
			.getName());

	@Autowired
	UserService userService;

	private VerificationToken loadToken(String encodedToken) {
		VerificationToken verificationToken = userService
				.loadToken(new VerificationToken(encodedToken));
		if (verificationToken == null) {
			throw new VerificationException(
					VerificationException.VerificationError.InvalidToken);
		}
		return verificationToken;
	}

	protected VerificationToken verify(String encodedToken) {
		VerificationToken verificationToken  = loadToken(encodedToken);
		if (verificationToken.hasExpired()) {
			throw new VerificationException(
					VerificationException.VerificationError.ExpiredToken);
		}
		return verificationToken;
	}
	
}
