package com.kode.web.handler;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.kode.app.exception.VerificationException;
import com.kode.app.model.User;
import com.kode.app.model.VerificationToken;
import com.kode.app.service.UserService;
import com.kode.web.security.SecurityAuthenticationGateway;

public class ChangePasswordHandler extends TokenVerificationHandler {

	static Logger log = Logger.getLogger(ChangePasswordHandler.class.getName());

	@Autowired
	UserService userService;

	@Autowired(required = true)
	private SecurityAuthenticationGateway securityAuthenticationGateway;

	public String handle(HttpServletRequest request, Object obj) {
		String view = "redirect:/kode/home";
		String password = (String) obj;
		try {
			VerificationToken verificationToken = loadToken(request
					.getParameter("vt"));
			changePassword(verificationToken.getUser(), password);
			setRequiredRequestParams(verificationToken.getUser(), request);
		} catch (Exception exp) {
			log.error("VerificationException occured while processing request , Message : ["
					+ exp.getMessage() + "] ,Cause : [" + exp.getCause() + "]");
			exp.printStackTrace();
			view = "error";
			request.setAttribute("errormessage",
					"Some issue while processing your request");
		}
		return view;
	}

	private VerificationToken loadToken(String encodedToken) {
		VerificationToken verificationToken = userService
				.loadToken(new VerificationToken(encodedToken));
		if (verificationToken == null) {
			throw new VerificationException(
					VerificationException.VerificationError.InvalidToken);
		}
		return verificationToken;
	}

	private void changePassword(User user, String newpassword) {
		user.changePassword(newpassword);
		userService.changePassword(user);
	}

	private void setRequiredRequestParams(User user, HttpServletRequest request) {
		/**
		 * For autologin in controller
		 **/
		request.setAttribute("user", user);
	}
}
