package com.kode.web.handler;

import java.sql.SQLException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;

import com.kode.app.exception.RuleFailureException;
import com.kode.app.model.Application;
import com.kode.app.service.MentorApplicationService;
import com.kode.app.util.KodeConstants;

public class ActivationHandler implements RequestHandler {

	static Logger log = Logger.getLogger(ActivationHandler.class.getName());
	@Autowired(required = true)
	MentorApplicationService applicationService;

	public String handle(HttpServletRequest request , Object obj)
			throws RuleFailureException {
		try {
			/**
			 * Null check not required for AppId as its a part of the Request
			 * mapping
			 * 
			 * Check whether the appid is valid i.e if its not yet activated
			 */
			Application application = (Application)obj;
			String appId = (String) request.getAttribute(KodeConstants.APPID);
			Map applicationMap = applicationService.getApplication(appId);
			if (applicationMap != null) {
				if (applicationMap.get("status") != null
						&& ((Boolean) applicationMap.get("status")).booleanValue()) {
					log.info("Status is Confirmed and hence this Application is already confirmed");
					throw new RuleFailureException(
							"This application is already confirmed",
							"This application is already confirmed",
							"application_error");
				} else {
					applicationService.activate(appId,
							application.getPassword(),application.getProfilehandle());
				}
			} else {
				throw new RuleFailureException(
						"There is no application with this ID",
						"There is no application with this ID",
						"application_error");
			}
		} catch (DataAccessException exp) {
			log.error("DataAcessException Occurred,Cause : [ " + exp.getCause()
					+ "]");
			Throwable th = exp.getCause();
			log.error("DataAcessException Occurred,Message : ["
					+ exp.getMessage() + "]");
			if (th instanceof SQLException) {
				SQLException sqlExp = (SQLException) th;
				log.error("SQLState: " + sqlExp.getSQLState());
				log.error("Error Code: " + sqlExp.getErrorCode());
			}
			throw exp;
		}
		return null;
	}

}
