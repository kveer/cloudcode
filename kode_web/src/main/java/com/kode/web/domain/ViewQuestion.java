package com.kode.web.domain;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.log4j.Logger;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;

import com.kode.app.model.Question;
import com.kode.app.service.MentorService;
import com.kode.app.service.QuestionService;
import com.kode.app.util.KodeConstants;

/**
 * @author kiran
 * 
 */
public class ViewQuestion implements QuestionIntent {

	static Logger log = Logger.getLogger(ViewQuestion.class.getName());

	@Autowired(required = true)
	QuestionService questionService;

	@Autowired(required = true)
	MentorService mentorService;

	protected String questionUUID;

	protected HttpServletRequest request;

	public void setInput(Object[] input) {
		this.request = (HttpServletRequest) input[0];
		this.questionUUID = (String) input[1];
	}

	public String act() {
		Question question = questionService.getQuestion(questionUUID,
				(Integer) request.getAttribute("uid"));
		log.info(ReflectionToStringBuilder.toString(question));
		setQuestionTime(question);
		setRequiredRequestParams(question);
		return "question_view";
	}

	protected void setRequiredRequestParams(Question question) {
		log.info("Entry");
		request.setAttribute("question", question);
		request.setAttribute("tags", question.getTags());
		log.info("Exit");
	}

	protected void setQuestionTime(Question question) {
		log.info("Millis while viewing question : "
				+ question.getScheduletime().getTime());
		String timeZone = (String) request.getAttribute(KodeConstants.TIMEZONE);
		DateTimeFormatter dtFormatter = DateTimeFormat.forPattern(
				"MM/dd/yyyy HH:mm").withZone(DateTimeZone.forID(timeZone));
		String date = dtFormatter.print(question.getScheduletime().getTime());
		log.info("Date while viewing question : " + date);
		String[] dateTimeStr = date.split(" ");
		String dateStr = dateTimeStr[0];
		String timeStr = dateTimeStr[1];
		question.setDate(dateStr);
		question.setTime(timeStr);
	}

}
