/**
 * 
 */
package com.kode.web.domain;

import org.apache.log4j.Logger;

import com.kode.app.model.Question;

/**
 * @author kiran
 * 
 */
public class PresentQuestionForUpdate extends ViewQuestion {

	static Logger log = Logger.getLogger(PresentQuestionForUpdate.class
			.getName());

	public String act() {
		Question question = questionService.getQuestion(questionUUID,
				(Integer) request.getAttribute("uid"));
		setQuestionTime(question);
		setRequiredRequestParams(question);
		return "question_update";
	}

}
