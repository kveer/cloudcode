/**
 * 
 */
package com.kode.web.domain;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.kode.app.model.Mentor;
import com.kode.app.model.Profile;
import com.kode.app.model.Question;
import com.kode.app.service.MentorService;
import com.kode.app.service.QuestionService;

/**
 * @author kiran
 * 
 */
public class PresentQuestion implements QuestionIntent {

	static Logger log = Logger.getLogger(PresentQuestion.class.getName());

	@Autowired(required = true)
	QuestionService questionService;

	@Autowired(required = true)
	MentorService mentorService;

	private Question question;

	private HttpServletRequest request;

	public void setInput(Object[] input) {
		this.request = (HttpServletRequest) input[0];
		this.question = (Question) input[1];
	}

	/*
	 * We can exclude timezone field from the question page .It can be there in
	 * the question model though incase a user wants to change his timezone
	 * other than the default timezone in settings page.
	 */
	public String act() {
		Mentor mentor = null;
		if (question.getProfilehandle() != null) {
			mentor = mentorService.getMentorByHandle(question
					.getProfilehandle());

		}
		setRequiredRequestParams(mentor);
		return "question_ask";
	}

	private void setRequiredRequestParams(Mentor mentor) {
		log.info("Entry");
		if (mentor != null) {
			Profile profile = mentor.getProfile();
			request.setAttribute("gravatar", profile.getGravatar());
			request.setAttribute("profilehandle", mentor.getProfilehandle());
			request.setAttribute("firstname", mentor.getFirstname());
			request.setAttribute("lastname", mentor.getLastname());
			if (mentor.getLastname() == null) {
				request.setAttribute("lastname", mentor.getLastname());
			}
			request.setAttribute("successmessage",
					"Question posted successfully");
			log.info("Profile :[" + ReflectionToStringBuilder.toString(profile)
					+ "]");
		}
		log.info("Exit");
	}

}
