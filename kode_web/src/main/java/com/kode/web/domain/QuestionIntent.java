/**
 * 
 */
package com.kode.web.domain;

/**
 * @author kiran
 * 
 */
public interface QuestionIntent {

	void setInput(Object[] input);

	String act();

}
