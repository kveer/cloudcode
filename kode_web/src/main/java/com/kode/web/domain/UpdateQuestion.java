/**
 * 
 */
package com.kode.web.domain;

import java.sql.Timestamp;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.kode.app.service.QuestionService;
import com.kode.app.util.KodeConstants;

/**
 * @author kiran
 * 
 */
public class UpdateQuestion extends SaveQuestion {

	static Logger log = Logger.getLogger(UpdateQuestion.class.getName());

	@Autowired(required = true)
	QuestionService questionService;

	public void buildQuestion() {
		log.info("Entry");
		Integer uid = (Integer) request.getAttribute("uid");
		String timeZone = (String) request.getAttribute(KodeConstants.TIMEZONE);
		question.setTimezone(timeZone);
		question.setProfilehandle(request.getParameter("mentor"));
		question.setUid(uid);
		log.info("Millis while saving question : "
				+ parseQuestionTime(question).getMillis());
		log.info("Date while saving question : " + parseQuestionTime(question));
		question.setScheduletime(new Timestamp(parseQuestionTime(question)
				.getMillis()));
		// question.setStatus("asked");
		log.info("Exit");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.kode.web.domain.IQuestion#act()
	 */
	public String act() {
		buildQuestion();
		questionService.updateQuestion(question,
				(Integer) request.getAttribute("uid"));
		return "question_success";
	}

}
