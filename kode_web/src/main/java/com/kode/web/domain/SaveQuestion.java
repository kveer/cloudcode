package com.kode.web.domain;

import java.sql.Timestamp;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;

import com.kode.app.model.Question;
import com.kode.app.service.QuestionService;
import com.kode.app.util.KodeConstants;

/**
 * @author kiran
 * 
 */
public class SaveQuestion implements QuestionIntent {

	static Logger log = Logger.getLogger(SaveQuestion.class.getName());

	@Autowired(required = true)
	QuestionService questionService;

	protected Question question;

	protected HttpServletRequest request;

	public void setInput(Object[] input) {
		this.request = (HttpServletRequest) input[0];
		this.question = (Question) input[1];
	}

	/*
	 * We can exclude timezone field from the question page .It can be there in
	 * the question model though incase a user wants to change his timezone
	 * other than the his default timezone in settings page. Also we need to
	 * store the timezone field from the user table in question table for each
	 * question because if he changes timezone in user ie.e settings page we
	 * will not have track of previously asked questions timezone's
	 * 
	 * (non-Javadoc)
	 * 
	 * @see com.kode.web.domain.IQuestion#feedInput()
	 */
	public void buildQuestion() {
		log.info("Entry");
		Integer uid = (Integer) request.getAttribute("uid");
		String timeZone = (String) request.getAttribute(KodeConstants.TIMEZONE);
		question.setTimezone(timeZone);
		question.setProfilehandle(request.getParameter("mentor"));
		question.setQuid(UUID.randomUUID().toString());
		question.setUid(uid);
		log.info("Millis while saving question : "+parseQuestionTime(question).getMillis());
		log.info("Date while saving question : "+parseQuestionTime(question));
		question.setScheduletime(new Timestamp(parseQuestionTime(question)
				.getMillis()));
		question.setStatus("asked");
		/*
		 * log.info(question.getScheduletime());
		 * log.info(question.getScheduletime().getTime());
		 * log.info(question.getScheduletime().getTimezoneOffset());
		 */
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.kode.web.domain.IQuestion#act()
	 */
	public String act() {
		buildQuestion();
		questionService.saveQuestion(question);
		return "question_success";
	}

	protected DateTime parseQuestionTime(Question question) {
		DateTimeFormatter dtFormatter = DateTimeFormat.forPattern(
				"MM/dd/yyyy HH:mm").withZone(
				DateTimeZone.forID(question.getTimezone()));
		String datetimeStr = question.getDate() + " " + question.getTime();
		/**
		 * Datetime datetime = dtFormatter.parseDateTime(datetimeStr);
		 * log.info(datetime); <br>
		 * log.info(datetime.getMillis());
		 */
		return dtFormatter.parseDateTime(datetimeStr);
	}

}
